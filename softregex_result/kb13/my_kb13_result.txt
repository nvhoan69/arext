lines that end with 'gh'	.*gh	.*gh	STRING_EQ
lines that have the word 'laugh', and at least 1 number.	(.*\blaugh\b.*)&(.*[0-9].*)	(.*\blaugh\b.*)&(.*[0-9].*){}	DFA_NEQ
lines that contain the text, 'New York'.	.*NewYork.*	.*NewYork.*	STRING_EQ
lines using 4 or more words	(.*\b[A-Za-z]+\b.*){4,}	(.*\b[A-Za-z]+\b.*){7,}	DFA_NEQ
lines that begin with the word 'the'	\bthe\b.*	the\b.*	DFA_EQ
lines that have 2 words using 4 letters	(.*\b[A-Za-z]{4}\b.*){2}	(.*\b[A-Za-z]{4}\b.*){2,}	DFA_EQ
lines which contain the letter 'z' twice.	(.*z.*){2,}	.*(z|<M1>).*	DFA_NEQ
lines where the word 'and' appears between the words 'sand' and 'tide'	.*\bsand\b.*\band\b.*\btide\b.*|.*\btide\b.*\band\b.*\bsand\b.*	.*(\band\b.*)&(.*\btide\b.*)	DFA_NEQ
lines that contain three letter words.	.*\b[A-Za-z]{3}\b.*	.*\b[A-Za-z]{3}\b.*	STRING_EQ
lines that contain the word 'unifax' but not the word 'colorado'.	(.*\bunifax\b.*)&(~(.*\bcolorado\b.*))	(.*\bunifax\b.*)&(~(.*\bcolorado\b.*))	STRING_EQ
lines using at least 6 characters	.*.{6}.*	.*.{6,}.*	DFA_EQ
lines ending in 'az'	.*az	.*az	STRING_EQ
lines using words that begin with 'z'.	.*\bz[A-Za-z]*\b.*	.*\bz[A-Za-z]*\b.*	STRING_EQ
lines having words starting with 'di'.	.*\bdi[A-Za-z]*\b.*	.*\bdi[A-Za-z]*\b.*	STRING_EQ
lines that have a word of 3 letters	.*\b[A-Za-z]{3}\b.*	.*\b[A-Za-z]{3}\b.*	STRING_EQ
lines containing 'a' before 'b'	.*a.*b.*	.*a.*b.*	STRING_EQ
lines that contain a number.	.*[0-9].*	.*[0-9].*	STRING_EQ
lines using the word 'An' at least once.	.*\bAn\b.*	.*(.*\bAn\b.*).*	DFA_EQ
lines using the word 'day' followed by the letters 'abc'	.*\bday\b.*abc.*	.*\bday\b.*abc.*	STRING_EQ
lines that contain words ending in 'ch'	.*\b[A-Za-z]*ch\b.*	.*\b[A-Za-z]*ch\b.*	STRING_EQ
lines that use words ending with 'ot'.	.*\b[A-Za-z]*ot\b.*	.*\b[A-Za-z]*ot\b.*	STRING_EQ
lines that have no instances of 'old' but at least one instance of 'ion'.	(~(.*old.*))&((.*ion.*){1,})	~(.*old.*)	DFA_NEQ
lines that contain at least one word which is 8 characters or longer	.*((\b[A-Za-z]+\b)&(.{8,})).*	.*((\b[A-Za-z][A-Za-z]*\b)&(.*[AEIOUaeiou].*){6}).*	DFA_NEQ
lines containing the word 'shoe' and containing the word 'lace' 	(.*\bshoe\b.*)&(.*\blace\b.*)	.*(.*\bshoe\b.*)&(.*\blace\b.*)	DFA_EQ
lines that have more than 6 numbers.	(.*[0-9].*){7,}	(.*[0-9].*){6,}	DFA_NEQ
lines that have six letter words ending in 'th'.	.*\b[A-Za-z]{4}th\b.*	.*\b[A-Za-z]*th\b.*	DFA_NEQ
lines using a word	.*\b[A-Za-z]+\b.*	.*\b[A-Za-z]+\b.*	STRING_EQ
lines that contain 6 letter words beginning with the letter 'y'.	.*\by[A-Za-z]{5}\b.*	.*\by[A-Za-z]{4}\b.*	DFA_NEQ
lines where there are three characters between instances of 'ABC' and 'WEX'	.*ABC.*.{3}.*WEX.*|.*WEX.*.{3}.*ABC.*	.*(.*[0-9].*){3}.*	DFA_NEQ
lines with words that end in 'n'.	.*\b[A-Za-z]*n\b.*	.*\b[A-Za-z]*n\b.*	STRING_EQ
lines containing words that start with 'th'	.*\bth[A-Za-z]*\b.*	.*\bth[A-Za-z]*\b.*	STRING_EQ
lines which have the word 'egg'	.*\begg\b.*	.*\begg\b.*	STRING_EQ
lines any words beginning with the letter 'k'.	.*((\b[A-Za-z]+\b)&(k.*)).*	.*\bk[A-Za-z]*\b.*	DFA_EQ
lines that have the numbers '12345' and '54321'.	(.*12345.*)&(.*54321.*)	(.*12345.*)&(.*54321.*)	STRING_EQ
lines containing words ending in 're' 	.*\b[A-Za-z]*re\b.*	.*\b[A-Za-z]*re\b.*	STRING_EQ
lines that have words ending in 'ing'	.*\b[A-Za-z]*ing\b.*	.*\b[A-Za-z]*ing\b.*	STRING_EQ
lines that start with 'the'.	(the.*).*	the.*	DFA_EQ
lines that show 'Mr' and 'Mrs' but not 'Ms' or 'Miss'.	((.*Mr.*)&(.*Mrs.*)&(~(.*(Ms|Miss).*)))	(.*Mr.*)&(.*Mrs.*)&(~(.*Ms.*))	DFA_NEQ
lines that use words ending in 'g'	.*((.*g)&(\b[A-Za-z][A-Za-z]*\b)).*	.*\b[A-Za-z]*g\b.*	DFA_EQ
lines with words that contain 3 vowels.	.*((\b[A-Za-z]+\b)&(.*[AEIOUaeiou].*){3}).*	.*\b[A-Za-z]{3}\b.*	DFA_NEQ
lines that end in 'o'	.*o	.*o	STRING_EQ
lines that have at least two words that start with 's'.	(.*\bs[A-Za-z]*\b.*){2}	(.*\bs[A-Za-z]*\b.*){2}	STRING_EQ
lines that have 3 characters after the first vowel	.*[AEIOUaeiou].*(.{3}).*	.*(<M0>|<M1>).*	DFA_NEQ
lines using at least 5 characters	.*.{5}.*	.*.{6,}.*	DFA_NEQ
lines that include words with 'ra'.	.*((\b[A-Za-z]+\b)&(.*ra.*)).*	.*((\b[A-Za-z]+\b)&(.*ra.*)).*	STRING_EQ
lines that contain words with 'ru'.	.*\b[A-Za-z]*ru[A-Za-z]*\b.*	.*((\b[A-Za-z]+\b)&(.*ru.*)).*	DFA_EQ
lines that contain a word using at most 6 letters	.*\b[A-Za-z]{1,6}\b.*	.*\b[A-Za-z]{6,}\b.*	DFA_NEQ
lines using 'min' before 'imal'	.*min.*imal.*	.*min.*imal.*	STRING_EQ
lines that contain the word 'null'	.*\bnull\b.*	.*\bnull\b.*	STRING_EQ
lines that contain 'Beaker' but do not contain 'Bunsen'.	(.*Beaker.*)&(~(.*Bunsen.*))	(.*Beaker.*)&(~(.*Bunsen.*))	STRING_EQ
lines that contain 'mix' or 'shake'.	.*mix.*|.*shake.*	.*(mix|shake).*	DFA_EQ
lines with 'helper' where the word 'little' comes before 'helper'.	(.*helper.*)&(.*\blittle\b.*helper.*)	.*helper.*\bhelper\b.*	DFA_NEQ
lines using 3 instances of 'sw'	.*(.*sw.*){3}.*	.*sw.*	DFA_NEQ
lines that contain words with 're'.	.*\b[A-Za-z]*re[A-Za-z]*\b.*	.*((\b[A-Za-z]+\b)&(.*re.*)).*	DFA_EQ
lines that begin with the word 'once'.	once\b.*	once\b.*	STRING_EQ
lines that begin with a number and end with 'street' or 'avenue'.	[0-9].*(street|avenue)	[0-9].*street	DFA_NEQ
lines that have three numbers between 'abc' and 'def'.	.*(abc.*(.*[0-9].*){3}.*def|.*def.*(.*[0-9].*){3}.*abc).*	(.*abc.*){3}&(.*abc.*){()}	DFA_NEQ
lines that contain at least one number.	.*[0-9].*	.*[0-9].*	STRING_EQ
lines that do not contain any number	~(.*[0-9].*)	~(.*[0-9].*)	STRING_EQ
lines using five letter words.	.*\b[A-Za-z]{5}\b.*	.*\b[A-Za-z]{5}\b.*	STRING_EQ
lines that have 'eye'	.*eye.*	.*eye.*	STRING_EQ
lines that contain 3 words with at least 1 number in between the 1st and 3rd word.	[^A-Za-z]*\b[A-Za-z]+\b((.*\b[A-Za-z]+\b.*)&(.*[0-9].*))\b[A-Za-z]+\b[^A-Za-z]*	(.*\b<M0>[A-Za-z]*\b.*){3}&(.*<M0>.*).*	DFA_NEQ
lines containing the number 11 and the word 'eleven'.	(.*11.*)&(.*eleven.*)	.*(.*\beleven\b.*)&(.*\b<M1>\b.*)	DFA_NEQ
lines which have 'ing' and 'out' but not 'sh' or 'cl'.	((.*ing.*)&(.*out.*)&(~(.*(sh|cl).*)))	(.*ing.*)&(.*out.*)&(~(.*sh.*))	DFA_NEQ
lines using more than 1 character	.*.{2,}.*	.*[A-Z].*	DFA_NEQ
lines which contain only lowercase letters.	[a-z]*	[A-Z]*	DFA_NEQ
lines that are composed of 4 or more words.	(.*\b[A-Za-z]+\b.*){4,}	(.*\b[A-Za-z]+\b.*){7}	DFA_NEQ
lines that end with vowels.	.*[AEIOUaeiou]	.*[]	DFA_NEQ
lines containg the number '9'.	.*9.*	.*9.*	STRING_EQ
lines that use 'a' or 'b' before words ending with 'er'.	.*(a|b).*\b[A-Za-z]*er\b.*	.*((|b).*\ba\b.*)	DFA_NEQ
lines that have three words comprised of 4 characters each.	.*(.*\b[A-Za-z]{4}\b.*){3}.*	(.*\b[A-Za-z]{4}\b.*){3}	DFA_EQ
lines containing only a letter	[A-Za-z]	[A-Za-z]*	DFA_NEQ
lines using 2 instances of 'irl'	.*(.*irl.*){2}.*	(.*irl.*){2,}	DFA_EQ
lines with capital letters	.*[A-Z].*	.*[AEIOUaeioui].*	DFA_NEQ
lines that show 'May' and 'June'.	.*(.*May.*)&(.*June.*).*	(.*May.*)&(.*June.*)	DFA_EQ
lines that include one word using at least 2 letters	.*\b[A-Za-z]{2,}\b.*	.*((\b[A-Za-z][A-Za-z]*\b.*){2}.*)	DFA_NEQ
lines that contain words between 'I' and 'you'	.*I.*\b[A-Za-z]+\b.*you.*|.*you.*\b[A-Za-z]+\b.*I.*	.*(.*\bI\b.*)&(.*you.*)	DFA_NEQ
lines utilizing the number '1'.	.*1.*	.*1.*	STRING_EQ
lines that have words containing 'ei'	.*\b[A-Za-z]*ei[A-Za-z]*\b.*	.*\b[A-Za-z]*ei[A-Za-z]*\b.*	STRING_EQ
lines that contain at least one digit.	.*[0-9].*	.*[0-9].*	STRING_EQ
lines containing a word using 'ause' 	.*\b[A-Za-z]*ause[A-Za-z]*\b.*	.*\b[A-Za-z]*ause[A-Za-z]*\b.*	STRING_EQ
lines that use words starting with 's'.	.*\bs[A-Za-z]*\b.*	.*\bs[A-Za-z]*\b.*	STRING_EQ
lines containing words starting with 't'.	.*((\bt.*\b)&([A-Za-z]+)).*	.*\bt[A-Za-z]*\b.*	DFA_EQ
lines that have a word beginning with th and a number that begins in '7'	(.*\bth[A-Za-z]*\b.*)&(.*7[0-9]*.*)	.*\b[AEIOUaeiou][A-Za-z]*7\b.*	DFA_NEQ
lines that contain three or more digits	(.*[0-9].*){3}	(.*[0-9].*){3}	STRING_EQ
lines that have all of its letters capitalized.	~(.*[a-z].*)	~(.*\b[A-Za-z]+\b.*)&(.*[0-9].*)	DFA_NEQ
lines that contain 5 numbers and 2 words that contain 'ly'.	.*(.*[0-9].*){5}&(.*\b[A-Za-z]*ly[A-Za-z]*\b.*){2}.*	(.*[0-9].*){2}&(.*\b[A-Za-z]+\b.*){2}	DFA_NEQ
lines that use words that are only four letters long.	.*\b[A-Za-z]{4}\b.*	.*\b[A-Za-z]{7}\b.*	DFA_NEQ
lines starting with 'uu' followed by words starting with 'z'.	uu.*\bz[A-Za-z]*\b.*	uu.*((\b[A-Za-z]+\b)&(z.*)).*	DFA_EQ
lines containing the word 'house'.	.*\bhouse\b.*	.*\bhouse\b.*	STRING_EQ
lines that have any instance of 'ight'.	.*ight.*	.*ight.*	STRING_EQ
lines using 'abc' after 'def'	.*def.*abc.*	.*def.*abc.*	STRING_EQ
lines using words containing 'e' before 'i'.	.*((\b[A-Za-z][A-Za-z]*\b)&(.*e.*i.*)).*	.*e.*	DFA_NEQ
lines that contain words that end in 'y' that do not begin with 'w'	.*((\b[A-Za-z]+\b)&(.*y)&(~(w.*))).*	.*\b[A-Za-z]*w\b.*	DFA_NEQ
lines that contain 4 letter words ending in 's'	.*\b[A-Za-z]{3}s\b.*	.*\b[A-Za-z]*s\b.*	DFA_NEQ
lines ending with 'xyz'	.*xyz	.*xyz	STRING_EQ
lines that contain the word 'bar'	.*\bbar\b.*	.*\bbar\b.*	STRING_EQ
lines that have the number '44'.	.*44.*	.*44.*	STRING_EQ
lines that contain a 3 letter word and a 2 letter word.	.*(.*\b[A-Za-z]{3}\b.*)&(.*\b[A-Za-z]{2}\b.*).*	.*((\b[A-Za-z][A-Za-z]*\b)&(.*[0-9].*){2}.*)	DFA_NEQ
lines using at least 4 'c'	(.*c.*){4,}	(.*c.*){4,}	STRING_EQ
lines containing both letters and numbers, but no capitals.	(.*[A-Za-z].*)&(.*[0-9].*)&(~(.*[A-Z].*))	(.*[AEIOUaeiou].*)&(.*[0-9].*)	DFA_NEQ
lines that contain either of 'y' or 'z'	.*(y|z).*	.*(y|z).*	STRING_EQ
lines that contain words starting in 's'	.*((\b[A-Za-z][A-Za-z]*\b)&(s.*)).*	.*\bs[A-Za-z]*\b.*	DFA_EQ
lines that contain the symbol '-'.	.*-.*	.*\b-\b.*	DFA_NEQ
lines that contain 5 or more letters	.*(.*[A-Za-z].*){5,}.*	.*(.*[A-Za-z]{5}.*)	DFA_NEQ
lines that utilize the number '7'.	.*7.*	.*7.*	STRING_EQ
lines utilizing words ending with 'fe'.	.*((\b[A-Za-z]+\b)&(.*fe)).*	.*\b[A-Za-z]*fe\b.*	DFA_EQ
lines which contain only the number '3'.	3	3.*3	DFA_NEQ
lines using a word containing the letter 'l'.	.*((\b[A-Za-z]+\b)&(.*l.*)).*	.*\b[A-Za-z]*l[A-Za-z]*\b.*	DFA_EQ
lines with 'bit' and not 'ch'.	(.*bit.*)&(~(.*ch.*))	(.*bit.*)&(~(.*ch.*))	STRING_EQ
lines with a word ending in 'aought' that contain only one word	([^A-Za-z])*\b[A-Za-z]*aought\b([^A-Za-z])*	.*\b[A-Za-z]*aought\b.*\b[A-Za-z]*\b.*	DFA_NEQ
lines containing words that start with 'fa'	.*\bfa[A-Za-z]*\b.*	.*\bfa[A-Za-z]*\b.*	STRING_EQ
lines that contain 'day' or 'to'	.*(day|to).*	.*(day|to).*	STRING_EQ
lines using a 3 letter sequence starting with 'n'	.*n[A-Za-z]{2}.*	.*(n[A-Za-z]{2}).*	DFA_EQ
lines that have 'efg' followed by the word 'car'	.*efg.*\bcar\b.*	.*efg.*\bcar\b.*	STRING_EQ
lines which contain a word using 2 or more letters	.*\b[A-Za-z]{2,}\b.*	.*(.*\b[A-Za-z]+\b.*)&(.*[A-Za-z].*){2}.*	DFA_NEQ
lines that contain at least one numeric in it.	.*[0-9].*	.*[0-9].*	STRING_EQ
lines that end with 'hey' 	.*hey	.*hey	STRING_EQ
lines containing any number with an '8' in it.	.*8.*	.*8.*	STRING_EQ
lines that contain the 'staphyloccus aureus.'	.*staphyloccusaureus.*	.*staphyloccusaureus..*	DFA_NEQ
lines which contain 'I am mad'.	.*Iammad.*	.*Iammad.*	STRING_EQ
lines that contain five vowels	(.*[AEIOUaeiou].*){5}	(.*[AEIOUaeiou].*){5}	STRING_EQ
lines that contain 3 numbers and one number contains 1.	(.*[0-9].*)&(.*1.*)	.*(.*[0-9].*){3}.*	DFA_NEQ
lines which contain 'qu' at the start	qu.*	.*(qu.*).*	DFA_NEQ
lines containing 'e', or 'f'	.*(e|f).*	.*(e|f).*	STRING_EQ
lines that have 'de' before 'st' and 're' after 'col'.	(.*de.*st.*)&(.*col.*re.*)	.*(.*de.*)&(.*re.*).*	DFA_NEQ
lines having words ending with 'oo'.	.*((\b[A-Za-z]+\b)&(.*oo)).*	.*\b[A-Za-z]*oo\b.*	DFA_EQ
lines which contain the word 'black' and the letter 'z'.	(.*\bblack\b.*)&(.*z.*)	.*(.*\bblack\b.*)&(.*z.*).*	DFA_EQ
lines using more than 3 characters	.*.{4,}.*	.*.{3}.*	DFA_NEQ
lines that do not use the word 'the'	~(.*\bthe\b.*)	~(.*\bthe\b.*)	STRING_EQ
lines that contain the letter 'V' and the number '33'.	(.*V.*)&(.*33.*)	(.*V.*)&(.*33.*)	STRING_EQ
lines using at least 3 'b'	(.*b.*){3,}	(.*b.*){3,}	STRING_EQ
lines that have 3 numbers and contain the word 'Columbia'.	(.*[0-9].*){3}&(.*\bColumbia\b.*)	(.*[0-9].*){3}&(.*\bColumbia\b.*).*	DFA_EQ
lines having the letters 'ea'.	.*((ea)&([A-Za-z]*)).*	.*ea.*	DFA_EQ
lines using words beginning with the letter 'x'.	.*\bx[A-Za-z]*\b.*	.*\bx[A-Za-z]*\b.*	STRING_EQ
lines using three letter words.	.*\b[A-Za-z]{3}\b.*	.*\b[A-Za-z]{3}\b.*	STRING_EQ
lines using a word that ends in 'spoon'	.*((\b[A-Za-z]+\b)&(.*spoon)).*	.*\b[A-Za-z]*spoon\b.*	DFA_EQ
lines that end with 'bar' and include the phrase 'San Jose'.	(.*bar)&(.*SanJose.*)	(.*bar.*)&(.*\bSanJose\b.*)	DFA_NEQ
lines that have more than 5 words ending with a 'c.'	(.*\b[A-Za-z]*c\b.*){6,}	.*(.*\b[A-Za-z]*c.\b.*){2}.*	DFA_NEQ
lines which contain a word ending in 'ing'.	.*\b[A-Za-z]*ing\b.*	.*\b[A-Za-z]*ing\b.*	STRING_EQ
lines that contain only 2 words, and, begin with the letter 'f'.	f.*&(([^A-Za-z])*\b[A-Za-z]+\b([^A-Za-z])*){2}	(.*[A-Z][A-Za-z]*\b.*){2}f	DFA_NEQ
lines containing the word 'revolution'.	.*\brevolution\b.*	.*\brevolution\b.*	STRING_EQ
lines containing the word 'trade'.	.*\btrade\b.*	.*\btrade\b.*	STRING_EQ
lines using 4 instances of 'ug'	.*(.*ug.*){4}.*	.*ug.*	DFA_NEQ
lines that contain at least five words.	(.*\b[A-Za-z][A-Za-z]*\b.*){5,}	(.*\b[A-Za-z]+\b.*){5}	DFA_EQ
lines using the word 'clue'	.*\bclue\b.*	.*\bclue\b.*	STRING_EQ
lines containing words that end with s.	.*\b[A-Za-z]*s\b.*	.*\b[A-Za-z]*<M0>\b.*	DFA_NEQ
lines utilizing words starting with 'w'.	.*((w.*)&(\b[A-Za-z][A-Za-z]*\b)).*	.*\ww[A-Za-z]*\w.*	DFA_NEQ
lines that have 'ser' after 'lin' or 'ku'.	.*(lin|ku).*ser.*	.*(lin|ku).*ser.*	STRING_EQ
lines that contain at least one word starting with 'q'.	.*\bq[A-Za-z]*\b.*	.*((\b[A-Za-z]+\b)&(q.*)).*	DFA_EQ
lines that have 4 words or less where at least 1 of those words contains the letter sequence 'uin'.	([^A-Za-z]*\b[A-Za-z]+\b[^A-Za-z]*){0,4}&(.*((\b[A-Za-z]+\b)&(.*uin.*)).*){1,}	([^A-Za-z]*\b[A-Za-z]+\b[^A-Za-z]*){0,5}&(.*\b[A-Za-z]+\b.*){5,}	DFA_NEQ
lines that do not have 'the' in them	~(.*the.*)	~(.*the.*)	STRING_EQ
lines which start with the word 'The'	\bThe\b.*	The\b.*	DFA_EQ
lines that have words with 're'.	.*\b[A-Za-z]*re[A-Za-z]*\b.*	.*\b[A-Za-z]*re[A-Za-z]*\b.*	STRING_EQ
lines where there is only one word.	[^A-Za-z]*\b[A-Za-z]+\b[^A-Za-z]*	([^b])*\b[A-Za-z]+\b([^A-Za-z])*	DFA_NEQ
lines that contain at least three words and at least one number.	(.*\b[A-Za-z][A-Za-z]*\b.*){3}&(.*[0-9].*)	(.*\b[A-Za-z]+\b.*){3}&(.*[0-9].*){}	DFA_NEQ
lines which contain both 'i' and 'e' but no instances of 'u'.	(.*i.*)&(.*e.*)&(~(.*u.*))	(.*i.*)&(.*e.*)&(~(.*u.*))	STRING_EQ
lines having words starting with 'fo'.	.*((\b[A-Za-z][A-Za-z]*\b)&(fo.*)).*	.*\bfo[A-Za-z]*\b.*	DFA_EQ
lines using 3 instances of 'b'	.*(.*b.*){3}.*	.*b.*	DFA_NEQ
lines that contain the number '2005' and 'May' and begin with 'Mary'.	(.*2005.*)&(.*May.*)&(Mary.*)	(.*2005.*)&(.*May.*)&(.*Mary.*)	DFA_NEQ
lines that contain more than five words.	(.*\b[A-Za-z]+\b.*){6,}	(.*\b[A-Za-z]+\b.*){5}	DFA_NEQ
lines which mention 'Pat' with the word 'turkey'.	.*(.*Pat.*)&(.*\bturkey\b.*).*	.*\bPat\b.*	DFA_NEQ
lines using words which have a vowel.	.*\b[A-Za-z]*[aeiouAEIOU][A-Za-z]*\b.*	.*\b[A-Za-z]*[aeiouAEIOU][A-Za-z]*\b.*	STRING_EQ
lines ending in 'fuzz'	.*fuzz	.*fuzz	STRING_EQ
lines that start with the letter 'c'	c.*	c.*	STRING_EQ
lines containing any mention of the word 'code'.	.*\bcode\b.*	.*\bcode\b.*	STRING_EQ
lines having words with 'ro'.	.*\b[A-Za-z]*ro[A-Za-z]*\b.*	.*\b[A-Za-z]*ro[A-Za-z]*\b.*	STRING_EQ
lines having the letter 'x'.	.*x.*	.*x.*	STRING_EQ
lines using 'life' or 'lives'	.*(life|lives).*	.*(life|lives).*	STRING_EQ
lines that contain a word in all uppercase.	.*\b[A-Z]+\b.*	.*\b([A-Za-z]+\b.*)&(.*[AEIOUaeiou].*)	DFA_NEQ
lines which do not begin with an uppercase letter.	~([A-Z].*)	~(.*[A-Za-z].*)	DFA_NEQ
lines using a word of 5 letters or less.	.*\b[A-Za-z]{1,5}\b.*	.*\b[A-Za-z]{5,}\b.*	DFA_NEQ
lines ending in 'z' preceded by a word that ends in 'nt'	.*\b[A-Za-z]*nt\b.*z	.*z.*\b[A-Za-z]*nt\b.*	DFA_NEQ
lines that contain words using the letters 'ant'	.*\b[A-Za-z]*ant[A-Za-z]*\b.*	.*\b[A-Za-z]*ant[A-Za-z]*\b.*	STRING_EQ
lines that use words with 'ca'.	.*((\b[A-Za-z]+\b)&(.*ca.*)).*	.*((\b[A-Za-z]+\b)&(.*ca.*)).*	STRING_EQ
lines that contain any numbers.	.*[0-9].*	.*[0-9].*	STRING_EQ
lines that utilize words starting with 'pu'.	.*((\b[A-Za-z]+\b)&(pu.*)).*	.*\bpu[A-Za-z]*\b.*	DFA_EQ
lines that have 3 or more words containing the letters 'ly'.	(.*((\b[A-Za-z]+\b)&(.*ly.*)).*){3,}	(.*\b[A-Za-z]*ly[A-Za-z]*\b.*){2,}	DFA_NEQ
lines using 2 or more words containing the letters 'ing'. 	(.*(\b[A-Za-z]+\b&(.*ing.*)).*){2,}	(.*\b[A-Za-z]*ing[A-Za-z]*\b.*){2,}	DFA_EQ
lines containing only four words.	(([^A-Za-z])*\b[A-Za-z]+\b([^A-Za-z])*){4}	(([^A-Za-z])*\b[A-Za-z]+\b([^A-Za-z])*){4}	STRING_EQ
lines that have words ending with 'ge'.	.*\b[A-Za-z]*ge\b.*	.*\b[A-Za-z]*ge\b.*	STRING_EQ
lines using 'ick' 	.*ick.*	.*ick.*	STRING_EQ
lines that contain at least 2 digits.	(.*[0-9].*){2,}	(.*[0-9].*){2,}	STRING_EQ
lines beginning with 'begin'	begin.*	begin.*	STRING_EQ
lines that begin with the phrase 'once upon a time'	onceuponatime.*	onceuponatime.*	STRING_EQ
lines using 6 instances of 'fu'	.*(.*fu.*){6}.*	(.*fu.*){6}	DFA_EQ
lines that use the word 'boom' followed by words starting with 'ka'	.*\bboom\b.*\bka[A-Za-z]*\b.*	.*\bboom\b.*\bka[A-Za-z]*\b.*	STRING_EQ
lines which end with 'hula' 	.*hula	.*hula	STRING_EQ
lines using 7 'f'	(.*f.*){7}	.*f.*	DFA_NEQ
lines which contain 'rh' but do not contain the letter 'y'.	(.*rh.*)&~(.*y.*)	(.*rh.*)&(~(.*y.*))	DFA_EQ
lines which start with a number.	[0-9].*	[0-9].*	STRING_EQ
lines that use words starting with 'no'.	.*\bno[A-Za-z]*\b.*	.*\bno[A-Za-z]*\b.*	STRING_EQ
lines using more than 4 characters	.*.{5,}.*	.*.{4}.*	DFA_NEQ
Find lines that mention dogs or cats but not any with mice	(.*(dogs|cats).*)&(~(.*mice.*))	(.*<M0>.*)&(~(.*[AEIOUaeiou].*))	DFA_NEQ
lines that contain the phrase 'Booyah'.	.*Booyah.*	.*(.*\bBooyah\b.*).*	DFA_NEQ
lines containing 'tal' and containing 'tel' too	(.*tal.*)&(.*tel.*)	(.*tal.*)&(.*tel.*)	STRING_EQ
lines using 'din' after 'e' or 'ky'.	.*(e|ky).*din.*	.*(e|ky).*din.*	STRING_EQ
lines which have words beginning with 'H'.	.*\bH[A-Za-z]*\b.*	.*\bH[A-Za-z]*\b.*	STRING_EQ
lines which contain multiple quoted words.	(.*\'[A-Za-z]+\'.*){2,}	.*(.*\b[A-Za-z]+\b.*){4}.*	DFA_NEQ
lines that contain words starting with 'ac'.	.*((\b[A-Za-z][A-Za-z]*\b)&(ac.*)).*	.*\bac[A-Za-z]*\b.*	DFA_EQ
lines using 'd' in the middle of a word.	.*\b[A-Za-z]+d[A-Za-z]+\b.*	.*(\b[A-Za-z]+d[A-Za-z]*\b.*){2}	DFA_NEQ
lines that contain words starting with 'gu'.	.*\bgu[A-Za-z]*\b.*	.*\bgu[A-Za-z]*\b.*	STRING_EQ
lines that use 'native' and 'tribes'.	(.*native.*)&(.*tribes.*)	(.*native.*)&(.*tribes.*)	STRING_EQ
lines containing a word using the letters 'tt'.	.*\b[A-Za-z]*tt[A-Za-z]*\b.*	.*\b[A-Za-z]*tt[A-Za-z]*\b.*	STRING_EQ
lines that have 'car'	.*car.*	.*car.*	STRING_EQ
lines that have the word 'Facebook'	.*\bFacebook\b.*	.*\bFacebook\b.*	STRING_EQ