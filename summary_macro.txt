(0) dnobori_DN-OSS-Learn_test-poison-disasm-arm.cc[12]
	[0]re.2460.map_load_re
	content: 0x[0-9a-f]+ +[0-9a-f]+ +[0-9a-f]+ +ldr r([0-9]+), \[r([0-9]+), #-1\]
	raw: start + "ldr r([0-9]+), \\[r([0-9]+), #-1\\]"
	[1]re.6563.load_const_re
	content: 0x[0-9a-f]+ +[0-9a-f]+ +[0-9a-f]+ +ldr r([0-9]+), \[pc, .*
	raw: start + "ldr r([0-9]+), \\[pc, .*"
	[2]re.5071.cmp_re
	content: 0x[0-9a-f]+ +[0-9a-f]+ +[0-9a-f]+ +cmp r([0-9]+), r([0-9]+)
	raw: start + "cmp r([0-9]+), r([0-9]+)" [3]re.8984.bne_re
	content: 0x[0-9a-f]+ +[0-9a-f]+ +[0-9a-f]+ +bne (.*)
	raw: start + "bne (.*)"
	[4]re.6911.beq_re
	content: 0x[0-9a-f]+ +[0-9a-f]+ +[0-9a-f]+ +beq (.*)
	raw: start + "beq (.*)"
	[5]re.7244.b_re
	content: 0x[0-9a-f]+ +[0-9a-f]+ +[0-9a-f]+ +b (.*)
	raw: start + "b (.*)"
	[6]re.6455.eorne_re
	content: 0x[0-9a-f]+ +[0-9a-f]+ +[0-9a-f]+ +eorne r([0-9]+), r([0-9]+), r([0-9]+)
	raw: start + "eorne r([0-9]+), r([0-9]+), r([0-9]+)"
	[7]re.4922.eoreq_re
	content: 0x[0-9a-f]+ +[0-9a-f]+ +[0-9a-f]+ +eoreq r([0-9]+), r([0-9]+), r([0-9]+)
	raw: start + "eoreq r([0-9]+), r([0-9]+), r([0-9]+)"
	[8]re.3678.csdb_re
	content: 0x[0-9a-f]+ +[0-9a-f]+ +[0-9a-f]+ +csdb
	raw: start + "csdb"
	[9]re.3434.load_field_re
	content: 0x[0-9a-f]+ +[0-9a-f]+ +[0-9a-f]+ +ldr r([0-9]+), \[r([0-9]+), #\+[0-9]+\]
	raw: start + "ldr r([0-9]+), \\[r([0-9]+), #\\+[0-9]+\\]"
	[10]re.3500.mask_re
	content: 0x[0-9a-f]+ +[0-9a-f]+ +[0-9a-f]+ +and r([0-9]+), r([0-9]+), r([0-9]+)
	raw: start + "and r([0-9]+), r([0-9]+), r([0-9]+)"
	[11]re.1187.untag_re
	content: 0x[0-9a-f]+ +[0-9a-f]+ +[0-9a-f]+ +mov r([0-9]+), r([0-9]+), asr #1
	raw: start + "mov r([0-9]+), r([0-9]+), asr #1"

(1) Ziezi_Chapter23Drill.cpp[1]
	[0]re.5243.pattern
	content: \w{2}\s*\d{5}(-\d{4})?
	raw: "\\w{2}\\s*\\d{5}(-\\d{4})?"

(2) adipose_mpc-hc_src_STS.cpp[11]
	[0]re.6016.clrrgx
	content: (<c\.([a-z]*)\.bg_([a-z]*)>([^<]*)</c[\.\w\d]*>)
	raw: LR"(<c\.([a-z]*)\.bg_([a-z]*)>([^<]*)</c[\.\w\d]*>)"
	[1]re.8946.clrrgx2
	content: (<c\.([a-z]*)>([^<]*)</c[\.\w\d]*>)
	raw: LR"(<c\.([a-z]*)>([^<]*)</c[\.\w\d]*>)"
	[2]re.2778.NoNameRegex
	content: <c[.\w\d]*>
	raw: L"<c[.\\w\\d]*>"
	[3]re.6254.NoNameRegex
	content: </c[.\w\d]*>
	raw: L"</c[.\\w\\d]*>"
	[4]re.861.NoNameRegex
	content: <\d\d:\d\d:\d\d.\d\d\d>
	raw: L"<\\d\\d:\\d\\d:\\d\\d.\\d\\d\\d>"
	[5]re.8660.NoNameRegex
	content: <v[ .][^>]*>
	raw: L"<v[ .][^>]*>"
	[6]re.4923.NoNameRegex
	content: </v>
	raw: L"</v>"
	[7]re.2990.NoNameRegex
	content: <lang[^>]*>
	raw: L"<lang[^>]*>"
	[8]re.5841.NoNameRegex
	content: </lang>
	raw: L"</lang>"
	[9]re.5680.alignRegex
	content: align:(start|left|center|middle|end|right)
	raw: L"align:(start|left|center|middle|end|right)"
	[10]re.900.cuePattern
	content: (::cue\(([^)]+)\)\s*\{([^}]*)\})
	raw: LR"(::cue\(([^)]+)\)\s*\{([^}]*)\})"

(3) haris-hindic_Programiranje_2_PR2_2020-07-15_Rjesenje.cpp[1]
	[0]re.6650.NoNameRegex
	content: (?=.{7,})(?=.*[A-Z]{1,})(?=.*[a-z]{1,})(?=.*\d{1,})(?=.*\W{1,})
	raw: "(?=.{7,})(?=.*[A-Z]{1,})(?=.*[a-z]{1,})(?=.*\\d{1,})(?=.*\\W{1,})"

(4) gunchleoc_widelands_map_object_program.cc[5]
	[0]re.4072.one_unit
	content: ^(\d+)(s|m|ms)$
	raw: "^(\\d+)(s|m|ms)$"
	[1]re.3559.two_units
	content: ^(\d+)(m|s)(\d+)(s|ms)$
	raw: "^(\\d+)(m|s)(\\d+)(s|ms)$"
	[2]re.1221.three_units
	content: ^(\d+)(m)(\d+)(s)(\d+)(ms)$
	raw: "^(\\d+)(m)(\\d+)(s)(\\d+)(ms)$"
	[3]re.751.without_unit
	content: ^(\d+)$
	raw: "^(\\d+)$"
	[4]re.5919.re
	content: ^(\d+)([.](\d{1,2})){0,1}%$
	raw: "^(\\d+)([.](\\d{1,2})){0,1}%$"

(5) includeos_path_to_regex.cpp[2]
	[0]re.8756.PATH_REGEXP
	content: ((\\.)|(([\/.])?(?:(?:\:(\w+)(?:\(((?:\\.|[^\\()])+)\))?|\(((?:\\.|[^\\()])+)\))([+*?])?|(\*))))
	raw: "((\\\\.)|(([\\/.])?(?:(?:\\:(\\w+)(?:\\(((?:\\\\.|[^\\\\()])+)\\))?|\\(((?:\\\\.|[^\\\\()])+)\\))([+*?])?|(\\*))))"
	[1]re.9411.re
	content: (.*\/$)
	raw: "(.*\\/$)"

(6) blopit_BBL_Classes_LevelScene.cpp[1]
	[0]re.8152.re
	content: ([\w\d]*)(,|-|&)\s?([\w\d]*)(,|-|&)\s?([\w\d]*)(|,|-|&)$
	raw: "([\\w\\d]*)(,|-|&)\\s?([\\w\\d]*)(,|-|&)\\s?([\\w\\d]*)(|,|-|&)$"

(7) fbs_bpftrace_src_clang_parser.cpp[1]
	[0]re.2557.re
	content: ^(const volatile\s+)|^(const\s+)|^(volatile\s+)|\*(\s*restrict)$
	raw: "^(const volatile\\s+)|^(const\\s+)|"
                       "^(volatile\\s+)|\\*(\\s*restrict)$"

(8) AREXT_copy_airtrack_regex_master_benchmarks.cpp[0]

(9) alicevision_AliceVision_src_viewIO.cpp[1]
	[0]re.8245.regexFrame
	content: ^(.*\D)?([0-9]+)([\-_\.].*[[:alpha:]].*)?$
	raw: "^(.*\\D)?"       // the optional prefix which end with a non digit character
                          "([0-9]+)"        // the sequence frame number
                          "([\\-_\\.]"      // the suffix start with a separator
                          ".*[[:alpha:]].*" // at least one letter in the suffix
                          ")?$"

(10) AREXT_copy_caryliu1999_cocos2d-x-lite-1_cocos_ProgramLib.cpp[2]
	[0]re.1450.precision
	content: precision\s+(lowp|mediump|highp).*?;
	raw: "precision\\s+(lowp|mediump|highp).*?;"
	[1]re.7639.accuracy
	content: (lowp|mediump|highp)\s
	raw: "(lowp|mediump|highp)\\s"

(11) caryliu1999_cocos2d-x-lite-1_cocos_ProgramLib.cpp[2]
	[0]re.2748.precision
	content: precision\s+(lowp|mediump|highp).*?;
	raw: "precision\\s+(lowp|mediump|highp).*?;"
	[1]re.9915.accuracy
	content: (lowp|mediump|highp)\s
	raw: "(lowp|mediump|highp)\\s"

(12) regex_sample_00.cpp[0]

(13) regex_sample_00.cpp[0]

(14) avast_r2utils.cpp[3]
	[0]re.1227.NoNameRegex
	content: ([^*]+)([*]+)
	raw: "([^*]+)([*]+)"
	[1]re.7239.NoNameRegex
	content: [*]+
	raw: "[*]+"
	[2]re.3050.NoNameRegex
	content: [{](.*)[}]
	raw: "[{](.*)[}]"

(15) chellmuth_obj_parser.cpp[3]
	[0]re.1282.expression
	content: (-?\d+)\s+(-?\d+)\s+(-?\d+)\s+(-?\d+)\s*
	raw: "(-?\\d+)\\s+(-?\\d+)\\s+(-?\\d+)\\s+(-?\\d+)\\s*"
	[1]re.5342.expression
	content: (-?\d+)/(-?\d+)/(-?\d+) (-?\d+)/(-?\d+)/(-?\d+) (-?\d+)/(-?\d+)/(-?\d+)\s*
	raw: "(-?\\d+)/(-?\\d+)/(-?\\d+) (-?\\d+)/(-?\\d+)/(-?\\d+) (-?\\d+)/(-?\\d+)/(-?\\d+)\\s*"
	[2]re.2422.expression
	content: (-?\d+)//(-?\d+) (-?\d+)//(-?\d+) (-?\d+)//(-?\d+)\s*
	raw: "(-?\\d+)//(-?\\d+) (-?\\d+)//(-?\\d+) (-?\\d+)//(-?\\d+)\\s*"

(16) CESNET_Server.cpp[2]
	[0]re.3929.moduleWildcard
	content: ^/restconf/data/([a-zA-Z_][a-zA-Z_.-]*:\*)$
	raw: "^"s + restconfRoot + "data/(" + atom + ":\\*)$"
	[1]re.7061.subtree
	content: ^/restconf/data/([a-zA-Z_][a-zA-Z_.-]*:[a-zA-Z_][a-zA-Z_.-]*(/([a-zA-Z_][a-zA-Z_.-]*:)?[a-zA-Z_][a-zA-Z_.-]*)*)$
	raw: "^"s + restconfRoot + "data/(" + atom + ":" + atom + "(/(" + atom + ":)?" + atom + ")*)$"

(17) xander-io_overwatch_utils.cpp[1]
	[0]re.8180.ip_regex
	content: ^(?:[0-9]{1,3}\.){3}[0-9]{1,3}$
	raw: "^(?:[0-9]{1,3}\\.){3}[0-9]{1,3}$"

(18) matusnovak_yamlreader.cpp[4]
	[0]re.5630.re
	content: ^(true|false)$
	raw: "^(true|false)$"
	[1]re.9904.re
	content: ^[-+]?\d+([Ee][+-]?\d+)?$
	raw: "^[-+]?\\d+([Ee][+-]?\\d+)?$"
	[2]re.8603.re
	content: ^[-+]?\d*\.{1}\d+([Ee][+-]?\d+)?$
	raw: "^[-+]?\\d*\\.{1}\\d+([Ee][+-]?\\d+)?$"
	[3]re.1984.re
	content: ^[-+]?(?:0|[1-9]\d*)(?:\.\d+)?(?:[eE][+-]?\d+)?$
	raw: "^[-+]?(?:0|[1-9]\\d*)(?:\\.\\d+)?(?:[eE][+-]?\\d+)?$"

(19) tud-zih-energy_lo2s_format.cpp[2]
	[0]re.6973.field_regex
	content: ^\s+field:([^;]+);\s+offset:(\d+);\s+size:(\d+);\s+signed:(\d+);$
	raw: "^\\s+field:([^;]+);\\s+offset:(\\d+);\\s+size:(\\d+);\\s+signed:(\\d+);$"
	[1]re.6004.type_name_regex
	content: ^(.*) ([^ \[\]]+)(\[[^\]]+\])?$
	raw: "^(.*) ([^ \\[\\]]+)(\\[[^\\]]+\\])?$"

(20) jvisenti_MPReader.cpp[7]
	[0]re.8127.MeshValue
	content: Vertex|Index|Texture
	raw: "Vertex|Index|Texture"
	[1]re.3127.ModelValue
	content: Mesh|Position|Rotation|Scale|Motion
	raw: "Mesh|Position|Rotation|Scale|Motion"
	[2]re.8597.MotionValue
	content: Path|Repeat|Loop|Duration
	raw: "Path|Repeat|Loop|Duration"
	[3]re.5062.EnvironmentValue
	content: Dynamic|Step|RotationStep|Origin|Size|ActiveObject|Obstacles
	raw: "Dynamic|Step|RotationStep|Origin|Size|ActiveObject|Obstacles"
	[4]re.5102.FloatLit
	content: ^\-?[\d]*\.?[\d]*$
	raw: "^\\-?[\\d]*\\.?[\\d]*$"
	[5]re.4635.IntLit
	content: ^\-?[\d]+
	raw: "^\\-?[\\d]+"
	[6]re.9111.Identifier
	content: ^\w+[\w\d]*$
	raw: "^\\w+[\\w\\d]*$"

(21) encryptogroup_algorithm_description.cpp[4]
	[0]re.8987.kLineTwoNumbersRegex
	content: ^\s*(\d+)\s+(\d+)\s*$
	raw: "^\\s*(\\d+)\\s+(\\d+)\\s*$"
	[1]re.4862.kLineThreeNumbersRegex
	content: ^\s*(\d+)\s+(\d+)\s+(\d+)\s*$
	raw: "^\\s*(\\d+)\\s+(\\d+)\\s+(\\d+)\\s*$"
	[2]re.5852.kLineGateRegex
	content: ^\s*(1|2)\s+(1)\s+(\d+)\s+(\d+\s+)?(\d+)\s+(XOR|AND|INV)\s*$
	raw: "^\\s*(1|2)\\s+(1)\\s+(\\d+)\\s+(\\d+\\s+)?(\\d+)\\s+(XOR|AND|INV)\\s*$"
	[3]re.2149.kLineWhitespaceRegex
	content: ^\s*$
	raw: "^\\s*$"

(22) nputikhin-sat_atpg-src-iscas89_parser.cpp[6]
	[0]re.2536.input_regex
	content: \s*input\s*\(\s*(\S+)\s*\)\s*\r?\n?
	raw: R"r(\s*input\s*\(\s*(\S+)\s*\)\s*\r?\n?)r"
	[1]re.8795.output_regex
	content: \s*output\s*\(\s*(\S+)\s*\)\s*\r?\n?
	raw: R"r(\s*output\s*\(\s*(\S+)\s*\)\s*\r?\n?)r"
	[2]re.5792.gate_regex
	content: \s*(\S+)\s*=\s*(\w+)\s*\(\s*((?:\S+\s*,?\s*)+)\s*\)\s*\r?\n?
	raw: R"r(\s*(\S+)\s*=\s*(\w+)\s*\(\s*((?:\S+\s*,?\s*)+)\s*\)\s*\r?\n?)r"
	[3]re.2708.gate_input_regex
	content: \s*(\S+)\s*
	raw: R"r(\s*(\S+)\s*)r"
	[4]re.5730.comment_regex
	content: \s*#.*\r?
	raw: R"r(\s*#.*\r?)r"
	[5]re.9989.empty_regex
	content: \s+
	raw: R"r(\s+)r"

(23) dertuxmalwieder_MediaEmbedderClass.cpp[5]
	[0]re.2389.re_url
	content: ^https?://[^\s\"<>]+/?$
	raw: "^" + url_regex + "/?$"
	[1]re.5082.re_url_inline
	content: https?://[^\s\"<>]+
	raw: url_regex
	[2]re.9031.re_oembed_json
	content: <link.*?type=\"application/json\+oembed\".*?>
	raw: "<link.*?type=\"application/json\\+oembed\".*?>"
	[3]re.1205.re_oembed_xml
	content: <link.*?type=\"text/xml\+oembed\".*?>
	raw: "<link.*?type=\"text/xml\\+oembed\".*?>"
	[4]re.7637.amp
	content: &amp;
	raw: "&amp;"

(24) OlivierLDff_Network.cpp[3]
	[0]re.85.regex
	content: ^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$
	raw: "^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\."
                                  "(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\."
                                  "(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\."
                                  "(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$"
	[1]re.4881.regex
	content: ((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])(\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])(\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])(\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])(\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])(\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])(\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])(\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])){3}))|:)))(%.+)?
	raw: "((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|"
        "(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|"
        "((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])"
        "(\\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])){3})|:))|"
        "(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|"
        ":((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])"
        "(\\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])){3})|:))|"
        "(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|"
        "((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])"
        "(\\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])){3}))|:))|"
        "(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|"
        "((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])"
        "(\\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])){3}))|:))|"
        "(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|"
        "((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])"
        "(\\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])){3}))|:))|"
        "(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|"
        "((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])"
        "(\\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])){3}))|:))|"
        "(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:"
        "((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])"
        "(\\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])){3}))|:)))(%.+)?"
	[2]re.263.regex
	content: ^([0-9a-fA-F][0-9a-fA-F])(?::)([0-9a-fA-F][0-9a-fA-F])(?::)([0-9a-fA-F][0-9a-fA-F])(?::)([0-9a-fA-F][0-9a-fA-F])(?::)([0-9a-fA-F][0-9a-fA-F])(?::)([0-9a-fA-F][0-9a-fA-F])$
	raw: "^([0-9a-fA-F][0-9a-fA-F])(?::)"
                                  "([0-9a-fA-F][0-9a-fA-F])(?::)"
                                  "([0-9a-fA-F][0-9a-fA-F])(?::)"
                                  "([0-9a-fA-F][0-9a-fA-F])(?::)"
                                  "([0-9a-fA-F][0-9a-fA-F])(?::)"
                                  "([0-9a-fA-F][0-9a-fA-F])$"

(25) yyhtbs_face-uri.cpp[6]
	[0]re.460.protocolExp
	content: (\w+\d?(\+\w+)?)://([^/]*)(\/[^?]*)?
	raw: "(\\w+\\d?(\\+\\w+)?)://([^/]*)(\\/[^?]*)?"
	[1]re.3469.v6LinkLocalExp
	content: ^\[([a-fA-F0-9:]+)%([^\s/:]+)\](?:\:(\d+))?$
	raw: "^\\[([a-fA-F0-9:]+)%([^\\s/:]+)\\](?:\\:(\\d+))?$"
	[2]re.4308.v6Exp
	content: ^\[([a-fA-F0-9:]+)\](?:\:(\d+))?$
	raw: "^\\[([a-fA-F0-9:]+)\\](?:\\:(\\d+))?$"
	[3]re.8108.etherExp
	content: ^\[((?:[a-fA-F0-9]{1,2}\:){5}(?:[a-fA-F0-9]{1,2}))\]$
	raw: "^\\[((?:[a-fA-F0-9]{1,2}\\:){5}(?:[a-fA-F0-9]{1,2}))\\]$"
	[4]re.8640.v4MappedV6Exp
	content: ^\[::ffff:(\d+(?:\.\d+){3})\](?:\:(\d+))?$
	raw: "^\\[::ffff:(\\d+(?:\\.\\d+){3})\\](?:\\:(\\d+))?$"
	[5]re.2614.v4HostExp
	content: ^([^:]+)(?:\:(\d+))?$
	raw: "^([^:]+)(?:\\:(\\d+))?$"

(26) toddcbryant_fastiptv_stypes.cpp[2]
	[0]re.3398.digit_end
	content: ^([^_]+_)+([0-9]+)$
	raw: "^([^_]+_)+([0-9]+)$"
	[1]re.9522.chunk_index
	content: ^[0-9]+_([0-9]+).ts$
	raw: "^[0-9]+_([0-9]+)" CHUNK_EXT "$"

(27) alvyC_face-uri.cpp[6]
	[0]re.2492.protocolExp
	content: (\w+\d?(\+\w+)?)://([^/]*)(\/[^?]*)?
	raw: "(\\w+\\d?(\\+\\w+)?)://([^/]*)(\\/[^?]*)?"
	[1]re.4875.v6LinkLocalExp
	content: ^\[([a-fA-F0-9:]+)%([^\s/:]+)\](?:\:(\d+))?$
	raw: "^\\[([a-fA-F0-9:]+)%([^\\s/:]+)\\](?:\\:(\\d+))?$"
	[2]re.3145.v6Exp
	content: ^\[([a-fA-F0-9:]+)\](?:\:(\d+))?$
	raw: "^\\[([a-fA-F0-9:]+)\\](?:\\:(\\d+))?$"
	[3]re.1667.etherExp
	content: ^\[((?:[a-fA-F0-9]{1,2}\:){5}(?:[a-fA-F0-9]{1,2}))\]$
	raw: "^\\[((?:[a-fA-F0-9]{1,2}\\:){5}(?:[a-fA-F0-9]{1,2}))\\]$"
	[4]re.9862.v4MappedV6Exp
	content: ^\[::ffff:(\d+(?:\.\d+){3})\](?:\:(\d+))?$
	raw: "^\\[::ffff:(\\d+(?:\\.\\d+){3})\\](?:\\:(\\d+))?$"
	[5]re.6190.v4HostExp
	content: ^([^:]+)(?:\:(\d+))?$
	raw: "^([^:]+)(?:\\:(\\d+))?$"

(28) userghmrt-testrepo-src-test-time_utils.cpp[1]
	[0]re.627.re
	content: ^(?:[1-9]\d{3}-(?:(?:0[1-9]|1[0-2])-(?:0[1-9]|1\d|2[0-8])|(?:0[13-9]|1[0-2])-(?:29|30)|(?:0[13578]|1[02])-31)|(?:[1-9]\d(?:0[48]|[2468][048]|[13579][26])|(?:[2468][048]|[13579][26])00)-02-29)T(?:[01]\d|2[0-3]):[0-5]\d:[0-5]\d(?:Z|[+-][01]\d:[0-5]\d)$
	raw: "^" + year_regexp + "-" + month_regexp + "-" + day_regexp + "T" + hour + timeshift + "$"

(29) i1bro_btimetable_src_AddingEvents.cpp[5]
	[0]re.4790.inputPattern
	content: ^\s{0,}[1-2]{1,1}\s{0,}
	raw: R"(^\s{0,}[1-2]{1,1}\s{0,})"
	[1]re.4610.inputPattern
	content: ^\s{0,}[a-z,A-Z]{1,20}\s{1,}[a-z,A-Z]{1,20}\s{0,}
	raw: R"(^\s{0,}[a-z,A-Z]{1,20}\s{1,}[a-z,A-Z]{1,20}\s{0,})"
	[2]re.7679.inputPattern
	content: ^\s{0,}[0-9]{2,2}[.][0-9]{2,2}[.][0-9]{4,4}\s{0,}
	raw: R"(^\s{0,}[0-9]{2,2}[.][0-9]{2,2}[.][0-9]{4,4}\s{0,})"
	[3]re.4153.inputPattern
	content: ^\s{0,}[0-9]{2,2}[.][0-9]{2,2}\s{0,}
	raw: R"(^\s{0,}[0-9]{2,2}[.][0-9]{2,2}\s{0,})"
	[4]re.7364.inputPattern
	content: ^\s{0,}[0-9]{1,}\s{0,}
	raw: R"(^\s{0,}[0-9]{1,}\s{0,})"

(30) YADRO-KNS_manifest.cpp[2]
	[0]re.3101.iniRegex
	content: ([^= ]+)\s*=\s*\"?([^\"]+)\"?
	raw: "([^= ]+)\\s*=\\s*\"?([^\"]+)\"?"
	[1]re.9551.verRegex
	content: v([0-9]+)\.([0-9]+).*
	raw: "v([0-9]+)\\.([0-9]+).*"

(31) xalanq_utils.cpp[3]
	[0]re.8391.patternUsername
	content: [\w\.\-]+
	raw: "[\\w\\.\\-]+"
	[1]re.2546.patternEmail
	content: (\w+)(\.|_)?(\w*)@(\w+)(\.(\w+))+
	raw: "(\\w+)(\\.|_)?(\\w*)@(\\w+)(\\.(\\w+))+"
	[2]re.6393.specialChars
	content: [.^$|()\[\]{}*+?\\\&]
	raw: R"([.^$|()\[\]{}*+?\\\&])"

(32) luboO_valueparser.cpp[3]
	[0]re.2530.RE_FLOAT
	content: ([-+]?)([0-9]+)(:?.([0-9]+))
	raw: "([-+]?)([0-9]+)(:?.([0-9]+))"
	[1]re.9199.RE_INTEGER
	content: ([-+]?)([0-9]+)
	raw: "([-+]?)([0-9]+)"
	[2]re.172.RE_UINTEGER
	content: ([0-9]+)
	raw: "([0-9]+)"

(33) Soundux_youtube-dl.cpp[2]
	[0]re.582.YoutubeDl::urlRegex
	content: https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&\/\/=]*)
	raw: R"(https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&\/\/=]*))"
	[1]re.6877.progressRegex
	content: ([0-9.,]+)%.*(ETA (.+))
	raw: R"(([0-9.,]+)%.*(ETA (.+)))"

(34) SteveWolligandt_shaderstage.cpp[2]
	[0]re.2198.shaderstage::regex_nvidia_compiler_error
	content: \d+\((\d+)\)\s*:\s*(error|warning)\s*\w*:\s*(.*)
	raw: R"(\d+\((\d+)\)\s*:\s*(error|warning)\s*\w*:\s*(.*))"
	[1]re.8423.shaderstage::regex_mesa_compiler_error
	content: \d+:(\d+)\(\d+\)\s*:\s*(error|warning)\s*\w*:\s*(.*)
	raw: R"(\d+:(\d+)\(\d+\)\s*:\s*(error|warning)\s*\w*:\s*(.*))"

(35) libretro_cdromtoc.cpp[7]
	[0]re.652.FILE_REGEX
	content: ^[ \t]*FILE[ \t]+\"(.*)\"[ \t]+([^ \t]+)[ \t]*$
	raw: "^[ \\t]*FILE[ \\t]+\"(.*)\"[ \\t]+([^ \\t]+)[ \\t]*$"
	[1]re.8895.TRACK_REGEX
	content: ^[ \t]*TRACK[ \t]+([0-9]+)[ \t]+([^ \t]*)[ \t]*$
	raw: "^[ \\t]*TRACK[ \\t]+([0-9]+)[ \\t]+([^ \\t]*)[ \\t]*$"
	[2]re.7302.PREGAP_REGEX
	content: ^[ \t]*PREGAP[ \t]+([0-9]+):([0-9]+):([0-9]+)[ \t]*$
	raw: "^[ \\t]*PREGAP[ \\t]+([0-9]+):([0-9]+):([0-9]+)[ \\t]*$"
	[3]re.1918.INDEX_REGEX
	content: ^[ \t]*INDEX[ \t]+([0-9]+)[ \t]+([0-9]+):([0-9]+):([0-9]+)[ \t]*$
	raw: "^[ \\t]*INDEX[ \\t]+([0-9]+)[ \\t]+([0-9]+):([0-9]+):([0-9]+)[ \\t]*$"
	[4]re.4052.POSTGAP_REGEX
	content: ^[ \t]*POSTGAP[ \t]+([0-9]+):([0-9]+):([0-9]+)[ \t]*$
	raw: "^[ \\t]*POSTGAP[ \\t]+([0-9]+):([0-9]+):([0-9]+)[ \\t]*$"
	[5]re.5278.CHTR_REGEX
	content: .*TRACK:([0-9]+) TYPE:(.*) SUBTYPE:(.*) FRAMES:([0-9]+).*
	raw: ".*TRACK:([0-9]+) TYPE:(.*) SUBTYPE:(.*) FRAMES:([0-9]+).*"
	[6]re.7352.CHT2_REGEX
	content: .*TRACK:([0-9]+) TYPE:(.*) SUBTYPE:(.*) FRAMES:([0-9]+) PREGAP:([0-9]+) PGTYPE:(.*) PGSUB:(.*) POSTGAP:([0-9]+).*
	raw: ".*TRACK:([0-9]+) TYPE:(.*) SUBTYPE:(.*) FRAMES:([0-9]+) PREGAP:([0-9]+) PGTYPE:(.*) PGSUB:(.*) POSTGAP:([0-9]+).*"

(36) ZJUNlict_paraminterface.cpp[3]
	[0]re.4382.boolExp
	content: true|false|t|f
	raw: "true|false|t|f"
	[1]re.496.doubleExp
	content: ^(-?)(0|([1-9][0-9]*))(\.[0-9]+)?$
	raw: "^(-?)(0|([1-9][0-9]*))(\\.[0-9]+)?$"
	[2]re.5412.integerExp
	content: ^(0|[1-9][0-9]*)$
	raw: "^(0|[1-9][0-9]*)$"

(37) pohly_syncevolution_Exception.cpp[2]
	[0]re.5113.re
	content: .* \((?:local|remote), status (\d+)\)(?:: ([\s\S]*))
	raw: ".* \\((?:local|remote), status (\\d+)\\)(?:: ([\\s\\S]*))"
	[1]re.5634.re
	content: (org\.syncevolution(?:\.\w+)+): ([\s\S]*)
	raw: "(org\\.syncevolution(?:\\.\\w+)+): ([\\s\\S]*)"

(38) melak47_sms.cpp[2]
	[0]re.7951.module
	content: \s*module\s+([\w:\.]+)\s*;\s*
	raw: R"(\s*module\s+([\w:\.]+)\s*;\s*)"
	[1]re.5124.import
	content: \s*(?:export\s+)?import\s+(?:([\w:\.]+)|"([\w\.]+)"|<([\w\.]+)>)\s*;\s*
	raw: R"~~(\s*(?:export\s+)?import\s+(?:([\w:\.]+)|"([\w\.]+)"|<([\w\.]+)>)\s*;\s*)~~"


