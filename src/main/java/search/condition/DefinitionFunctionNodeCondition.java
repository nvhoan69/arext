package search.condition;

import parser.object.DefinitionFunctionNode;
import parser.object.INode;
import search.SearchCondition;

/**
 * Demo a condition
 *
 * 
 */
public class DefinitionFunctionNodeCondition extends SearchCondition {

    @Override
    public boolean isSatisfiable(INode n) {
        return n instanceof DefinitionFunctionNode;
    }
}
