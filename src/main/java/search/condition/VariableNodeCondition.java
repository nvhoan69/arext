package search.condition;

import parser.object.CloneVariableNode;
import parser.object.INode;
import parser.object.VariableNode;
import search.SearchCondition;

public class VariableNodeCondition extends SearchCondition {

    @Override
    public boolean isSatisfiable(INode n) {
        return n instanceof VariableNode && !(n instanceof CloneVariableNode);
    }
}
