package search.condition;

import parser.object.INode;
import parser.object.TypedefDeclaration;
import search.SearchCondition;

public class TypedefNodeCondition extends SearchCondition {

    @Override
    public boolean isSatisfiable(INode n) {
        return n instanceof TypedefDeclaration;
    }
}
