package search.condition;

import parser.object.INode;
import parser.object.MacroDefinitionNode;
import search.SearchCondition;

public class MacroDefinitionNodeCondition extends SearchCondition {

    @Override
    public boolean isSatisfiable(INode n) {
        return n instanceof MacroDefinitionNode;
    }
}
