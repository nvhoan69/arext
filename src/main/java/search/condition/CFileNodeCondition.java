package search.condition;

import parser.object.CFileNode;
import parser.object.INode;
import search.SearchCondition;

public class CFileNodeCondition extends SearchCondition {

    @Override
    public boolean isSatisfiable(INode n) {
        return n instanceof CFileNode;
    }
}
