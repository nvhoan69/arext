package search.condition;

import parser.object.INode;
import parser.object.UnionNode;
import search.SearchCondition;

public class UnionNodeCondition extends SearchCondition {

    @Override
    public boolean isSatisfiable(INode n) {
        return n instanceof UnionNode;
    }
}
