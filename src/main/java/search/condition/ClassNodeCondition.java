package search.condition;

import parser.object.ClassNode;
import parser.object.INode;
import search.SearchCondition;

public class ClassNodeCondition extends SearchCondition {

    @Override
    public boolean isSatisfiable(INode n) {
        return n instanceof ClassNode;
    }
}
