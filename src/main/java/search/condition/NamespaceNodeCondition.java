package search.condition;

import parser.object.INode;
import parser.object.NamespaceNode;
import search.SearchCondition;

public class NamespaceNodeCondition extends SearchCondition {

    @Override
    public boolean isSatisfiable(INode n) {
        return n instanceof NamespaceNode;
    }
}
