package search.condition;

import parser.object.AliasDeclaration;
import parser.object.INode;
import search.SearchCondition;

public class AliasNodeCondition extends SearchCondition {

    @Override
    public boolean isSatisfiable(INode n) {
        return n instanceof AliasDeclaration;
    }
}
