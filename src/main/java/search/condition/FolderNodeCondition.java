package search.condition;

import parser.object.FolderNode;
import parser.object.INode;
import search.SearchCondition;


public class FolderNodeCondition extends SearchCondition {

    @Override
    public boolean isSatisfiable(INode n) {
        return n instanceof FolderNode;
    }
}
