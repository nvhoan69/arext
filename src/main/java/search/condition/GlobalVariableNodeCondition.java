package search.condition;

import parser.object.ExternalVariableNode;
import parser.object.INode;
import search.SearchCondition;

/**
 * Represent global or extern variable, e.g., "int MY_MAX_VALUE"
 *
 *
 */
public class GlobalVariableNodeCondition extends SearchCondition {

    @Override
    public boolean isSatisfiable(INode n) {
        return n instanceof ExternalVariableNode;
    }
}
