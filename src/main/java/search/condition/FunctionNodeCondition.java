package search.condition;

import parser.object.IFunctionNode;
import parser.object.INode;
import search.SearchCondition;

/**
 * Demo a condition
 *
 * 
 */
public class FunctionNodeCondition extends SearchCondition {

    @Override
    public boolean isSatisfiable(INode n) {
        return n instanceof IFunctionNode;
    }
}
