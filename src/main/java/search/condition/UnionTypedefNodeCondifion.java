package search.condition;

import parser.object.INode;
import parser.object.UnionTypedefNode;
import search.SearchCondition;

/**
 * Created by DucToan on 14/07/2017.
 */
public class UnionTypedefNodeCondifion extends SearchCondition {
    @Override
    public boolean isSatisfiable(INode n) {
        return n instanceof UnionTypedefNode;
    }
}
