package search.condition;

import parser.object.ClassNode;
import parser.object.INode;
import parser.object.NamespaceNode;
import parser.object.StructNode;
import search.SearchCondition;

public class ClassvsStructvsNamespaceCondition extends SearchCondition {

    @Override
    public boolean isSatisfiable(INode n) {
        return n instanceof ClassNode || n instanceof StructNode || n instanceof NamespaceNode;
    }
}
