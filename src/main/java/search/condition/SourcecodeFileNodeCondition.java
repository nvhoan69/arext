package search.condition;


import parser.object.INode;
import parser.object.ISourcecodeFileNode;
import search.SearchCondition;

public class SourcecodeFileNodeCondition extends SearchCondition {

    @Override
    public boolean isSatisfiable(INode n) {
        return n instanceof ISourcecodeFileNode;
    }
}
