package search.condition;

import parser.object.INode;
import parser.object.Node;
import search.SearchCondition;

public class NodeCondition extends SearchCondition {

    @Override
    public boolean isSatisfiable(INode n) {
        return n instanceof Node;
    }
}
