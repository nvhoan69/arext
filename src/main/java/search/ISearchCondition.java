package search;

import parser.object.INode;

public interface ISearchCondition {

    boolean isSatisfiable(INode n);

}