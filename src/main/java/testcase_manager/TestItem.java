package testcase_manager;

import util.DateTimeUtils;
import util.SpecialCharacter;

import java.time.LocalDateTime;

public abstract class TestItem implements ITestItem {
    // name of test case
    private String name;

    private LocalDateTime creationDateTime;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = removeSpecialCharacter(name);

    }

    @Override
    public void setCreationDateTime(LocalDateTime creationDateTime) {
        this.creationDateTime = creationDateTime;
    }

    @Override
    public LocalDateTime getCreationDateTime() {
        return creationDateTime;
    }

    @Override
    public String getCreationDate() {
        return DateTimeUtils.getDate(creationDateTime);
    }

    @Override
    public String getCreationTime() {
        return DateTimeUtils.getTime(creationDateTime);
    }

    public static String removeSysPathInName(String path) {
        // name could not have File.separator
        return path.replaceAll("operator\\s*/", "operator_division");
    }

    public static String removeSpecialCharacter(String name) {
        return name
                .replace("+", "plus")
                .replace("-", "minus")
                .replace("*", "mul")
                .replace("/", "div")
                .replace("%", "mod")
                .replace("=", "equal")
                .replaceAll("[^\\w]", SpecialCharacter.UNDERSCORE)
                .replaceAll("[_]+", SpecialCharacter.UNDERSCORE);
    }
}
