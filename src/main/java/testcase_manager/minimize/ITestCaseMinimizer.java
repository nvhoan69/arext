package testcase_manager.minimize;

import testcase_manager.TestCase;

import java.util.List;

public interface ITestCaseMinimizer {

    List<TestCase> clean(List<TestCase> testCases, Scope scope);

    List<TestCase> minimize(List<TestCase> testCases, Scope scope) throws Exception;
}
