package testcase_manager.minimize;

import parser.object.ICommonFunctionNode;
import testcase_manager.ITestCase;
import testcase_manager.TestCase;
import testcase_manager.TestCaseManager;
import util.VFPLogger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public abstract class TestCaseMinimizer implements ITestCaseMinimizer {

    public static final VFPLogger logger = VFPLogger.get(TestCaseMinimizer.class);

    public List<TestCase> clean(List<TestCase> testCases, Scope scope) {
        final int originSize = testCases.size();

        List<TestCase> results = new ArrayList<>();

        logger.debug("Grouping test case by subprogram");
        Map<ICommonFunctionNode, List<TestCase>> group = groupTestCaseByFunction(testCases);

        for (Map.Entry<ICommonFunctionNode, List<TestCase>> entry : group.entrySet()) {
            try {
                List<TestCase> list = entry.getValue();
                if (list.size() > 1) {
                    long before = System.nanoTime();

                    List<TestCase> optimizes = minimize(list, scope);
                    results.addAll(optimizes);

                    long after = System.nanoTime();
                    long executedTime = after - before;

                    logger.debug("Executed Time: " + executedTime);

                    List<TestCase> unnecessary = list.stream()
                            .filter(tc -> !optimizes.contains(tc))
                            .collect(Collectors.toList());

                    deleteTestCase(unnecessary);
                } else {
                    results = testCases;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        logger.debug("Optimize test cases done: " + originSize + " -> " + results.size());
        return results;
    }


    private void deleteTestCase(List<TestCase> unnecessary) {
        for (TestCase testCase : unnecessary) {
            TestCaseManager.removeBasicTestCase(testCase.getName());
        }
    }

    // Grouping test case by subprogram under test
    protected static Map<ICommonFunctionNode, List<TestCase>> groupTestCaseByFunction(List<TestCase> testCases) {
        Map<ICommonFunctionNode, List<TestCase>> map = new HashMap<>();

        for (ITestCase testCase : testCases) {
            if (testCase instanceof TestCase) {
                ICommonFunctionNode sut = ((TestCase) testCase).getFunctionNode();

                List<TestCase> list = map.get(sut);
                if (list == null)
                    list = new ArrayList<>();

                if (!list.contains(testCase))
                    list.add((TestCase) testCase);

                map.put(sut, list);
            }
        }

        return map;
    }
}
