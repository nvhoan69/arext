package testcase_manager.minimize;

import auto_testcase_generation.cfg.object.ICfgNode;
import testcase_manager.TestCase;
import util.TestPathUtils;

import java.util.*;

public class GreedyMinimizer extends TestCaseMinimizer {

    @Override
    public List<TestCase> minimize(List<TestCase> testCases, Scope scope) {
        List<TestCase> minimizedSet = new ArrayList<>();

        if (!testCases.isEmpty()) {

            List<ICfgNode> allNodes = new ArrayList<>();
            Map<TestCase, List<ICfgNode>> subsets = new HashMap<>();

            testCases.forEach(tc -> {
                logger.debug("Gather information in " + tc.getName());
                List<ICfgNode> visitedNodes = TestPathUtils.getVisited(tc, scope);
                visitedNodes.forEach(n -> {
                    if (!allNodes.contains(n))
                        allNodes.add(n);
                });
                subsets.put(tc, visitedNodes);
            });

            List<ICfgNode> uncoveredNodes = new ArrayList<>(allNodes);

            while (!uncoveredNodes.isEmpty()) {
                /* select a subset that has the maximum number of uncovered elements */
                /*
                 * select selectedNodes belong to subsets
                 * that |selectedNodes communicate uncoveredNodes| is maximum
                 */
                Map.Entry<TestCase, List<ICfgNode>> selectedNodes = subsets.entrySet().stream()
                        .max(new Comparator<Map.Entry<TestCase, List<ICfgNode>>>() {
                            @Override
                            public int compare(Map.Entry<TestCase, List<ICfgNode>> o1, Map.Entry<TestCase, List<ICfgNode>> o2) {
//                                logger.debug("Comparing test case " + o1.getKey().getName() + " with " + o2.getKey().getName());
                                List<ICfgNode> list1 = o1.getValue();
                                List<ICfgNode> list2 = o2.getValue();
                                int common1 = countCommon(list1, uncoveredNodes);
                                int common2 = countCommon(list2, uncoveredNodes);
                                return Integer.compare(common1, common2);
                            }
                        })
                        .orElse(null);

                if (selectedNodes == null)
                    break;

                uncoveredNodes.removeIf(n -> selectedNodes.getValue().contains(n));
                logger.debug("Select " + selectedNodes.getKey().getName());
                minimizedSet.add(selectedNodes.getKey());
            }
        }

        return minimizedSet;
    }

    private int countCommon(List<ICfgNode> selectedNodes, List<ICfgNode> uncoveredNodes) {
        List<ICfgNode> common = new ArrayList<>(selectedNodes);
        common.retainAll(uncoveredNodes);
        return common.size();
    }
}
