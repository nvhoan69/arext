package testcase_manager;

import compiler.Compiler;
import config.CommandConfig;
import config.WorkspaceConfig;
import environment.Environment;
import parser.IProjectLoader;
import parser.object.HeaderNode;
import parser.object.INode;
import project_init.ProjectClone;
import util.*;

import java.io.File;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractTestCase extends TestItem implements ITestCase {

    private final static VFPLogger logger = VFPLogger.get(AbstractTestCase.class);

    // some test cases need to be added some specified headers
    private String additionalHeaders = SpecialCharacter.EMPTY;

    // Not executed (by default)
    private String status = TestCase.STATUS_NA;

    // the file containing the test path after executing this test case
    private String testPathFile;
    private String executableFile;

    private LocalDateTime executionDateTime;

    private String executeLog = SpecialCharacter.EMPTY;

    private double executedTime;

    public void setExecutionDateTime(LocalDateTime executionDateTime) {
        this.executionDateTime = executionDateTime;
    }

    public LocalDateTime getExecutionDateTime() {
        return executionDateTime;
    }

    public String getExecutionDate() {
        return DateTimeUtils.getDate(executionDateTime);
    }

    public String getExecutionTime() {
        return DateTimeUtils.getTime(executionDateTime);
    }

    @Override
    public boolean isPrototypeTestcase() {
        return false;
    }

    // Is a part of test driver
    // The test driver of a test case has two files.
    // This file contains the main function to run a test case
    // This file is stored in {working-directory}/testdrivers)
    private String sourcecodeFile;

    /**
     * Delete all files related to the test case except the file storing the value of test case
     */
    public void deleteOldDataExceptValue() {
        // todo: need to validate and set coverage, progress coverage file for testcase
        logger.debug("Deleting all files related to the test case " + getName() + " before continuing");
        if (getTestPathFile() != null)
            Utils.deleteFileOrFolder(new File(this.getTestPathFile()));
        if (getExecutableFile() != null)
            Utils.deleteFileOrFolder(new File(this.getExecutableFile()));

        /*
         * Delete test drivers
         */
        if (getSourceCodeFile() != null) {
            Utils.deleteFileOrFolder(new File(getSourceCodeFile()));
            String outFile = getSourceCodeFile() + Environment.getInstance().getCompiler().getOutputExtension();
            Utils.deleteFileOrFolder(new File(outFile));
        }
    }

    /**
     * Given a test case, we try to generate compilation commands + linking command.
     * <p>
     * The original project has its own commands, all the thing we need to do right now is
     * modify these commands.
     */
    @Override
    public CommandConfig generateCommands() {
        CommandConfig config = new CommandConfig();
        Compiler compiler = Environment.getInstance().getCompiler();

        // step 1: generate compilation command
        String relativePath = PathUtils.toRelative(getSourceCodeFile());
        String newCompileCommand = compiler.generateCompileCommand(relativePath);
        newCompileCommand += SpecialCharacter.SPACE + generateDefinitionCompileCmd();

        config.getCompilationCommands().put(relativePath, newCompileCommand);

        // step 2: generate linking command
        String relativeExePath = PathUtils.toRelative(executableFile);
        CommandConfig.LinkEntry linkEntry = generateLinkEntry(compiler, relativeExePath, relativePath);
        config.setLinkEntry(linkEntry);

        // step 3: set executable file path
        config.setExecutablePath(relativeExePath);

        return config;
    }

    protected abstract List<INode> getAllRelatedFileToLink();

    private CommandConfig.LinkEntry generateLinkEntry(Compiler compiler, String executableFile, String sourceFile) {
        CommandConfig.LinkEntry linkEntry = new CommandConfig.LinkEntry();
        linkEntry.setCommand(compiler.getLinkCommand());
        linkEntry.setOutFlag(compiler.getOutputFlag());
        linkEntry.setExeFile(executableFile);

        List<String> projectBinFiles = Environment.getInstance().getLinkEntry().getBinFiles();
        List<String> testBinFiles = new ArrayList<>(projectBinFiles);

        List<INode> sourceNodes = getAllRelatedFileToLink();

        sourceNodes.stream().filter(n -> !(n instanceof HeaderNode)).forEach(n -> {
            String originBinFile = CompilerUtils.getOutfilePath(n.getAbsolutePath(), compiler.getOutputExtension());
            originBinFile = PathUtils.toRelative(originBinFile);
            testBinFiles.remove(originBinFile);
        });

        List<String> additionalIncludes = getAdditionalIncludes();
        additionalIncludes.forEach(i -> {
            String originFile = ProjectClone.getOriginFilePath(i);
            String originBinFile = CompilerUtils.getOutfilePath(originFile, compiler.getOutputExtension());
            originBinFile = PathUtils.toRelative(originBinFile);
            testBinFiles.remove(originBinFile);
        });

        String outputFilePath = CompilerUtils.getOutfilePath(sourceFile, compiler.getOutputExtension());
        outputFilePath = PathUtils.toRelative(outputFilePath);
        testBinFiles.add(outputFilePath);

        linkEntry.setBinFiles(testBinFiles);

        return linkEntry;
    }

    protected abstract String generateDefinitionCompileCmd();

    public void deleteOldData() {
        deleteOldDataExceptValue();
    }

    @Override
    public String getSourceCodeFile() {
        return sourcecodeFile;
    }

    @Override
    public void setSourceCodeFile(String sourcecodeFile) {
        this.sourcecodeFile = removeSysPathInName(sourcecodeFile);
    }

    @Override
    public String getStatus() {
        return status;
    }

    @Override
    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String getTestPathFile() {
        return testPathFile;
    }

    @Override
    public void setTestPathFile(String testPathFile) {
        this.testPathFile = removeSysPathInName(testPathFile);
    }

    @Override
    public String getExecutableFile() {
        return executableFile;
    }

    @Override
    public void setExecutableFile(String executableFile) {
        this.executableFile = removeSysPathInName(executableFile);
    }

    public void setSourcecodeFileDefault() {
        String srcFile = WorkspaceConfig.TEST_DRIVER_DIR.getAbsolutePath() + File.separator + removeSpecialCharacter(getName());
        if (Environment.getInstance().isC())
            srcFile += IProjectLoader.C_FILE_SYMBOL;
        else
            srcFile += IProjectLoader.CPP_FILE_SYMBOL;
        setSourceCodeFile(srcFile);
    }

    @Override
    public void setTestPathFileDefault() {
        String testpathFile = WorkspaceConfig.TEST_PATH_DIR.getAbsolutePath() + File.separator + removeSpecialCharacter(getName()) + ".tp";
        setTestPathFile(testpathFile);
    }

    @Override
    public void setExecutableFileDefault() {
        String executableFile = WorkspaceConfig.EXE_DIR.getAbsolutePath() + File.separator + removeSpecialCharacter(getName()) + ".exe";
        setExecutableFile(executableFile);
    }


    @Override
    public String toString() {
        return String.format("%s (%s)", getName(), status);
    }

    public String getAdditionalHeaders() {
        return additionalHeaders;
    }

    public void setAdditionalHeaders(String additionalHeaders) {
        this.additionalHeaders = additionalHeaders;
    }

    public void appendAdditionHeader(String includeStm) {
        if (additionalHeaders == null)
            additionalHeaders = includeStm;
        else if (!additionalHeaders.contains(includeStm))
            additionalHeaders += SpecialCharacter.LINE_BREAK + includeStm;
    }

    public String getExecuteLog() {
        if (executeLog != null)
            return executeLog.trim();
        return null;
    }

    public void setExecuteLog(String executeLog) {
        this.executeLog = executeLog;
    }

    public double getExecutedTime() {
        return executedTime;
    }

    public void setExecutedTime(double executedTime) {
        this.executedTime = executedTime;
    }
}
