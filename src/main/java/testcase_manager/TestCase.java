package testcase_manager;

import auto_testcase_generation.track.QuantityTracker;
import parser.dependency.IncludeHeaderDependency;
import parser.funcdetail.FunctionDetailTree;
import parser.object.ICommonFunctionNode;
import parser.object.INode;
import project_init.ProjectClone;
import search.Search2;
import testdata.DataTree;
import testdata.object.*;
import user_code.UserCodeManager;
import user_code.objects.AbstractUserCode;
import user_code.objects.ParameterUserCode;
import user_code.objects.UsedParameterUserCode;
import util.NodeType;
import util.SpecialCharacter;
import util.Utils;
import util.VFPLogger;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Represent a single test case
 */
public class TestCase extends AbstractTestCase implements IDataTestItem {

    private final static VFPLogger logger = VFPLogger.get(TestCase.class);

    private RootDataNode rootDataNode;
    private ICommonFunctionNode functionNode;
    // map input to expected output of global variables
    private Map<ValueDataNode, ValueDataNode> globalInputExpOutputMap = new HashMap<>();
    // map data node to include paths of the data node
    private final Map<DataNode, List<String>> additionalIncludePathsMap = new HashMap<>();

    public TestCase(ICommonFunctionNode functionNode, String name) {
        setName(name);
        FunctionDetailTree functionDetailTree = new FunctionDetailTree(functionNode);
        DataTree dataTree = new DataTree(functionDetailTree);
        rootDataNode = dataTree.getRoot();
        setFunctionNode(functionNode);
        QuantityTracker.getInstance().increase(functionNode);
    }

    public TestCase() {
    }

    public Map<ValueDataNode, ValueDataNode> getGlobalInputExpOutputMap() {
        return globalInputExpOutputMap;
    }

    // is only called when create new testcase
    public void initGlobalInputExpOutputMap() {
        try {
            globalInputExpOutputMap.clear();
            FunctionDetailTree functionDetailTree = new FunctionDetailTree(functionNode);
            DataTree dataTree = new DataTree(functionDetailTree);
            RootDataNode root = dataTree.getRoot();
//            RootDataNode root = getRootDataNode();

            // children of the global data node of the root are used to be expected output values
            RootDataNode newGlobalDataNode = getGlobalDataNode(root);
            GlobalRootDataNode globalDataNode = getGlobalDataNode(rootDataNode);
            if (newGlobalDataNode != null && globalDataNode != null) {
                mapGlobalInputToExpOutput(globalDataNode.getChildren(), newGlobalDataNode.getChildren());
                globalDataNode.setGlobalInputExpOutputMap(globalInputExpOutputMap);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private GlobalRootDataNode getGlobalDataNode(RootDataNode root) {
        for (IDataNode uut : root.getChildren()) {
            if (uut instanceof UnitUnderTestNode) {
                for (IDataNode dataNode : uut.getChildren()) {
                    if (dataNode instanceof GlobalRootDataNode
                            && ((RootDataNode) dataNode).getLevel().equals(NodeType.GLOBAL)) {
                        return (GlobalRootDataNode) dataNode;
                    }
                }
            }
        }
        return null;
    }

    private void mapGlobalInputToExpOutput(List<IDataNode> inputs, List<IDataNode> expOutputs) {
        if (inputs != null) {
            for (IDataNode inputValue : inputs) {
                for (IDataNode expectedOutput : expOutputs) {
                    if (expectedOutput.getName().equals(inputValue.getName())) {
                        ((ValueDataNode) expectedOutput).setExternel(false);
                        expectedOutput.setParent(inputValue.getParent());
                        globalInputExpOutputMap.put((ValueDataNode) inputValue, (ValueDataNode) expectedOutput);
                    }
                }
            }
        }
    }

    public void updateGlobalInputExpOutputAfterInportFromFile() {
        GlobalRootDataNode globalRootDataNode = getGlobalDataNode(rootDataNode);
        if (globalRootDataNode != null)
            globalInputExpOutputMap = globalRootDataNode.getGlobalInputExpOutputMap();
    }

    public RootDataNode getRootDataNode() {
        return rootDataNode;
    }

    public void setRootDataNode(RootDataNode rootDataNode) {
        this.rootDataNode = rootDataNode;
    }

    public boolean initParameterExpectedOutputs() {
        if (rootDataNode != null) {
            SubprogramNode sut = Search2.findSubprogramUnderTest(rootDataNode);
            if (sut != null) {
                // init parameter expected output datanodes
                sut.initInputToExpectedOutputMap();
                return true;
            }
        }

        return false;
    }

    public String getCloneSourcecodeFilePath() {
        // find the source code file containing the tested function
        INode sourcecodeFile = Utils.getSourcecodeFile(functionNode);

        // set up path for the cloned source code file
        return ProjectClone.getClonedFilePath(sourcecodeFile.getAbsolutePath());
    }

    /**
     * put or update data node and is include paths (used by user code) to map
     *
     * @param dataNode data node that use user code
     */
    public void putOrUpdateDataNodeIncludes(DataNode dataNode) {
        if (dataNode instanceof IUserCodeNode) {
            AbstractUserCode uc = ((IUserCodeNode) dataNode).getUserCode();
            if (uc instanceof UsedParameterUserCode) {
                UsedParameterUserCode userCode = (UsedParameterUserCode) uc;
                List<String> includePaths = new ArrayList<>();
                if (userCode.getType().equals(UsedParameterUserCode.TYPE_CODE)) {
                    includePaths.addAll(userCode.getIncludePaths());
                } else if (userCode.getType().equals(UsedParameterUserCode.TYPE_REFERENCE)) {
                    ParameterUserCode reference = UserCodeManager.getInstance()
                            .getParamUserCodeById(userCode.getId());
                    includePaths.addAll(reference.getIncludePaths());
                }

                if (additionalIncludePathsMap.containsKey(dataNode)) {
                    List<String> paths = additionalIncludePathsMap.get(dataNode);
                    paths.clear();
                    paths.addAll(includePaths);
                } else {
                    additionalIncludePathsMap.put(dataNode, includePaths);
                }
            } else {
                additionalIncludePathsMap.remove(dataNode);
            }
        }
    }

    public void putOrUpdateDataNodeIncludes(DataNode dataNode, String includePath) {
        if (dataNode instanceof IUserCodeNode) {
            additionalIncludePathsMap.remove(dataNode);
            List<String> paths = new ArrayList<>();
            paths.add(includePath);
            additionalIncludePathsMap.put(dataNode, paths);
        }
    }

    public ICommonFunctionNode getFunctionNode() {
        return functionNode;
    }

    public void setFunctionNode(ICommonFunctionNode functionNode) {
        this.functionNode = functionNode;
    }

    @Override
    public void setStatus(String status) {
        super.setStatus(status);
        if (status.equals(STATUS_NA)) {
            deleteOldDataExceptValue();
        }
    }

    /**
     * get all additional include headers used by user codes of test case
     */
    public List<String> getAdditionalIncludes() {
        // add from additionalIncludePathsMap
        List<String> allPaths = new ArrayList<>();
        for (Collection<String> collection : additionalIncludePathsMap.values()) {
            allPaths.addAll(collection);
        }

        return allPaths.stream().distinct().collect(Collectors.toList());
    }

    public Map<DataNode, List<String>> getAdditionalIncludePathsMap() {
        return additionalIncludePathsMap;
    }


    @Override
    protected List<INode> getAllRelatedFileToLink() {
        List<INode> sourceNodes = new ArrayList<>();

        INode sourceNode = Utils.getSourcecodeFile(functionNode);
        sourceNodes.add(sourceNode);

        sourceNode.getDependencies().stream()
                .filter(d -> d instanceof IncludeHeaderDependency && d.getStartArrow().equals(sourceNode))
                .forEach(d -> {
                    INode start = d.getEndArrow();
                    if (!sourceNodes.contains(start))
                        sourceNodes.add(start);
                });

        return sourceNodes;
    }

    @Override
    protected String generateDefinitionCompileCmd() {
        String defineName = getName().toUpperCase()
                .replace(SpecialCharacter.DOT, SpecialCharacter.UNDERSCORE_CHAR);

        return String.format("-DVFP_TC_%s", defineName);
    }
}
