package testcase_manager;

import environment.Environment;
import parser.object.ICommonFunctionNode;
import parser.object.IFunctionNode;
import search.Search;
import search.condition.AbstractFunctionNodeCondition;
import util.VFPLogger;

import java.time.LocalDateTime;
import java.util.*;

public class TestCaseManager {
    private final static VFPLogger logger = VFPLogger.get(TestCaseManager.class);
    public static Map<String, IDataTestItem> nameToBasicTestCaseMap = new HashMap<>();
    private static Map<ICommonFunctionNode, List<String>> functionToTestCasesMap = new HashMap<>();

    public static void clearMaps() {
        nameToBasicTestCaseMap.clear();
        functionToTestCasesMap.clear();
    }

    public static void initializeMaps() {
        List<IFunctionNode> functionNodes = Search
                .searchNodes(Environment.getInstance().getProjectNode(), new AbstractFunctionNodeCondition());
        for (IFunctionNode functionNode : functionNodes) {
            functionToTestCasesMap.put(functionNode, new ArrayList<>());
        }
    }

    public static TestPrototype createPrototype(String name, ICommonFunctionNode functionNode) {
        if (name == null || functionNode == null)
            return null;

        if (!TestCaseManager.checkTestCaseExisted(name)) {
            TestPrototype prototype = new TestPrototype(functionNode, name);

            prototype.setCreationDateTime(LocalDateTime.now());

            // need to validate name of testcase
            List<String> testcaseNames = functionToTestCasesMap.get(functionNode);
            if (testcaseNames != null) {
                testcaseNames.add(name);
                nameToBasicTestCaseMap.put(name, prototype);
            } else {
                logger.error("Can not find list testcase names correspond to functionNode: " + functionNode.getAbsolutePath() + "when create test case");
            }

            return prototype;
        } else {
            return null;
        }
    }


    public static TestCase createTestCase(String name, ICommonFunctionNode functionNode) {
        if (name == null || functionNode == null)
            return null;

        if (!TestCaseManager.checkTestCaseExisted(name)) {
            TestCase testCase = new TestCase(functionNode, name);

            testCase.setCreationDateTime(LocalDateTime.now());

            // need to validate name of testcase
            List<String> testcaseNames = functionToTestCasesMap.get(functionNode);
            if (testcaseNames != null) {
                testcaseNames.add(name);
                nameToBasicTestCaseMap.put(name, testCase);
            } else {
                logger.error("Can not find list testcase names correspond to functionNode: " + functionNode.getAbsolutePath() + "when create test case");
            }

            // init parameter expected outputs
            logger.debug("initParameterExpectedOuputs");
            testCase.initParameterExpectedOutputs();

            logger.debug("initGlobalInputExpOutputMap");
            testCase.initGlobalInputExpOutputMap();

            return testCase;
        } else {
            return null;
        }
    }

    public static TestCase createTestCase(ICommonFunctionNode functionNode, String nameTestcase) {
        TestCase testCase;
        String testCaseName;
        if (nameTestcase != null || nameTestcase.length() > 0)
            testCaseName = AbstractTestCase.removeSpecialCharacter(nameTestcase);
        else {
            testCaseName = TestCaseManager.generateContinuousNameOfTestcase(functionNode.getSimpleName());
        }
        testCase = createTestCase(testCaseName, functionNode);
        return testCase;
    }

    public static TestPrototype createPrototype(ICommonFunctionNode functionNode, String name) {
        TestPrototype testCase;
        String testCaseName;
        if (name != null || name.length() > 0)
            testCaseName = AbstractTestCase.removeSpecialCharacter(name);
        else {
            testCaseName = TestCaseManager.generateContinuousNameOfTestcase(functionNode.getSimpleName());
        }
        testCase = createPrototype(testCaseName, functionNode);
        return testCase;
    }

    public static TestCase createTestCase(ICommonFunctionNode functionNode) {
        if (functionNode == null)
            return null;
        String testCaseName = TestCaseManager.
                generateContinuousNameOfTestcase(functionNode.getSimpleName() + ITestCase.POSTFIX_TESTCASE_BY_USER);
        TestCase testCase = createTestCase(testCaseName, functionNode);
        return testCase;
    }

    public static ITestCase getTestCaseByName(String name) {
        ITestCase testCase = getBasicTestCaseByName(name);

        if (testCase == null)
            logger.error(String.format("Test case %s not found.", name));

        return testCase;
    }

    public static TestCase getBasicTestCaseByName(String name) {
        if (name == null)
            return null;

        // find in the map first
        if (nameToBasicTestCaseMap.containsKey(name)) {
            optimizeNameToBasicTestCaseMap(name);
            ITestItem itemInMap = nameToBasicTestCaseMap.get(name);
            if (itemInMap instanceof TestCase) {
                    return (TestCase) itemInMap;
            }
        }

        return null;
    }

    public static TestPrototype getPrototypeByName(String name) {
        if (name == null)
            return null;

        // find in the map first
        if (nameToBasicTestCaseMap.containsKey(name)) {
            optimizeNameToBasicTestCaseMap(name);
            ITestItem itemInMap = nameToBasicTestCaseMap.get(name);
            if (itemInMap instanceof TestPrototype) {
                return (TestPrototype) itemInMap;
            }
        }
        return null;
    }

    public static String getStatusTestCaseByName(String name) {
        ITestCase testCase = getTestCaseByName(name);
        if (testCase == null)
            return ITestCase.STATUS_EMPTY;
        else
            return testCase.getStatus();
    }

    public static void removeBasicTestCase(String name) {
        TestCase testCase = getBasicTestCaseByName(name);
        if (testCase != null) {
            testCase.deleteOldData();
            nameToBasicTestCaseMap.remove(name);

            ICommonFunctionNode functionNode = testCase.getFunctionNode();
            functionToTestCasesMap.get(functionNode).remove(name);

        } else {
            logger.error("Test case not found. Name: " + name);
        }
    }

    /**
     * This method to check if the testcase file (or compound testcase) that has the name
     * exists or not
     *
     * @param name: name of the testcase (or compound testcase)
     * @return true if find out a testcase or a compound testcase in directories
     */
    public static boolean checkTestCaseExisted(String name) {
        optimizeNameToBasicTestCaseMap(name);
        return nameToBasicTestCaseMap.containsKey(name);
    }

    public static void optimizeNameToBasicTestCaseMap(String name) {
    }

    public static List<TestCase> getTestCasesByFunction(ICommonFunctionNode functionNode) {
        List<TestCase> testCases = new ArrayList<>();
        List<String> names = new ArrayList<>(functionToTestCasesMap.get(functionNode));
        for (String name : names) {
            TestCase tc = getBasicTestCaseByName(name);
            if (tc != null)
                testCases.add(tc);
        }
        return testCases;
    }

    public static Map<ICommonFunctionNode, List<String>> getFunctionToTestCasesMap() {
        return functionToTestCasesMap;
    }

    public static Map<String, IDataTestItem> getNameToBasicTestCaseMap() {
        return nameToBasicTestCaseMap;
    }

    public synchronized static String generateContinuousNameOfTestcase(String testCaseNamePrefix){
        logger.debug("Generate name of test case name");
        return testCaseNamePrefix + new Random().nextInt(RANDOM_BOUND);
    }

    private static final int RANDOM_BOUND = 9999999;
}
