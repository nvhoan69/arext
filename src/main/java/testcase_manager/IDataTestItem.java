package testcase_manager;

import parser.object.ICommonFunctionNode;
import testdata.object.DataNode;
import testdata.object.RootDataNode;

import java.util.List;
import java.util.Map;

public interface IDataTestItem extends ITestItem {

    RootDataNode getRootDataNode();

    void setRootDataNode(RootDataNode rootDataNode);

    ICommonFunctionNode getFunctionNode();

    void setFunctionNode(ICommonFunctionNode functionNode);

    Map<DataNode, List<String>> getAdditionalIncludePathsMap();

    void putOrUpdateDataNodeIncludes(DataNode dataNode);

    void putOrUpdateDataNodeIncludes(DataNode dataNode, String includePath);
}
