package testcase_manager;

import java.time.LocalDateTime;
import java.util.List;

public interface ITestItem {
    String getName();

    void setName(String name);

    void setCreationDateTime(LocalDateTime creationDateTime);

    LocalDateTime getCreationDateTime();

    String getCreationDate();

    String getCreationTime();

    boolean isPrototypeTestcase();

    List<String> getAdditionalIncludes();
}
