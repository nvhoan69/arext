package gui;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Swimming {
    public static void main(String[] args) {
//        int[] intValues = new int[] {122, 32, 194, 32};
//        byte[] byteValues = new byte[intValues.length];
//        for (int i = 0; i < intValues.length; i++) {
//            byteValues[i] = Integer.valueOf(intValues[i]).byteValue();
//        }
//
//        String input = new String(byteValues, StandardCharsets.UTF_8);
//        input.codePointCount(0, input.length());
//
//        System.out.println(input);

        // validate char sequence
//        int[] values = new int[] {244, 131, 181, 132, 111, 76};
//        List<Integer> integers = new ArrayList<>();
//
//        for (int i : values) {
//            integers.add(i);
//        }
//
//        System.out.println(integers.subList(0, integers.size()));
//        System.out.println(isValidCharacterSequence(integers));

        // Huyen split string
//        String[] huyenCho = "main[1]  res.length() = 245".split("res\\.length\\(\\) =");
//        System.out.println(huyenCho);
//        String hehe = "Nguyễn Văn Hoàn";
//
//        String hoho = new String(hehe.getBytes(StandardCharsets.UTF_8), StandardCharsets.UTF_16);
//        System.out.println(hoho);
//        System.out.println(String.format("file.encoding: %s", System.getProperty("file.encoding")));
//        System.out.println(String.format("defaultCharset: %s", Charset.defaultCharset().name()));
//        String a = "Họ và tên: ";
//        System.out.println(a + "Nguyễn Ngọc Sơn");

        Pattern pattern = Pattern.compile("[a-zA-Z0-9_]+\\s*\\((.|\\s)*\\)\\s*\\{(.|\\s)*}");
        String sample = "TEST(time_utils, \n daylight_saving) {\n" +
                "\n" +
                "\tstd::string formated = time_t_to_readable(gen_exact_date(2016,12,6,12,0,0,1), \"Europe/Warsaw\");\n" +
                "\tstd::string time_to_match = \"2016-12-06T12:00:00\";\n" +
                "\t//pfp_mark(\"Testing zone: \" << zone << \" formated: \" << formated);\n" +
                "\tEXPECT_NE(formated.find(time_to_match),std::string::npos)\n" +
                "\t\t\t<< \"Generated time [\"<< formated\n" +
                "\t\t\t<< \"] not match expected [\"<< time_to_match <<\"]\";\n" +
                "}";
        Matcher m = pattern.matcher(sample);
        if (m.find()) {
            String string = m.group();
            System.out.println(string);
        }
    }

    /**
     * check if the list of bytes (int values) is valid to convert to character UTF-8.
     *
     * @param integers list of bytes (int values)
     * @return true if valid, vice versus
     */
    private static boolean isValidCharacterSequence(List<Integer> integers) {
        if (integers.isEmpty()) {
            return false;
        }

        if (integers.get(0) >= 0 && integers.get(0) <= 127) { // ascii code <0-127>
            if (integers.size() == 1) {
                return true;
            } else
                return isValidCharacterSequence(integers.subList(1, integers.size())); // size > 1

        } else if (integers.get(0) >= 194 && integers.get(0) <= 223) { // <194-223><128-191>
            if (integers.size() < 2)
                return false;

            if (integers.get(1) >= 128 && integers.get(1) <= 191) {
                if (integers.size() == 2) {
                    return true;
                }
                return isValidCharacterSequence(integers.subList(2, integers.size())); // size > 2
            }

        } else if (integers.get(0) == 224) { // <224><160-191><128-191>
            if (integers.size() < 3)
                return false;

            if (integers.get(1) >= 160 && integers.get(1) <= 191
                    && integers.get(2) >= 128 && integers.get(2) <= 191) {
                if (integers.size() == 3) {
                    return true;
                }
                return isValidCharacterSequence(integers.subList(3, integers.size())); // size > 3
            }

        } else if (integers.get(0) >= 225 && integers.get(0) <= 239) { // <225-239><128-191><128-191>
            if (integers.size() < 3)
                return false;

            if (integers.get(1) >= 128 && integers.get(1) <= 191
                    && integers.get(2) >= 128 && integers.get(2) <= 191) {
                if (integers.size() == 3) {
                    return true;
                }
                return isValidCharacterSequence(integers.subList(3, integers.size())); // size > 3
            }

        } else if (integers.get(0) == 240) { // <240><144-191><128-191><128-191> (48x64x64)
            if (integers.size() < 4)
                return false;

            if (integers.get(1) >= 144 && integers.get(1) <= 191
                    && integers.get(2) >= 128 && integers.get(2) <= 191
                    && integers.get(3) >= 128 && integers.get(3) <= 191) {
                if (integers.size() == 4) {
                    return true;
                }
                return isValidCharacterSequence(integers.subList(4, integers.size())); // size > 4
            }

        } else if (integers.get(0) >= 241 && integers.get(0) <= 243) {
            if (integers.size() < 4)
                return false;

            if (integers.get(1) >= 128 && integers.get(1) <= 191
                    && integers.get(2) >= 128 && integers.get(2) <= 191
                    && integers.get(3) >= 128 && integers.get(3) <= 191) {
                if (integers.size() == 4) {
                    return true;
                }
                return isValidCharacterSequence(integers.subList(4, integers.size())); // size > 4
            }

        } else if (integers.get(0) == 244) { // <244><128-143><128-191><128-191>
            if (integers.size() < 4)
                return false;

            if (integers.get(1) >= 128 && integers.get(1) <= 143
                    && integers.get(2) >= 128 && integers.get(2) <= 191
                    && integers.get(3) >= 128 && integers.get(3) <= 191) {
                if (integers.size() == 4) {
                    return true;
                }
                return isValidCharacterSequence(integers.subList(4, integers.size())); // size > 4
            }
        }

        return false;
    }
}
