package gui.graph;

import gui.Configuration;
import gui.controllers.MVIWindowController;
import gui.controllers.MasterController;
import gui.controllers.RegexTabViewController;
import gui.objects.ExecCommand;
import gui.objects.Interval;
import gui.objects.StringInput;
import javafx.concurrent.Task;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import parser.object.RegexNode;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Prog {
    private RegexNode regexNode;
    private final List<State> states = new ArrayList<>();
    private final List<Transition> transitions = new ArrayList<>();
    private final List<State> finalStates = new ArrayList<>();
    private final List<String> edgePairs = new ArrayList<>();
    private final List<List<Integer>> byteRanges = new ArrayList<>();
    private int forward_size = 0;
    private final Map<String, DynamicProg> inputToDynamicProgMap = new HashMap<>();
    private final Map<String, Character> edgeToCharInputMap = new HashMap<>();
    private final Map<Integer, List<Interval>> byteRangeToIntervalsMap = new HashMap<>();
    private final Map<String, Transition> deadTransitionsMap = new HashMap<>();
    // dead edge pair is ends by the end state
    private final List<String> deadEdgePairs = new ArrayList<>();

    private final List<String> tmpPositiveStrings = new ArrayList<>();
    private final List<String> tmpNegativeStrings = new ArrayList<>();

    private boolean isShortestPathComputed = false;
    private boolean isEdgePairComputed = false;

    private static final String genDynamicFilePath = Configuration.EXECUTABLES_DIR + File.separator + "genDynamic";

    public Prog(RegexNode regexNode) {
        this.regexNode = regexNode;
    }

    public boolean getDFA() {
        if (regexNode == null) {
            System.out.println("The regex node is null");
            return false;
        }

        String getDFAFilePath = Configuration.GET_DFA_EXEC;
        String dataFilePath = regexNode.getDataFilePath();

        // run command and export data into json data file
        String[] commandArr = new String[]{getDFAFilePath, regexNode.getContent(), dataFilePath};
//        String command = String.join(" ", array);

        new ExecCommand(commandArr);

        loadFromJson(dataFilePath, regexNode.getTargetGraphFilePrefix(), false);
        return forward_size > 1;
    }

    public static void main(String[] args) throws UnsupportedEncodingException {
//        demoGenDynamic();

//        for (int i = 0; i < 256; i++) {
//            StringBuilder builder = new StringBuilder();
//            builder.append(i).append(" : ");
//            byte[] tmps = new byte[]{(byte) i};
////            System.out.println(new String(tmps, 0, 1, "Windows-1252"));
//            String s = new String(tmps, 0, 1, "Windows-1252");
//            char c = s.charAt(0);
//            builder.append(c + " : ");
////            System.out.println(c);
////            System.out.println((int) c);
//            byte[] bytes = s.getBytes(StandardCharsets.UTF_8);
//            for (byte b : bytes) {
//                if (Byte.valueOf(b) < 0) {
//                    builder.append(256 + Byte.valueOf(b).intValue()).append(" ");
//                } else {
//                    builder.append(b).append(" ");
//                }
//            }
//            System.out.println(builder);
//        }
        String str1 = "\uD801\uDC326";
        String str2 = "\uFFFF";
        byte[] arr = str1.getBytes("UTF-8");
        byte[] brr = str2.getBytes("UTF-8");
        System.out.println("UTF-8 for \\uD801\\uDC28");
        System.out.println(str1);
        for (byte a : arr) {
            System.out.print(a);
        }
        System.out.println("\nUTF-8 for \\ufffff");
        for (byte b : brr) {
            System.out.print(b);
        }
    }

    public static void demoGenDynamic() {
//        new Prog().loadFromJson("/home/william/IdeaProjects/arext/data.json", "MyGraph.png");
        String regex = "\\w+";
        String input = "hehe2";
//        String command = "/home/william/IdeaProjects/arext/executable/genDynamic "
//                + regex + " " + input;
//                + " > /home/william/IdeaProjects/arext/graph_data/log.txt";
//        System.out.println(command);

        try { // for executing command
            Runtime rt = Runtime.getRuntime();
//            Process proc = rt.exec(new String[] {genDynamicFilePath,
//                    regex, input});
            Process proc = rt.exec(new String[]{"rm", Configuration.DATA_DIR +
                    File.separator + "*"});

            BufferedReader stdInput = new BufferedReader(new
                    InputStreamReader(proc.getInputStream()));

            BufferedReader stdError = new BufferedReader(new
                    InputStreamReader(proc.getErrorStream()));

            final StringBuilder content = new StringBuilder();
            // Read the output from the command
            System.out.println("Here is the standard output of the command:\n");
            String s = null;
            while ((s = stdInput.readLine()) != null) {
                System.out.println(s);
                content.append(s).append("\n");
            }

            // write content to file
            File logdata = new File(Configuration.LOG_DIR + File.separator + "log.txt");
            final BufferedWriter writer = new BufferedWriter(new FileWriter(logdata));
            writer.write(content.toString());
            writer.close();

            // Read any errors from the attempted command
            if (stdError.readLine() != null) {
                System.out.println("Here is the standard error of the command:");
                System.out.println(s);
                while ((s = stdError.readLine()) != null) {
                    System.out.println(s);
                }
            }

            List<String> allMatches = new ArrayList<>();
            Matcher m = Pattern.compile("(.*) (last)?byte: (\\d+) next: (.*) isMatch: (\\d)")
                    .matcher(content);
            while (m.find()) {
                allMatches.add(m.group(1) + " " + m.group(3) + " " + m.group(4) + " " + m.group(5));
            }

            System.out.println(String.join("\n", allMatches));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public DynamicProg convertDynamicStatesName(String dynamicData) {

        DynamicProg dynamicProg = new DynamicProg();
        // map dynamic name to static state name
        Map<String, State> stateMap = dynamicProg.getStateMap();
        // list of transitions is cover by the dynamic DFA
        List<Transition> coveredTransitions = dynamicProg.getCoveredTransitions();
        StringBuilder path = new StringBuilder("0");

        String[] transitions = dynamicData.split("\n");
        for (int i = 0; i < transitions.length; i++) {
            String[] data = transitions[i].split(" ");
            String dynamicStartState = data[0];
            int charInput = Integer.parseInt(data[1]);
            String dynamicEndState = data[2];

            // if i = 0 then the dynamic start state is state 0
            // else, the dynamic start is the previous end state, that converted in the previous loop,
            // and save in the stateMap defined above.
            State startState;
            if (i == 0) {
                stateMap.put(dynamicStartState, states.get(0));
                startState = states.get(0);
            } else {
                startState = stateMap.get(dynamicStartState);
                if (startState == null) {
                    System.out.println("AREXT ERROR: the dynamic start state is not converted before!");
                }
            }

            assert startState != null;
            Transition transition = null;
            int byteRangeNumber = -1; // -1 is the invalid value
            for (Transition trans : startState.getTransitions()) {
                for (int byteRangeNum : trans.getConditions()) {
                    boolean found = false;
                    for (int ch : byteRanges.get(byteRangeNum)) {
                        if (ch == charInput) {
                            coveredTransitions.add(trans);
                            transition = trans;
                            byteRangeNumber = byteRangeNum;
                            found = true;
                            break;
                        }
                    }
                    if (found) break;
                }
            }

            State endState = null;
            if (transition == null) {
                System.out.println("AREXT ERROR: can not detect transition with charInput: " + charInput
                        + " from the state " + startState.getName());
            } else {
                endState = transition.getEndState();
                stateMap.put(dynamicEndState, endState);
                path.append(".").append(endState.getName());
            }

//            System.out.println("Start: " + startState.getName() + " - Byte Range: " + byteRangeNumber +
//                    " - End: " + endState.getName());
        }
        dynamicProg.setPath(path.toString());
        String[] states = path.toString().split("\\.");
        if (states.length >= 3) {
            for (int i = 0; i < states.length - 2; i++) {
                String edgePair = states[i] + "." + states[i + 1] +
                        "." + states[i + 2];
                if (!dynamicProg.getCoveredEdgePairs().contains(edgePair)) {
                    dynamicProg.getCoveredEdgePairs().add(edgePair);
                }
            }

        }
        return dynamicProg;
    }

    public DynamicProg fullMatch(String regex, String input) {
        try { // for executing command
            // STEP 1: execute matching
            Runtime rt = Runtime.getRuntime();
            Pattern reg = Pattern.compile(regex);
//            System.out.println("pattern: " + reg);

            // normalize for pass as command line argument
//            input = input.replace("\"", "\\\"");
//            input = input.replace("\\\\\"", "\\\"");

//            Process proc = rt.exec(new String[]{genDynamicFilePath,
//                    "\"" + regex + "\"", "\"" + input + "\""});
            Process proc = rt.exec(new String[]{genDynamicFilePath,
                    regex, input});

            BufferedReader stdInput = new BufferedReader(new
                    InputStreamReader(proc.getInputStream()));

            BufferedReader stdError = new BufferedReader(new
                    InputStreamReader(proc.getErrorStream()));

            final StringBuilder content = new StringBuilder();
            // Read the output from the command
//            System.out.println("Here is the standard output of the command:\n");
            String s;
            while ((s = stdInput.readLine()) != null) {
//                System.out.println(s);
                content.append(s).append("\n");
            }

            // write content to file
            File logdata = new File(Configuration.LOG_DIR + File.separator + "log.txt");
            final BufferedWriter writer = new BufferedWriter(new FileWriter(logdata));
            writer.write(content.toString());
            writer.close();

            // Read any errors from the attempted command
            if (stdError.readLine() != null) {
//                System.out.println("Here is the standard error of the command:");
                while ((s = stdError.readLine()) != null) {
//                    System.out.println(s);
                }
            }

            List<String> allMatches = new ArrayList<>();
            Matcher m = Pattern.compile("(.*) (last)?byte: (\\d+) next: (.*) isMatch: (\\d)")
                    .matcher(content);
            while (m.find()) {
                allMatches.add(m.group(1) + " " + m.group(3) + " " + m.group(4) + " " + m.group(5));
            }

            boolean isMatch = false;
            Matcher isMatchMatcher = Pattern.compile("isMatch:(\\d)\n").matcher(content);
            if (isMatchMatcher.find()) {
                if (Integer.parseInt(isMatchMatcher.group(1)) == 1) {
                    isMatch = true;
                }
            }

            String dynamicData = String.join("\n", allMatches);
//            System.out.println(dynamicData);

            // STEP 2: convert dynamic states to map static states
            DynamicProg dynamicProg = convertDynamicStatesName(dynamicData);
            dynamicProg.setMatch(isMatch);
            return dynamicProg;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public String computeInput(String input) {
        String regex = regexNode.getContent();
        DynamicProg dynamicProg = fullMatch(regex, input);

        if (!inputToDynamicProgMap.containsKey(input)) {
            inputToDynamicProgMap.put(input, dynamicProg);
        }

        if (dynamicProg == null) {
            return null;
        }
        return dynamicProg.getPath();
    }

    public void computeEdgePairs() {
        for (State state : states) {
            if (state.getTransitions().size() > 0 && state.getBackwardTransitions().size() > 0) {
                for (Transition backTran : state.getBackwardTransitions()) {
                    for (Transition forwardTran : state.getTransitions()) {
                        String edgePair = backTran.getStartState().getName() + "." +
                                state.getName() + "." + forwardTran.getEndState().getName();
                        this.edgePairs.add(edgePair);
                    }
                }
            }
        }
        isEdgePairComputed = true;
    }

    public void generateStringsEPPool() {
        if (!isShortestPathComputed) {
            computeShortestPaths();
        }
        if (!isEdgePairComputed) {
            computeEdgePairs();
        }
        tmpNegativeStrings.clear();
        tmpPositiveStrings.clear();

        // compute edge and char input map, that need for converting path to string input
        for (Transition transition : transitions) {
            String edge = transition.getStartState().getName() + "." +
                    transition.getEndState().getName();
            if (!edgeToCharInputMap.containsKey(edge)) {
                edgeToCharInputMap.put(edge, chooseCharInput(transition.getConditions()));
            }
        }

        List<String> edgePairsPool = new ArrayList<>(edgePairs);
        Map<String, State> nameToStateMap = new HashMap<>();
        for (State state : states) {
            nameToStateMap.put(state.getName(), state);
        }

        // handle path 0.e
        if (edgeToCharInputMap.containsKey("0.e")) {
            String input = convertPathToStringInput("0.e", false);
            if (input != null) {
                computeInput(input);
                DynamicProg dynamicProg = getInputToDynamicProgMap().get(input);
                if (dynamicProg != null) {
                    if (!dynamicProg.isMatch()) {
                        tmpNegativeStrings.add(input);
                    } else {
                        System.out.println("Error, path 0.e but " + dynamicProg.getPath());
                    }
                }
            } else { // dead edge
                Transition deadTran = null;
                for (Transition tran : nameToStateMap.get("0").getTransitions()) {
                    if (isDeadState(tran.getEndState())) {
                        deadTran = tran;
                        break;
                    }
                }
                if (deadTran != null) {
                    deadTransitionsMap.put("0.e", deadTran);
                }
            }
        }
        String finalStateName = getFinalStates().get(0).getName(); // assume that have only one final state
        String pathToFinal = "0." + finalStateName;
        if (edgeToCharInputMap.containsKey(pathToFinal)) {
            String input = convertPathToStringInput(pathToFinal, true);
            if (input != null) {
                computeInput(input);
                DynamicProg dynamicProg = getInputToDynamicProgMap().get(input);
                if (dynamicProg != null) {
                    if (dynamicProg.getPath().equals(pathToFinal)) {
                        tmpPositiveStrings.add(input);
                    } else {
                        System.out.println("WARNING, path 0.final but " + dynamicProg.getPath() + " - 1");
                        // may be there is a transition to final state that doesn't contain 256 byte
                        input = convertPathToStringInput(pathToFinal, false);
                        pathToFinal += ".e";
                        if (input != null) {
                            String realPath = computeInput(input);
                            if (!realPath.equals(pathToFinal)) {
                                System.out.println("WARNING: real path is different from expected path - 2");
                            }
                        }
                    }
                }
            } else {
                System.out.println("Failed to gen input for the special path from 0 to final: " +
                        pathToFinal);
            }
        }

        while (!edgePairsPool.isEmpty()) {
            int randIndex = new Random().nextInt(edgePairsPool.size());
            String edgePair = edgePairsPool.get(randIndex);
            String[] states = edgePair.split("\\.");
            State startState = nameToStateMap.get(states[0]);
            State middleState = nameToStateMap.get(states[1]);
            State endState = nameToStateMap.get(states[2]);

            boolean isLastByte256 = true;
            String path = startState.getPathFrom0() + "." + middleState.getName() + "." +
                    endState.getPathToF();
            if (isDeadState(endState)) {
                path = startState.getPathFrom0() + "." + middleState.getName() + ".e";
                isLastByte256 = false;
            }

            String input = convertPathToStringInput(path, isLastByte256);

            if (input != null) { // valid sequence
                String realPath = computeInput(input);
                if (!realPath.equals(path)) {
                    System.out.println("WARNING: real path is different from expected path - 1");
                    // may be there is a transition to final state that doesn't contain 256 byte
                    if (isLastByte256) {
                        input = convertPathToStringInput(path, false);
                        path += ".e";
                        if (input != null) {
                            realPath = computeInput(input);
                            if (!realPath.equals(path)) {
                                System.out.println("WARNING: real path is different from expected path - 2");
                            }
                        }
                    }
                }
//                System.out.println("real path: " + realPath);
//                System.out.println("expected path: " + path);

                DynamicProg dynamicProg = getInputToDynamicProgMap().get(input);
                if (dynamicProg != null) {
                    edgePairsPool.removeIf(ep -> dynamicProg.getCoveredEdgePairs().contains(ep));

                    if (dynamicProg.isMatch()) {
                        tmpPositiveStrings.add(input);
                    } else {
                        tmpNegativeStrings.add(input);
                    }
                    if (isDeadState(endState)) { // so the last edge can be dead edge
                        String lastEdge = edgePair.substring(edgePair.indexOf(".") + 1);
                        if (deadTransitionsMap.containsKey(lastEdge)) {
                            deadTransitionsMap.replace(lastEdge, null);
                        } else {
                            deadTransitionsMap.put(lastEdge, null);
                        }
                    }
                } else {
                    System.out.println("Dynamic prog is null with the input: " + input);
                }
            } else { // dead edge pair
                if (!deadEdgePairs.contains(edgePair)) {
                    deadEdgePairs.add(edgePair);
                }

                if (isDeadState(endState)) {
                    String lastEdge = edgePair.substring(edgePair.indexOf(".") + 1);
                    if (!deadTransitionsMap.containsKey(lastEdge)) {
                        Transition deadTran = null;
                        for (Transition tran : middleState.getTransitions()) {
                            if (tran.getEndState() == endState) {
                                deadTran = tran;
                                break;
                            }
                        }
                        if (deadTran != null) {
                            deadTransitionsMap.put(lastEdge, deadTran);
                        }
                    }
                }

                // assume that if input is null then the path is end of ".e"
                edgePairsPool.remove(edgePair);// if the path is invalid so there is no string satisfy this edge pair
            }
        }

        removeFakeDeadEdges();
//
//        // free memory used for dynamic pros
//        getInputToDynamicProgMap().clear();
    }

    public void generateStringsEdgesPool() {
        if (!isShortestPathComputed) {
            computeShortestPaths();
        }
        if (!isEdgePairComputed) {
            computeEdgePairs();
        }
        tmpNegativeStrings.clear();
        tmpPositiveStrings.clear();

        // compute edge and char input map, that need for converting path to string input
        for (Transition transition : transitions) {
            String edge = transition.getStartState().getName() + "." +
                    transition.getEndState().getName();
            if (!edgeToCharInputMap.containsKey(edge)) {
                edgeToCharInputMap.put(edge, chooseCharInput(transition.getConditions()));
            }
        }

        List<Transition> transPool = new ArrayList<>(transitions);
        while (!transPool.isEmpty()) {
            int randIndex = new Random().nextInt(transPool.size());
            Transition tran = transPool.get(randIndex);
            String path = tran.getStartState().getPathFrom0() + "." +
                    tran.getEndState().getPathToF();

            State start = tran.getStartState();
            String lastEdge = start.getName() + ".e";
            String input = null;

            if (isDeadState(tran.getEndState())) {
                boolean isValidPath = false;
                if (finalStates.contains(start)) { // handle dead edge from finish state to "e"
                    for (Transition btran : start.getBackwardTransitions()) {
                        path = btran.getStartState().getPathFrom0() + "." +
                                start.getName() + ".e";
                        input = convertPathToStringInput(path, true);
                        if (input != null) {
                            isValidPath = true;
                            if (deadTransitionsMap.containsValue(lastEdge)) {
                                deadTransitionsMap.replace(lastEdge, null);
                            } else {
                                deadTransitionsMap.put(lastEdge, null);
                            }
                            break;
                        }
                    }
                }
                if (!isValidPath) {
                    path = tran.getStartState().getPathFrom0() + ".e";
                }
            }

            if (input == null) {
                input = convertPathToStringInput(path, true);
            }

            if (input != null) { // valid sequence
                String realPath = computeInput(input);
                if (!realPath.equals(path)) {
                    System.out.println("WARNING: real path is different from expected path");
                }
//                System.out.println("real path: " + realPath);
//                System.out.println("expected path: " + path);

                DynamicProg dynamicProg = getInputToDynamicProgMap().get(input);
                if (dynamicProg != null) {
                    transPool.removeIf(transition -> {
                        if (deadTransitionsMap.containsValue(transition)) {
                            return true;
                        }
                        return dynamicProg.getCoveredTransitions().contains(transition);
                    });

                    if (dynamicProg.isMatch()) {
                        tmpPositiveStrings.add(input);
                    } else {
                        tmpNegativeStrings.add(input);
                    }
                } else {
                    System.out.println("Dynamic prog is null with the input: " + input);
                }
            } else {
                transPool.removeIf(deadTransitionsMap::containsValue);
            }
        }

        removeFakeDeadEdges();

        // free memory used for dynamic pros
//        getInputToDynamicProgMap().clear();
    }

    public void generateStringsNodesPool() {
        if (!isShortestPathComputed) {
            computeShortestPaths();
        }
        if (!isEdgePairComputed) {
            computeEdgePairs();
        }
        tmpNegativeStrings.clear();
        tmpPositiveStrings.clear();

        // compute edge and char input map, that need for converting path to string input
        for (Transition transition : transitions) {
            String edge = transition.getStartState().getName() + "." +
                    transition.getEndState().getName();
            if (!edgeToCharInputMap.containsKey(edge)) {
                edgeToCharInputMap.put(edge, chooseCharInput(transition.getConditions()));
            }
        }

        List<State> nodesPool = new ArrayList<>(states);
        while (!nodesPool.isEmpty()) {
            int randIndex = new Random().nextInt(nodesPool.size());
            State node = nodesPool.get(randIndex);
            String path;
            if (isDeadState(node))
                path = node.getPathFrom0();
            else
                path = node.getPathFrom0().substring(0, node.getPathFrom0().lastIndexOf(node.getName()))
                        + node.getPathToF();

//            State start = tran.getStartState();
//            String lastEdge = start.getName() + ".e";
            String input = null;

            if (isDeadState(node)) {
                for (Transition btran : node.getBackwardTransitions()) {
                    String newPath = btran.getStartState().getPathFrom0() + ".e";
                    input = convertPathToStringInput(newPath, false);
                    if (input != null) {
                        path = newPath;
                        break;
                    }
                }
            }

            if (input == null) {
                input = convertPathToStringInput(path, true);
            }

            if (input != null) { // valid sequence
                String realPath = computeInput(input);
                if (!realPath.equals(path)) {
                    System.out.println("WARNING: real path is different from expected path");
                    input = convertPathToStringInput(path, false);
                    computeInput(input);
                }
//                System.out.println("real path: " + realPath);
//                System.out.println("expected path: " + path);

                DynamicProg dynamicProg = getInputToDynamicProgMap().get(input);
                if (dynamicProg != null) {
                    nodesPool.removeIf(state -> dynamicProg.getStateMap().containsValue(state));

                    if (dynamicProg.isMatch()) {
                        tmpPositiveStrings.add(input);
                    } else {
                        tmpNegativeStrings.add(input);
                    }
                } else {
                    System.out.println("Dynamic prog is null with the input: " + input);
                }
            } else {
                System.out.println("Can not find the path visit the state " + node.getName() + ", path: " + path);
                nodesPool.remove(node); // tmp fix
            }
        }
    }

    /**
     * handle to get and save some useful information
     */
    private void handleProg() {
        convertByteRangeToIntervals(); // need to execute after loadFromJson
        for (Transition transition : transitions) {
            transition.convertConditionsToIntervals(byteRangeToIntervalsMap);
        }
    }

    /**
     * load data from json file,
     * according this data to create states and transitions
     */
    public void loadFromJson(String filePath, String targetPath, boolean genGraph) {
        File file = new File(filePath);
        if (!file.exists())
            return;
        //JSON parser object to parse read file
        JSONParser jsonParser = new JSONParser();

        try {
            FileReader reader = new FileReader(file);
            //Read JSON file
            Object obj = jsonParser.parse(reader);
            JSONObject jsonObject = (JSONObject) obj;
            Object fw_size = jsonObject.get("forward_size");
            Long longValue = (Long) fw_size;
            this.forward_size = longValue.intValue();
            Object statesObject = jsonObject.get("states");
            if (statesObject instanceof JSONArray) {
                JSONArray states = (JSONArray) statesObject;
//                System.out.println(states);
                // generate DFA with States, Transitions and Byte ranges
                createStates(states);

//                System.out.println(dotFileContent);
            }

            Object byteRangesJObj = jsonObject.get("byte_ranges");
            if (byteRangesJObj instanceof JSONArray) {
                JSONArray byteRangesJArray = (JSONArray) byteRangesJObj;
                for (int i = 0; i < byteRangesJArray.size(); i++) {
                    List<Long> longValues = new ArrayList<>((JSONArray) byteRangesJArray.get(i));
                    List<Integer> byte_range = new ArrayList<>();
                    for (Long val : longValues) {
                        byte_range.add(val.intValue());
                    }
                    byteRanges.add(byte_range);
                }

                System.out.println("load byte ranges done!");
            }
            handleProg();

            if (genGraph) {
                // generate dot file to generate graph of DFA
                long time1 = System.nanoTime();
                String dotFileContent = generateDotFile(targetPath, new ArrayList<>(), new ArrayList<>());
                long time2 = System.nanoTime();
                double time = ((double) time2 - time1) / 1000000000;
                System.out.println("Time: " + time);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String convertPathToStringInput(String path, boolean isLastByte256) {
        String[] stateNames = path.split("\\.");
        String lastEdge = stateNames[stateNames.length - 2] + "." + stateNames[stateNames.length - 1];
//        if (deadTransitionsMap.containsKey(lastEdge)) {
//            return null;
//        }
        boolean isMatchingPath = !path.endsWith("e"); // if is matching path then the last byte can change to 256
//        System.out.println("Path: " + path);

        List<Integer> intValuesOfBytes = new ArrayList<>(); // element is in 0-256;

        while (path.contains(".")) {
            // if path is "0.11.3.4" => newPath: "11.3.4" => nextState: "11";
            String newPath = path.substring(path.indexOf(".") + 1);
            String nextState;
            if (!newPath.contains(".")) {
                nextState = newPath;
            } else {
                nextState = newPath.substring(0, newPath.indexOf(".")); // case that next state name is more than one digit is handled
            }
            String edge = path.substring(0, path.indexOf(".") + 1) + nextState; // "0.11"

            Character nextCharInput = getEdgeToCharInputMap().get(edge);
            if (nextCharInput == null) {
                System.out.println("ERROR in edgeToCharInputMap");
                return null;
            }
            intValuesOfBytes.add(Integer.valueOf(nextCharInput));
            path = newPath;
        }

        if (isLastByte256) {
            // change the last bytes of positive path to 97, so it is valid character sequence. The last byte will change to 256 when need
            if (isMatchingPath && intValuesOfBytes.size() > 0) { // a positive path, so the last byte is ignore and assumed as 256
                intValuesOfBytes.set(intValuesOfBytes.size() - 1, 97); // any byte, 97 or another, this byte is ignore and assume as 256
            }
        }

        // check if the bytes is valid
        if (MasterController.isValidCharacterSequence(intValuesOfBytes)) {
            byte[] utf_8_form = new byte[intValuesOfBytes.size()];
            for (int i = 0; i < intValuesOfBytes.size(); i++) {
                utf_8_form[i] = intValuesOfBytes.get(i).byteValue();
            }

            String input = new String(utf_8_form, StandardCharsets.UTF_8);

            if (isMatchingPath) {
                if (input.length() > 0 && isLastByte256) {
                    input = input.substring(0, input.length() - 1);
                }
            } else {
                if (deadTransitionsMap.containsKey(lastEdge)) {
                    deadTransitionsMap.replace(lastEdge, null);
                } else {
                    deadTransitionsMap.put(lastEdge, null);
                }
            }
//            System.out.println("Input: " + input);
//
////            intValuesOfBytes.set(intValuesOfBytes.size() - 1, 256);
//            for (int i : intValuesOfBytes) {
//                System.out.print(i + " ");
//            }
//            System.out.println();
            return input;
        } else { // the lastEdge is a dead edge
            // put deadEdge to map
            String[] nodes = lastEdge.split("\\.");
            int stateIndex = Integer.parseInt(nodes[0]);
//            System.out.println("Invalid sequence: " + intValuesOfBytes);
            if (!deadTransitionsMap.containsKey(lastEdge)) {
                for (Transition trans : states.get(stateIndex).getTransitions()) {
                    if (trans.getEndState().getName().equals("e")) {
                        deadTransitionsMap.put(lastEdge, trans);
                        break;
                    }
                }
            }

            return null;
        }
    }

    private boolean isDeadState(State state) {
        return state.getName().equals("e");
    }

    public void removeFakeDeadEdges() {
        List<String> edges = new ArrayList<>();
        for (String edge : deadTransitionsMap.keySet()) {
            if (deadTransitionsMap.get(edge) == null) {
                edges.add(edge);
            }
        }
        for (String edge : edges) {
            deadTransitionsMap.remove(edge);
        }
    }

    public void generateStrings() {
        computeShortestPaths();

        // compute edge and char input map, that need for converting path to string input
        for (Transition transition : transitions) {
            String edge = transition.getStartState().getName() + "." +
                    transition.getEndState().getName();
            if (!edgeToCharInputMap.containsKey(edge)) {
                edgeToCharInputMap.put(edge, chooseCharInput(transition.getConditions()));
            }
        }

        List<Transition> transPool = new ArrayList<>(transitions);
        RegexTabViewController controller = MVIWindowController.getInstance()
                .getRegexToViewControllerMap().get(regexNode);

        while (!transPool.isEmpty()) {
            int randIndex = new Random().nextInt(transPool.size());
            Transition tran = transPool.get(randIndex);
            String path = tran.getStartState().getPathFrom0() + "." +
                    tran.getEndState().getPathToF();

            State start = tran.getStartState();
            String lastEdge = start.getName() + ".e";
            String input = null;

            if (isDeadState(tran.getEndState())) {
                boolean isValidPath = false;
                if (finalStates.contains(start)) { // handle dead edge from finish state to "e"
                    for (Transition btran : start.getBackwardTransitions()) {
                        path = btran.getStartState().getPathFrom0() + "." +
                                start.getName() + ".e";
                        input = convertPathToStringInput(path, true);
                        if (input != null) {
                            isValidPath = true;
                            if (deadTransitionsMap.containsValue(lastEdge)) {
                                deadTransitionsMap.replace(lastEdge, null);
                            } else {
                                deadTransitionsMap.put(lastEdge, null);
                            }
                            break;
                        }
                    }
                }
                if (!isValidPath) {
                    path = tran.getStartState().getPathFrom0() + ".e";
                }
            }

            if (input == null) {
                input = convertPathToStringInput(path, true);
            }

            if (input != null) { // valid sequence
                String realPath = controller.computeInput(input, true);
                if (!realPath.equals(path)) {
                    System.out.println("WARNING: real path is different from expected path");
                }
                System.out.println("real path: " + realPath);
                System.out.println("expected path: " + path);

                DynamicProg dynamicProg = getInputToDynamicProgMap().get(input);
                if (dynamicProg != null) {
                    transPool.removeIf(transition -> {
                        if (deadTransitionsMap.containsValue(transition)) {
                            return true;
                        }
                        return dynamicProg.getCoveredTransitions().contains(transition);
                    });
                } else {
                    System.out.println("Dynamic prog is null with the input: " + input);
                }
            } else {
                transPool.removeIf(deadTransitionsMap::containsValue);
            }
        }

        removeFakeDeadEdges();
        controller.syntheticAllDynamic();

        // free memory used for dynamic pros
        getInputToDynamicProgMap().clear();

//        Stack<String> stack = new Stack<>();
//        stack.push("0");
//        generateStrings(usingAlgorithms, stack, null);
//
//
//        while (true) {
//            List<State> coveredStates = new ArrayList<>();
//            List<Transition> coveredEdges = new ArrayList<>();
//            for (DynamicProg dynamicProg : prog.getInputToDynamicProgMap().values()) {
//                for (State state : dynamicProg.getStateMap().values()) {
//                    if (!coveredStates.contains(state)) {
//                        coveredStates.add(state);
//                    }
//                }
//                for (Transition tran : dynamicProg.getCoveredTransitions()) {
//                    if (!coveredEdges.contains(tran)) {
//                        coveredEdges.add(tran);
//                    }
//                }
//            }
//
//            // find the state that hasn't yet satisfied
//            State notSatisfiedState = findNotSatisfiedState(coveredStates, coveredEdges);
//
//            // find the path that the state is visited earliest
//            String root = "";
//            int earliestOccur = -1;
//            if (notSatisfiedState != null) {
//                for (DynamicProg dynamicProg : prog.getInputToDynamicProgMap().values()) {
//                    String path = dynamicProg.getPath();
//                    String[] names = path.split("\\.");
//                    List<String> stateNames = new ArrayList<>();
//                    for (String name : names) {
//                        stateNames.add(name);
//                    }
//                    int firstOccur = stateNames.indexOf(notSatisfiedState.getName());
//                    stateNames = stateNames.subList(0, firstOccur + 1);
//
//                    if (firstOccur != -1) {
//                        if (earliestOccur == -1 || firstOccur < earliestOccur) {
//                            earliestOccur = firstOccur;
//                            root = String.join(".", stateNames);
//                        }
//                    }
//                }
//            } else {
//                break;
//            }
//
//            if (earliestOccur != -1) { // continue to generate string, with basic algorithms (allow revisiting some edges)
//                stack.clear();
//                stack.push(root);
//                List<String> canBeRevisitOnceEdges = new ArrayList<>();
//                String[] stateNames = root.split("\\.");
//                for (int i = 0; i < stateNames.length - 1; i++) {
//                    String nextEdge = stateNames[i] + "." + stateNames[i + 1];
//                    if (!canBeRevisitOnceEdges.contains(nextEdge) && !stateNames[i].equals(stateNames[i + 1])) {
//                        canBeRevisitOnceEdges.add(nextEdge);
//                    }
//                }
//                generateStrings(Algorithm.BASIC, stack, canBeRevisitOnceEdges);
//            }
//        }
    }

    public void generateStringsUsingMutRex() {
        tmpPositiveStrings.clear();
        tmpNegativeStrings.clear();

        String[] cmd = {"java", "-jar", Configuration.MUTREX_RUN, regexNode.getContent()};
        System.out.println(Arrays.toString(cmd));
        Runtime runtime = Runtime.getRuntime();
        try {
            Process process = runtime.exec(cmd);
//            process.waitFor(10, TimeUnit.SECONDS);
            process.waitFor();
            InputStream inputStream = process.getInputStream();
            BufferedReader std = new BufferedReader(new InputStreamReader(inputStream));

            List<String> inputs = new ArrayList<>();
            String logLine;
            Pattern pattern = Pattern.compile("\"(.*?)\" (\\(CONF\\)|\\(REJECT\\)) kills ");
            while ((logLine = std.readLine()) != null) {
                Matcher m = pattern.matcher(logLine);
                if (m.find()) {
                    String input = m.group(1);
                    inputs.add(input);
                }
            }

            for (String string : inputs) {
                computeInput(string);
                DynamicProg dynamicProg = inputToDynamicProgMap.get(string);
                if (dynamicProg.isMatch()) {
                    tmpPositiveStrings.add(string);
                } else {
                    tmpNegativeStrings.add(string);
                }
            }
        } catch (IOException | InterruptedException ioe) {
            ioe.printStackTrace();
        }

    }

    public void generateStringsUsingEgret() {
        tmpPositiveStrings.clear();
        tmpNegativeStrings.clear();

//        RegexTabViewController controller = MVIWindowController.getInstance()
//                .getRegexToViewControllerMap().get(regexNode);
//        controller.clearInputStrings();

//        String[] cmd = {"python3", Configuration.EGRET_RUN, "-r", "\"" + regexNode.getContent() + "\""};
        String[] cmd = {"python3", Configuration.EGRET_RUN, "-r", regexNode.getContent()};
        System.out.println(Arrays.toString(cmd));
        Runtime runtime = Runtime.getRuntime();
        try {
            Process process = runtime.exec(cmd);
            process.waitFor(10, TimeUnit.SECONDS);
            if (!process.isAlive()) {
                InputStream inputStream = process.getInputStream();
                BufferedReader std = new BufferedReader(new InputStreamReader(inputStream));

                List<String> inputs = new ArrayList<>();
                String input;
//            System.out.println("Strings generated by Egret: ");
                while ((input = std.readLine()) != null) {
//                System.out.println(input);
                    if (!input.startsWith("Regex: ")
                            && !input.isEmpty()
                            && !input.startsWith("WARNING")
                            && !input.startsWith("...Regex:")
                            && !input.equals("Matches:")
                            && !input.equals("Non-matches:")) {
                        if (input.equals("<empty>")) {
                            input = "";
                        }
                        inputs.add(input);
                    }
                }

                for (String string : inputs) {
                    computeInput(string);
                    DynamicProg dynamicProg = inputToDynamicProgMap.get(string);
                    if (dynamicProg != null) {
                        if (dynamicProg.isMatch()) {
                            tmpPositiveStrings.add(string);
                        } else {
                            tmpNegativeStrings.add(string);
                        }
                    }
                }

//            controller.syntheticAllDynamic();
            } else {
                System.out.println("WARNING, time out! " + regexNode.getContent());
            }

        } catch (IOException | InterruptedException ioe) {
            ioe.printStackTrace();
        }

    }

    /**
     * choose a char satisfy the conditions of the transition (in some byte ranges).
     *
     * @param conditions byte ranges
     * @return a char
     */
    public char chooseCharInput(List<Integer> conditions) {
        List<Integer> mergedSet = new ArrayList<>();
        for (Integer rangeNum : conditions) {
            mergedSet.addAll(byteRanges.get(rangeNum));
        }
        List<Integer> priorCharSet = new ArrayList<>(mergedSet);
        priorCharSet.removeIf(aLong -> { // priority for range 0-127
//            if (aLong >= 0 && aLong < 32) {
//                return false;
//            }
            if (32 <= aLong && aLong <= 47) {// some normal chars
                return false;
            }
            if (aLong >= 48 && aLong <= 57) { // digital [0-9]
                return false;
            }
            if (aLong >= 58 && aLong <= 64) { // some normal chars
                return false;
            }
            if (aLong >= 65 && aLong <= 90) { // Capital chars [A-Z]
                return false;
            }
            if (aLong >= 91 && aLong <= 96) {// some normal chars
                return false;
            }
            if (aLong >= 97 && aLong <= 122) { // lower case char [a-z]
                return false;
            }
            if (aLong >= 123 && aLong <= 127) {// some normal char
                return false;
            }

            return true;
        });
//        System.out.println("filtered prior chars: " + mergedSet);

        if (priorCharSet.isEmpty()) {
            priorCharSet.addAll(mergedSet); // the filter number 2
            priorCharSet.removeIf(integer -> integer >= 32);
        }

        long characterCode;
        if (priorCharSet.size() > 0) {
            int randIndex = new Random().nextInt(priorCharSet.size());
            characterCode = priorCharSet.get(randIndex);
        } else {
            int randIndex = new Random().nextInt(mergedSet.size());
            characterCode = mergedSet.get(randIndex);

        }
        char character = Character.toString((char) characterCode).charAt(0);
//        System.out.println("Chose character: " + (int) character);
        return character;
    }

    /**
     * represent byte ranges in form of intervals
     * e.g. byte range 0: 1,2,3,5,7, 8,9 => 1-3, 5, 7-9
     */
    private void convertByteRangeToIntervals() {
        for (int i = 0; i < byteRanges.size(); i++) {
            List<Integer> elements = byteRanges.get(i);
            List<Interval> intervals = Interval.convertListToIntervals(elements);
            byteRangeToIntervalsMap.put(i, intervals);
        }
    }

    private void createStates(JSONArray data) {
        HashMap<Integer, Transition> tempMap = new HashMap<>();
        // initial states, optimize: create number of states that equals to forward_size saved in json file
        for (int i = 0; i < data.size(); i++) {
            Object obj = data.get(i);
            if (obj instanceof JSONArray) {
                State state = new State();
                state.setName(Integer.toString(i));
                states.add(i, state);
            } else {
                System.out.println("Invalid data of states");
            }
        }

        State errorState = new State();
        errorState.setName("e");
        states.add(errorState);

        // create transitions
        for (int i = 0; i < data.size(); i++) { // for each state
            tempMap.clear(); // the map contains name of end state (integer) and the end state
            List<Integer> existingStates = new ArrayList<>();
            Object obj = data.get(i);
            if (obj instanceof JSONArray) {
                JSONArray stateData = (JSONArray) obj;

                for (int j = 0; j < stateData.size(); j++) {
                    int end = ((Long) stateData.get(j)).intValue();
                    if (existingStates.contains(end)) {
                        tempMap.get(end).getConditions().add(j);
                    } else {
                        Transition transition = new Transition();
                        transition.setStartState(states.get(i));

                        State endState;
                        if (end == -1) {
                            endState = errorState;
                        } else {
                            endState = states.get(end);
                        }
                        transition.setEndState(endState);
                        endState.getBackwardTransitions().add(transition);
                        transition.getConditions().add(j);

                        existingStates.add(end);
                        tempMap.put(end, transition);
                    }
                }

                State startState = states.get(i);
                startState.getTransitions().addAll(tempMap.values());
            } else {
                System.out.println("Invalid data of states");
            }

            // todo: need to explain later
            if (tempMap.keySet().size() == 1 && tempMap.containsKey(-1)) {
                finalStates.add(states.get(i));
            }
            transitions.addAll(states.get(i).getTransitions());
        }
    }

    public void computeShortestPaths() {
        State state0 = null;
        for (State state : states) {
            if (state.getName().equals("0"))
                state0 = state;
        }
        if (state0 != null) {
            state0.setCountFrom0(0);
            state0.setPathFrom0("0");
            List<State> visited = new ArrayList<>();
            visited.add(state0);
            computeShortestPathsFrom0(state0, visited);

//            System.out.println("Shortest Paths From 0: ");
//            for (State state : states) {
//                System.out.println(state.getName() + ": " + state.getCountFrom0());
//                System.out.println(state.getPathFrom0());
//            }
        }

        for (State finalState : finalStates) {
            finalState.setCountToF(0);
            finalState.setPathToF(finalState.getName());
            List<State> visited = new ArrayList<>();
            visited.add(finalState);
            computeShortestPathsToF(finalState, visited);
        }
        isShortestPathComputed = true;
/*
        System.out.println("Shortest Paths to F: ");
        for (State state : states) {
            System.out.println(state.getName() + ": " + state.getCountToF());
            System.out.println(state.getPathToF());
        }
*/
    }

    private void computeShortestPathsToF(State state, List<State> visited) { // may be there are more than 1 final state
        for (Transition transition : state.getBackwardTransitions()) {
            State start = transition.getStartState();
            if (visited.contains(start)) {
                continue;
            }
            int shortestState = state.getCountToF() + 1;
            if (start.getCountToF() == -1
                    || start.getCountFrom0() > shortestState) {
                start.setCountToF(shortestState);
                start.setPathToF(start.getName() + "." + state.getPathToF());
            }
        }

        for (Transition transition : state.getBackwardTransitions()) {
            State start = transition.getStartState();
            if (!visited.contains(start)) {
                visited.add(start);
                computeShortestPathsToF(start, visited);
            }
        }
    }

    /**
     * compute the shortest count and path from state 0 to the passed in state
     *
     * @param state   the passed in state
     * @param visited states that was computed is add to this list and no need to compute again
     */
    private void computeShortestPathsFrom0(State state, List<State> visited) {
        for (Transition transition : state.getTransitions()) {
            State endState = transition.getEndState();
            if (visited.contains(endState)) {
                continue;
            }
            int shortestState = state.getCountFrom0() + 1;
            if (endState.getCountFrom0() == -1
                    || endState.getCountFrom0() > shortestState) {
                endState.setCountFrom0(shortestState);
                endState.setPathFrom0(state.getPathFrom0() + "." + endState.getName());
            }
        }

        for (Transition transition : state.getTransitions()) {
            State endState = transition.getEndState();
            if (!visited.contains(endState)) {
                visited.add(endState);
                computeShortestPathsFrom0(endState, visited);
            }
        }
    }

    /**
     * from states and transitions, generate dot file to GUI represent the graph
     *
     * @return dot file content
     */
    public String generateDotFile(String targetPath, List<State> coveredStates
            , List<Transition> coveredTransitions) {

        StringBuilder builder = new StringBuilder();
        builder.append("digraph finite_state_machine {\n")
                .append("\tgraph [overlap=false];")
                .append("\tsize=\"100\";\n")
                .append("\trankdir=LR;\n");
        builder.append("\tnode [shape = point color = green4] ENTRY;\n");

        for (State state : states) {
            if (finalStates.contains(state)) {
                if (coveredStates.contains(state)) {
                    builder.append("\tnode [shape = doublecircle style = filled color = darkolivegreen4]; ")
                            .append(state.getName()).append(";\n");
                } else {
                    builder.append("\tnode [shape = doublecircle style = \"\"]; ")
                            .append(state.getName()).append(";\n");
                }
            } else {
                if (coveredStates.contains(state)) {
                    builder.append("\tnode [shape = circle style = filled color = darkolivegreen3]; ")
                            .append(state.getName()).append(";\n");
                } else {
                    builder.append("\tnode [shape = circle color = black style = \"\"]; ")
                            .append(state.getName()).append(";\n");
                }
            }
        }

        builder.append("\tENTRY -> 0 [color = green4];\n");
        for (Transition transition : transitions) {
            State start = transition.getStartState();
            State end = transition.getEndState();

            String label = transition.printIntervals();
            builder.append("\t").append(start.getName()).append(" -> ")
                    .append(end.getName())
                    .append(" [ label = \"").append(label).append("\"");
            if (coveredTransitions.contains(transition)) {
                builder.append(" penwidth = 2 color = green4");
            } else if (deadTransitionsMap.containsValue(transition)) {
                builder.append(" penwidth = 2 color = red4");
            }
            builder.append(" ];\n");
        }
        builder.append("}");

        boolean isLargeGraph = transitions.size() > 200;
        GraphViz.createDotGraph(builder.toString(), targetPath, isLargeGraph);
        return builder.toString();
    }

    public List<Transition> getTransitions() {
        return transitions;
    }

    public List<State> getStates() {
        return states;
    }

    public List<State> getFinalStates() {
        return finalStates;
    }

    public List<List<Integer>> getByteRanges() {
        return byteRanges;
    }

    public Map<String, Character> getEdgeToCharInputMap() {
        return edgeToCharInputMap;
    }

    public Map<String, DynamicProg> getInputToDynamicProgMap() {
        return inputToDynamicProgMap;
    }

    public Map<String, Transition> getDeadTransitionsMap() {
        return deadTransitionsMap;
    }

    public List<String> getEdgePairs() {
        return edgePairs;
    }

    public List<String> getDeadEdgePairs() {
        return deadEdgePairs;
    }

    public List<String> getTmpPositiveStrings() {
        return tmpPositiveStrings;
    }

    public List<String> getTmpNegativeStrings() {
        return tmpNegativeStrings;
    }
}
