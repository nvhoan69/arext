package gui.graph;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DynamicProg {

    // map dynamic name to static state name
    private Map<String, State> stateMap = new HashMap<>();
    // list of transitions is cover by the dynamic DFA
    private List<Transition> coveredTransitions = new ArrayList<>();
    private List<String> coveredEdgePairs = new ArrayList<>();
    private boolean isMatch;
    private String path = null;

    public List<Transition> getCoveredTransitions() {
        return coveredTransitions;
    }

    public Map<String, State> getStateMap() {
        return stateMap;
    }

    public void setMatch(boolean match) {
        isMatch = match;
    }

    public boolean isMatch() {
        return isMatch;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public List<String> getCoveredEdgePairs() {
        return coveredEdgePairs;
    }
}
