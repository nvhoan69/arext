package gui.graph;

import java.util.ArrayList;
import java.util.List;

public class State {
    private String name;

    // list of transitions that start from this state
    private final List<Transition> transitions = new ArrayList<>();
    private final List<Transition> backwardTransitions = new ArrayList<>();

    private int countFrom0 = -1;
    private int countToF = -1;
    private String pathFrom0 = null;
    private String pathToF = null;

    public List<Transition> getTransitions() {
        return transitions;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPathFrom0(String pathFrom0) {
        this.pathFrom0 = pathFrom0;
    }

    public int getCountFrom0() {
        return countFrom0;
    }

    public void setCountFrom0(int countFrom0) {
        this.countFrom0 = countFrom0;
    }

    public String getPathFrom0() {
        return pathFrom0;
    }

    public String getPathToF() {
        return pathToF;
    }

    public void setCountToF(int countToF) {
        this.countToF = countToF;
    }

    public void setPathToF(String pathToF) {
        this.pathToF = pathToF;
    }

    public int getCountToF() {
        return countToF;
    }

    public List<Transition> getBackwardTransitions() {
        return backwardTransitions;
    }
}
