package gui.graph;

import gui.objects.Interval;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Transition {
    // label of the transition
    private final List<Integer> conditions = new ArrayList<>(); // byte ranges
    private List<Interval> intervals = new ArrayList<>(); // intervals (e.g. 0-6, 45-54, 128-256)

    public void convertConditionsToIntervals(Map<Integer, List<Interval>> byteRangeToIntervalsMap) {
        intervals.clear();
        for (int byteRange : conditions) {
            intervals.addAll(byteRangeToIntervalsMap.get(byteRange));
        }

        intervals = Interval.optimizeIntervals(intervals);
    }

    public String printIntervals() {
        return Interval.printIntervals(intervals);
    }
    public String printEdge() {
        return startState.getName() + "." + endState.getName();
    }

    private State startState = null;
    private State endState = null;

    public List<Integer> getConditions() {
        return conditions;
    }

    public State getStartState() {
        return startState;
    }

    public State getEndState() {
        return endState;
    }

    public void setStartState(State startState) {
        this.startState = startState;
    }

    public void setEndState(State endState) {
        this.endState = endState;
    }
}
