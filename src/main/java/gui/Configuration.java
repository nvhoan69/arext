package gui;

import java.io.File;

public class Configuration {
    private final static String PROJECT_DIR = "/home/william/IdeaProjects/arext_2";
    public final static String EXECUTABLES_DIR = PROJECT_DIR + File.separator + "executable";
    public final static String LOG_DIR = PROJECT_DIR + File.separator + "logs";
    public final static String DATA_DIR = PROJECT_DIR + File.separator + "data";
    public final static String DYNAMIC_DATA_DIR = DATA_DIR + File.separator +  "dynamic";
    public final static String GRAPH_DATA_DIR = PROJECT_DIR + File.separator + "graph_data";
    public final static String GRAPH_VIZ_PROPERTIES = PROJECT_DIR + File.separator + "config.properties";
    public final static String EGRET_PROJECT = "/home/william/PycharmProjects/egret";
    public final static String EGRET_RUN = EGRET_PROJECT + File.separator + "egret.py";
    public final static String MUTREX_RUN = EXECUTABLES_DIR + File.separator + "mutrex.jar";
    public final static String GET_DFA_EXEC = EXECUTABLES_DIR + File.separator + "getDFA00";

    public enum Gen_Algorithm {
        NODES_POOL, EDGES_POOL, EDGE_PAIRS_POOL, EGRET, MUTREX
    }

    public static void initializeDirectories() {
        File logsDir = new File(LOG_DIR);
        if (!logsDir.exists()) {
            logsDir.mkdirs(); // create parent files if need
        }

        File dataDir = new File(DATA_DIR);
        if (!dataDir.exists()) {
            dataDir.mkdirs(); // create parent files if need
        }

        File dynamicDir = new File(DYNAMIC_DATA_DIR);
        if (!dynamicDir.exists()) {
            dynamicDir.mkdirs(); // create parent files if need
        }

        File graphDir = new File(GRAPH_DATA_DIR);
        if (!graphDir.exists()) {
            graphDir.mkdirs(); // create parent files if need
        }
    }
}
