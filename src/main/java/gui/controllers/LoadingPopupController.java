package gui.controllers;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class LoadingPopupController {
    private AnchorPane loadingPopupPane = null;
    private Stage loadingStage = null;

    public static LoadingPopupController getInstance() {
        FXMLLoader loader = new FXMLLoader(LoadingPopupController
                .class.getResource("/FXML/LoadingPopup.fxml"));
        try {
            AnchorPane pane = loader.load();
            LoadingPopupController controller = loader.getController();
            controller.setLoadingPopupPane(pane);

            Stage loadingStage = new Stage();
            loadingStage.setScene(new Scene(pane));
            controller.setLoadingStage(loadingStage);

            return controller;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void showDialog(Stage ownerStage) {
        loadingStage.initOwner(ownerStage);
        loadingStage.initModality(Modality.WINDOW_MODAL);
        loadingStage.initStyle(StageStyle.UNDECORATED);

        loadingStage.show();
    }

    public void setLoadingStage(Stage loadingStage) {
        this.loadingStage = loadingStage;
    }

    public Stage getLoadingStage() {
        return loadingStage;
    }

    public void setLoadingPopupPane(AnchorPane loadingPopupPane) {
        this.loadingPopupPane = loadingPopupPane;
    }
}
