package gui.controllers;

import gui.Configuration;
import gui.graph.DynamicProg;
import gui.graph.Prog;
import gui.objects.StructureTreeCellFactory;
import gui.objects.StructureTreeItem;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TreeView;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import org.apache.commons.io.FileUtils;
import parser.object.*;
import util.Utils;

import java.io.File;
import java.net.URL;
import java.util.*;

public class BaseSceneController implements Initializable {
    @FXML
    private TreeView<INode> tvStructure;
    @FXML
    private SplitPane spMainSplitPane;

    private static BaseSceneController instance = null;
    private static Parent baseScenePane = null;

    private final static HashMap<INode, StructureTreeItem> nodeToTreeItemMap = new HashMap<>();
    private StructureTreeItem fakeItem; // this item is used to contain additional regexes
    private Map<RegexNode, RegexNode> incorrectCorrectRegexMap = new HashMap<>();

    public void initialize(URL var1, ResourceBundle var2) {
        Configuration.initializeDirectories();
        FolderNode fakeNode = new FolderNode();
        fakeNode.setName("additional regexes");
        fakeItem = new StructureTreeItem(fakeNode);

        MVIWindowController mviWindowController = MVIWindowController.getInstance();
        spMainSplitPane.getItems().set(1, mviWindowController.getMviWindowPane());

        File directory = new File("dataset");
        addDirectory(directory);
        // delete old data
        try {
            File graph_data_dir = new File(Configuration.GRAPH_DATA_DIR);
            File data_dir = new File(Configuration.DATA_DIR);

            FileUtils.cleanDirectory(graph_data_dir);
            FileUtils.cleanDirectory(data_dir);
        } catch (Exception e) {
            e.printStackTrace();
        }

        tvStructure.setCellFactory(param -> new StructureTreeCellFactory());

        // double click to view source code or regex content
        tvStructure.setOnMouseClicked(mouseEvent -> {
            if (mouseEvent.getClickCount() == 2) {
                StructureTreeItem item = (StructureTreeItem) tvStructure
                        .getSelectionModel().getSelectedItem();
                if (item != null) {
                    if (item.getValue() instanceof CppFileNode) {
                        MVIWindowController.getInstance().viewSourceCode((CppFileNode) item.getValue());
                    } else if (item.getValue() instanceof RegexNode) {
                        MVIWindowController.getInstance().viewRegex((RegexNode) item.getValue());
                    }
                }
            }
        });
    }

    public static void prepare() {
        FXMLLoader loader = new FXMLLoader(BaseSceneController.class.getResource("/FXML/BaseScene.fxml"));
        try {
            baseScenePane = loader.load();
            instance = loader.getController();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void addRegex() {

    }

    @FXML
    public void addRegexesFromFile() {
        final FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().add(
                new FileChooser.ExtensionFilter("Text Files", "*.txt")
        );
        File file = fileChooser.showOpenDialog(MasterController.getPrimaryStage());
        if (file != null) {
            addRegexesFromFile(file);
        }
    }

    private void addRegexesFromFile(File file) {
        String content = Utils.readFileContent(file);
        String[] regexes = content.split("\n");

        if (!tvStructure.getRoot().getChildren().contains(fakeItem)) {
            tvStructure.getRoot().getChildren().add(fakeItem);
        }
        for (String re : regexes) {
            String name = "additional";
            // normalize
            re = re.replaceAll("\\\\\\\\", "\\\\"); // all \\\\ to \\
            RegexNode regexNode = new RegexNode(name, re, re);
            StructureTreeItem item = new StructureTreeItem(regexNode);
            fakeItem.getChildren().add(item);
        }
    }

    @FXML
    void findMeaningfulStringsUsingNodesPool() {
        Configuration.Gen_Algorithm algorithm = Configuration.Gen_Algorithm.NODES_POOL;
        findMeaningfulStrings(algorithm);
    }

    @FXML
    void findMeaningfulStringsUsingEdgesPool() {
        Configuration.Gen_Algorithm algorithm = Configuration.Gen_Algorithm.EDGES_POOL;
        findMeaningfulStrings(algorithm);
    }

    @FXML
    void findMeaningfulStringsUsingEPPool() {
        Configuration.Gen_Algorithm algorithm = Configuration.Gen_Algorithm.EDGE_PAIRS_POOL;
        findMeaningfulStrings(algorithm);
    }

    @FXML
    void findMeaningfulStringsUsingEgret() {
        Configuration.Gen_Algorithm algorithm = Configuration.Gen_Algorithm.EGRET;
        findMeaningfulStrings(algorithm);
    }

    @FXML
    void findMeaningfulStringsUsingMutRex() {
        Configuration.Gen_Algorithm algorithm = Configuration.Gen_Algorithm.MUTREX;
        findMeaningfulStrings(algorithm);
    }

    private void findMeaningfulStrings(Configuration.Gen_Algorithm algorithm) {
        final FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().add(
                new FileChooser.ExtensionFilter("Text Files", "*.txt")
        );
        File file = fileChooser.showOpenDialog(MasterController.getPrimaryStage());
        if (file != null) {
            findMeaningfulStrings(file, algorithm);
        }
    }

    private void findMeaningfulStrings(File file, Configuration.Gen_Algorithm algorithm) {
        String content = Utils.readFileContent(file);
        String[] records = content.split("\n");
        // format: description \t target regex \t synthesized regex \t flag(string equivalent or dfa equivalent or dfa not equivalent)

        int stringEq = 0;
        int dfaEq = 0;
        int dfaNEq = 0;
        int detected = 0;

        int index = 0;
        String outputFileName = "detection-" + algorithm.name() + "-" + file.getName();
        String outputFile = file.getAbsolutePath().substring(0, file.getAbsolutePath().lastIndexOf(File.separator))
                + File.separator  + outputFileName;
        Utils.writeContentToFile("", outputFile);

        for (String record : records) {
            System.out.println(++index);
            String[] data = record.split("\\t");
            String correct = data[1];
            String incorrect = data[2];
            String flag = data[3];
            String name = "additional";

            if (flag.equals("DFA_EQ") || flag.equals("DFA_NEQ")) {
                RegexNode incorrectRe = new RegexNode(name, incorrect, incorrect);
                RegexNode correctRe = new RegexNode(name, correct, correct);

                Prog incorrectProg = new Prog(incorrectRe);
                Prog correctProg = new Prog(correctRe);

                System.out.println("Incorrect regex: " + incorrect);
                System.out.println("Correct regex: " + correct);
                System.out.println(flag);

                StringBuilder builder = new StringBuilder();
                builder.append(index).append("\t").append(data[0]).append("\t")
                        .append(correct).append("\t").append(incorrect).append("\t").append(flag);

                if (!incorrectProg.getDFA()) {
                    System.out.println("\nMaybe unsupported characters.\n");
                    builder.append("\t<unsupported>");

                    String oldContent = Utils.readFileContent(outputFile);
                    Utils.writeContentToFile(oldContent + builder, outputFile);
                    continue;
                }

                if (!correctProg.getDFA()) {
                    System.out.println("\nMaybe unsupported characters.\n");
                    builder.append("\t<unsupported>");

                    String oldContent = Utils.readFileContent(outputFile);
                    Utils.writeContentToFile(oldContent + builder, outputFile);
                    continue;
                }

                int nodesSize = incorrectProg.getStates().size();
                int tokenLength = incorrect.length();

                long timeStart = System.nanoTime();
                switch (algorithm) {
                    case EGRET:
                        incorrectProg.generateStringsUsingEgret();
                        break;
                    case EDGE_PAIRS_POOL:
                        incorrectProg.generateStringsEPPool();
                        break;
                    case MUTREX:
                        incorrectProg.generateStringsUsingMutRex();
                        break;
                    case EDGES_POOL:
                        incorrectProg.generateStringsEdgesPool();
                        break;
                    case NODES_POOL:
                        incorrectProg.generateStringsNodesPool();
                        break;
                    default:
                        break;
                }
                long timeEnd = System.nanoTime();
                long time = (timeEnd - timeStart) / 1000000;
                int negativeSize = incorrectProg.getTmpNegativeStrings().size();
                int positiveSize = incorrectProg.getTmpPositiveStrings().size();

                builder.append("\t").append(tokenLength).append("\t").append(nodesSize);
                builder.append("\t").append(time);
                builder.append("\t").append(positiveSize).append("\t").append(negativeSize);
;
                System.out.println("Checking for negative strings...\n");
                boolean isDetected = false;
                for (String neg : incorrectProg.getTmpNegativeStrings()) {
                    DynamicProg dynamicProg = correctProg.fullMatch(correctRe.getContent(), neg);
                    if (dynamicProg == null) {
                        System.out.println("Failed to get dynamic prog of the correct regex.\n");
                    } else {
                        if (dynamicProg.isMatch()) {
                            if (neg.equals(""))
                                neg = "<empty>";
                            builder.append("\t//").append(neg).append("//");
                            System.out.println("Evil string: " + neg);
                            isDetected = true;
                            break;
                        }
                    }
                }
                System.out.println("Checking for positive strings...");
                for (String pos : incorrectProg.getTmpPositiveStrings()) {
                    DynamicProg dynamicProg = correctProg.fullMatch(correctRe.getContent(), pos);
                    if (dynamicProg == null) {
                        System.out.println("Failed to get dynamic prog of the correct regex.\n");
                    } else {
                        if (!dynamicProg.isMatch()) { // should not be matched but matched
                            if (pos.equals(""))
                                pos = "<empty>";
                            builder.append("\t//").append(pos).append("//");
                            System.out.println("Evil string: " + pos);
                            isDetected = true;
                            break;
                        }
                    }
                }
                if (isDetected)
                    detected++;

                incorrectProg.getTmpPositiveStrings().clear();
                incorrectProg.getTmpNegativeStrings().clear();
                incorrectProg.getInputToDynamicProgMap().clear();
                correctProg.getInputToDynamicProgMap().clear();

                String oldContent = Utils.readFileContent(outputFile);
                Utils.writeContentToFile(oldContent + builder, outputFile);

                if (flag.equals("DFA_EQ")) {
                    dfaEq++;
                } else {
                    dfaNEq++;
                }
            } else if (flag.equals("STRING_EQ")) {
                stringEq++;
            }
        }

        System.out.println("STRING_EQ: " + stringEq);
        System.out.println("DFA_EQ: " + dfaEq);
        System.out.println("DFA_NEQ: " + dfaNEq);
        System.out.println("detected: " + detected);
    }

    @FXML
    public void addDirectory() {
        final DirectoryChooser directoryChooser = new DirectoryChooser();

        File directory = directoryChooser.showDialog(MasterController.getPrimaryStage());
        addDirectory(directory);
    }

    @FXML
    public void exportCaughtRegexes() {
        experimentGenerateStrings();
    }

    private void experimentGenerateStrings() {
        String distPath = Configuration.DATA_DIR + File.separator + "summary.txt";

        INode root = tvStructure.getRoot().getValue();
        List<CppFileNode> cppFileNodes = getAllCppFileNode(root);
        StringBuilder builder = new StringBuilder();
        int count = 0;
        for (int fi = 0; fi < cppFileNodes.size(); fi++) {
            CppFileNode fileNode = cppFileNodes.get(fi);
            for (int i = 0; i < fileNode.getChildren().size(); i++) {
                count++;
                if (count > 107) {
                    builder.append(count).append("\t").append(fileNode.getName()).append("\t");
                    builder.append(i + 1);
                    INode child = fileNode.getChildren().get(i);
                    Prog prog = new Prog((RegexNode) child);
                    prog.getDFA();

                    long startTime = System.nanoTime();
                    prog.generateStringsEPPool();
                    long endTime = System.nanoTime();
                    double genTime = ((double) endTime - startTime) / 1000000;

                    int numOfStrings = prog.getTmpNegativeStrings().size()
                            + prog.getTmpPositiveStrings().size();

                    builder.append("\t").append(((RegexNode) child).getContent());
                    builder.append("\t").append(genTime).append("\t").append(numOfStrings);
                    builder.append("\n");
                    Utils.writeContentToFile(builder.toString(), distPath);
                }
            }
        }


    }

    private void experimentVisualizationTime() {
        INode root = tvStructure.getRoot().getValue();
        List<CppFileNode> cppFileNodes = getAllCppFileNode(root);
        StringBuilder builder = new StringBuilder();
        int count = 0;
        for (int fi = 0; fi < cppFileNodes.size(); fi++) {
            CppFileNode fileNode = cppFileNodes.get(fi);
//            if (!fileNode.getChildren().isEmpty()) {
//                Long time = MasterController.extractingTimeMap.get(fileNode);
//                builder.append(++count).append("\t").append(fileNode.getName());
//                builder.append("\t").append(new File(fileNode.getAbsolutePath()).length());
//                builder.append("\t").append(fileNode.getChildren().size());
//                builder.append("\t").append(time / 1000000).append("\n");
//            }
//            builder.append("(").append(fi).append(") ")
//                    .append(fileNode.getName()).append("[")
//                    .append(fileNode.getChildren().size()).append("]\n");
            for (int i = 0; i < fileNode.getChildren().size(); i++) {
                builder.append(++count).append("\t").append(fileNode.getName()).append("\t");
//                Long time = MasterController.extractingTimeMap.get(fileNode);
                builder.append(i + 1);
                INode child = fileNode.getChildren().get(i);
                Prog prog = new Prog((RegexNode) child);
                int numStates = 0;
                int numEdges = 0;
                int numEdgePairs = 0;
                boolean getDFA = prog.getDFA();
                numStates = prog.getStates().size();
                numEdges = prog.getTransitions().size();
                prog.computeEdgePairs();
                numEdgePairs = prog.getEdgePairs().size();
                long startTime = System.nanoTime();
                prog.generateDotFile("/home/william/IdeaProjects/arext_2/dotsource.dot", new ArrayList<>(), new ArrayList<>());
                long endTime = System.nanoTime();
                double genGraph = ((double) endTime - startTime) / 1000000;

                builder.append("\t").append(getDFA);
                builder.append("\t").append(((RegexNode) child).getContent());
                builder.append("\t").append(((RegexNode) child).getContent().length());
                builder.append("\t").append(numStates);
                builder.append("\t").append(numEdges);
                builder.append("\t").append(numEdgePairs);
                builder.append("\t").append(genGraph);
                builder.append("\n");
            }
        }
//        for (int fi = 0; fi < cppFileNodes.size(); fi++) {
//            CppFileNode fileNode = cppFileNodes.get(fi);
//            builder.append("(").append(fi).append(") ")
//                    .append(fileNode.getName()).append("[")
//                    .append(fileNode.getChildren().size()).append("]\n");
//            for (int i = 0; i < fileNode.getChildren().size(); i++) {
//                INode child = fileNode.getChildren().get(i);
//                builder.append("\t[").append(i).append("]")
//                        .append(child.getName()).append("\n");
//                builder.append("\tcontent: ").append(((RegexNode) child).getContent())
//                        .append("\n");
//                builder.append("\traw: ").append(((RegexNode) child).getRawContent())
//                        .append("\n");
//            }
//            builder.append("\n");
//        }

        String distPath = Configuration.DATA_DIR + File.separator + "summary.txt";
        Utils.writeContentToFile(builder.toString(), distPath);
    }

    private List<CppFileNode> getAllCppFileNode(INode parent) {
        List<CppFileNode> cppFileNodes = new ArrayList<>();
        for (INode child : parent.getChildren()) {
            if (child instanceof CppFileNode) {
                cppFileNodes.add((CppFileNode) child);
            } else if (child instanceof FolderNode) {
                cppFileNodes.addAll(getAllCppFileNode(child));
            }
        }

        return cppFileNodes;
    }

    private void addDirectory(File directory) {
        if (directory != null && directory.exists()) {
            System.out.println("Add Directory");
            ProjectNode projectNode = MasterController.parseDirectory(directory);
            representStructureTree(projectNode);
        }
    }

    /**
     * represent structure of project onto the left pane
     *
     * @param projectNode project node.
     */
    public void representStructureTree(ProjectNode projectNode) {
        StructureTreeItem root = new StructureTreeItem(projectNode);
        tvStructure.setRoot(root);

        loadChildren(root);
    }

    public void loadChildren(StructureTreeItem parentItem) {
        parentItem.getChildren().clear();

        for (INode childNode : parentItem.getValue().getChildren()) {
            if (childNode instanceof CppFileNode
                    && childNode.getName().startsWith("AREXT_copy_")) { // this file is create by AREXT and don't need to show
                continue;
            }

            StructureTreeItem childItem = new StructureTreeItem(childNode);
            parentItem.getChildren().add(childItem);

            if (!getNodeToTreeItemMap().containsKey(childNode)) {
                getNodeToTreeItemMap().put(childNode, childItem);
            }

            loadChildren(childItem);
        }
    }

    public void loadChildren(INode parentNode) {
        if (getNodeToTreeItemMap().containsKey(parentNode)) {
            loadChildren(getNodeToTreeItemMap().get(parentNode));
        }
    }

    public static HashMap<INode, StructureTreeItem> getNodeToTreeItemMap() {
        return nodeToTreeItemMap;
    }

    public static Parent getBaseScenePane() {
        return baseScenePane;
    }

    public static BaseSceneController getInstance() {
        return instance;
    }
}
