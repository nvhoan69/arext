package gui.controllers;

import gui.graph.Prog;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;
import org.eclipse.cdt.core.dom.ast.*;
import org.eclipse.cdt.core.dom.ast.cpp.ICPPASTBinaryExpression;
import org.eclipse.cdt.core.dom.ast.cpp.ICPPASTLiteralExpression;
import org.eclipse.cdt.internal.core.dom.parser.cpp.*;
import parser.ProjectParser;
import parser.SourcecodeFileParser;
import parser.object.*;
import search.Search;
import search.condition.AbstractFunctionNodeCondition;
import util.SpecialCharacter;
import util.Utils;
import util.VariableTypeUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MasterController {
    public static Stage primaryStage = null;
    private final static Map<RegexNode, Prog> regexNodeToProgMap = new HashMap<>();
    private final static Map<CppFileNode, String> fileToMacroMap = new HashMap<>();
    public final static Map<CppFileNode, Long> extractingTimeMap = new HashMap<>();

    public static Alert showDialog(Alert.AlertType type, String content, String title) {
        Alert alert = new Alert(type, content, ButtonType.OK);
        alert.setTitle(title);
        alert.initOwner(primaryStage);
        return alert;
    }

    public static ProjectNode parseDirectory(File projectDirectory) {
        ProjectParser parser = new ProjectParser(projectDirectory);

        return parser.getRootTree();
    }

    public static void extractRegexesInAFile(CppFileNode cppFileNode) {
        long startTime = System.nanoTime();
        SourcecodeFileParser cppParser = new SourcecodeFileParser();
        cppFileNode.getChildren().clear();
        try {
            INode root = cppParser.parseSourcecodeFile(new File(cppFileNode.getUsedForProcessPath()));

            boolean anyFunctionSyntaxError = false;
            String fileContent = Utils.readFileContent(cppFileNode);

            // change TEST(...) {...} to void TEST(...) {...}, then reparse
            for (INode node : root.getChildren()) {
                if (node instanceof UnspecifiedDeclaration) {
                    IASTNode ast = ((UnspecifiedDeclaration) node).getAST();
                    if (ast instanceof CPPASTProblemDeclaration) {
                        String rawSignature = ((CPPASTProblemDeclaration) ast).getProblem().getRawSignature();
                        Pattern pattern = Pattern.compile("^\\s*[a-zA-Z0-9_]+\\s*\\(([^)]|\\s)*\\)\\s*\\{");
                        Matcher matcher = pattern.matcher(rawSignature);
                        if (matcher.find()) {
                            fileContent = fileContent.replace(rawSignature, "void " + rawSignature);
                            anyFunctionSyntaxError = true;
                        }
                    }
                }
            }

            if (anyFunctionSyntaxError) {
                if (!cppFileNode.isCopied()) {
                    cppFileNode.makeACopy();
                }
                String filePath = cppFileNode.getUsedForProcessPath();
                Utils.writeContentToFile(fileContent, filePath);
                root = cppParser.parseSourcecodeFile(new File(filePath));
            }

            // EXTRACTING
            // Step 1: get all external variable of the file, handle if it is a regex
            Map<String, String> externalVariablesMap = new HashMap<>();
            List<RegexNode> regexNodes = new ArrayList<>(); // to contain all regex node found in this file
            List<String> contentCheckList = new ArrayList<>();

            for (INode node : root.getChildren()) {
                if (node instanceof ExternalVariableNode) {
                    IASTInitializer initializer = ((ExternalVariableNode) node).getInitializer();
                    if (initializer instanceof CPPASTEqualsInitializer) {
                        IASTNode iastNode = initializer.getChildren()[0];
                        IASTExpression expression;
                        if (iastNode instanceof CPPASTInitializerList) {
                            expression = (IASTExpression) iastNode.getChildren()[0];
                        } else {
                            expression = (IASTExpression) initializer.getChildren()[0];
                        }
                        String value = getStringValue(expression, externalVariablesMap);
                        if (value != null && !value.isEmpty()) {
                            externalVariablesMap.put(node.getName(), value);
                        }
                    }

                    String type = ((ExternalVariableNode) node).getASTType().toString();
                    if (isRegexType(type)) {
                        RegexNode regexNode = handleToCreateRegexNode(node.getName(), initializer, externalVariablesMap);
                        assert regexNode != null;
                        if (!contentCheckList.contains(regexNode.getContent())) {
                            regexNodes.add(regexNode);
                            contentCheckList.add(regexNode.getContent());
                        }
                    }
                } else if (node instanceof MacroDefinitionNode) {
                    String name = node.getName();
                    String value = ((MacroDefinitionNode) node).getValue();
                    externalVariablesMap.put(name, value);
                }
            }

            // Step 2: handle namespaces
            // extract regexes defined out of any functions
            List<NamespaceNode> namespaceNodes = getNamespaceNode(root);
            List<IASTNode> handledASTNodes = new ArrayList<>();
            for (NamespaceNode namespaceNode : namespaceNodes) {
                List<RegexNode> regexes = visitASTToGetRegexes(namespaceNode.getAST(),
                        externalVariablesMap, handledASTNodes);
                for (RegexNode regexNode : regexes) {
                    if (!contentCheckList.contains(regexNode.getContent())) {
                        regexNodes.add(regexNode);
                        contentCheckList.add(regexNode.getContent());
                    }
                }
            }
            // Step 3: handle remaining functions (functions that out of any namespace)
            List<IFunctionNode> functionNodes = Search.searchNodes(root, new AbstractFunctionNodeCondition());
            for (IFunctionNode functionNode : functionNodes) { // extract all regexes in functions
                List<RegexNode> regexes = visitASTToGetRegexes(functionNode.getAST()
                        , externalVariablesMap, handledASTNodes);
                for (RegexNode regexNode : regexes) {
                    if (!contentCheckList.contains(regexNode.getContent())) {
                        regexNodes.add(regexNode);
                        contentCheckList.add(regexNode.getContent());
                    }
                }
            }

            cppFileNode.getChildren().addAll(regexNodes);
            BaseSceneController.getInstance().loadChildren(cppFileNode);

        } catch (Exception e) {
            e.printStackTrace();
        }

        long endTime = System.nanoTime();
        long time = endTime - startTime;
        extractingTimeMap.put(cppFileNode, time);
    }

    private static List<RegexNode> visitASTToGetRegexes(IASTNode ast, Map<String, String> predeclaredVarMap
            , List<IASTNode> handledASTNodes) {
        Map<String, String> variablesMap = new HashMap<>(predeclaredVarMap);
        List<RegexNode> regexNodes = new ArrayList<>();
        List<String> caughtRegexValues = new ArrayList<>();

        ASTVisitor visitor = new ASTVisitor() {
            @Override
            public int visit(IASTDeclaration declaration) {
                if (handledASTNodes.contains(declaration)) {
                    return PROCESS_CONTINUE;
                }

                handledASTNodes.add(declaration);
                if (declaration instanceof IASTSimpleDeclaration) {
                    IASTSimpleDeclaration simpleDeclaration = (IASTSimpleDeclaration) declaration;
//                    simpleDeclaration.getDeclSpecifier().getRawSignature();
//                            System.out.println(simpleDeclaration.getDeclSpecifier().getRawSignature());

                    if (((IASTSimpleDeclaration) declaration).getDeclarators().length == 0) {
                        return PROCESS_CONTINUE;
                    }

                    IASTDeclarator declarator = ((IASTSimpleDeclaration) declaration).getDeclarators()[0]; // int x, y => 2 declarators;
                    String nameVar = declarator.getName().toString();
                    IASTInitializer initializer = declarator.getInitializer();
                    if (initializer == null) {
                        return PROCESS_CONTINUE; // ignore
                    }

                    IASTDeclSpecifier specifier = simpleDeclaration.getDeclSpecifier();
                    String type = specifier.getRawSignature();
                    type = VariableTypeUtils.removeRedundantKeyword(type);

                    // catch all string variables
                    if (initializer instanceof CPPASTEqualsInitializer) {
                        IASTExpression expression;
                        IASTNode node = initializer.getChildren()[0];
                        if (node instanceof CPPASTInitializerList) {
                            if (node.getChildren()[0] instanceof IASTExpression) {
                                expression = (IASTExpression) node.getChildren()[0];
                            } else {
                                return PROCESS_CONTINUE; // weird case
                            }
                        } else {
                            expression = (IASTExpression) initializer.getChildren()[0];
                        }
                        String valueVar = getStringValue(expression, variablesMap);
                        if (valueVar != null) {
                            variablesMap.put(nameVar, valueVar);
                        }
                    } else if (initializer instanceof CPPASTConstructorInitializer) { // handle std::string("string");
                        if (isStringType(type)) {
                            IASTExpression expression = (IASTExpression) initializer.getChildren()[0];
                            String valueVar = getStringValue(expression, variablesMap);
                            if (valueVar != null) {
                                variablesMap.put(nameVar, valueVar);
                            }
                        }
                    }

                    boolean isRegex = false;
                    if (isRegexType(type))
                        isRegex = true;
                    else {
                        if (initializer instanceof CPPASTEqualsInitializer) {
                            if (initializer.getChildren()[0] instanceof IASTExpression) {
                                IASTExpression expression = (IASTExpression) initializer.getChildren()[0];
                                if (expression instanceof CPPASTFunctionCallExpression) {
                                    if (isRegexType(((CPPASTFunctionCallExpression) expression).getFunctionNameExpression()
                                            .getRawSignature())) {
                                        isRegex = true;
                                    }
                                }
                            }
                        }
                    }

                    if (isRegex) {
                        RegexNode regexNode = handleToCreateRegexNode(nameVar, initializer, variablesMap);
                        if (regexNode != null && !caughtRegexValues.contains(regexNode.getContent())) {
                            regexNodes.add(regexNode);
                            caughtRegexValues.add(regexNode.getContent());
                        }
                    }
                }

                return PROCESS_CONTINUE;
            }

            @Override
            public int visit(IASTExpression expression) {
                if (handledASTNodes.contains(expression)) {
                    return PROCESS_CONTINUE;
                }

                handledASTNodes.add(expression);
                if (expression instanceof CPPASTFunctionCallExpression) {
                    String funcName = ((CPPASTFunctionCallExpression) expression).getFunctionNameExpression().toString();
                    if (isRegexType(funcName)) {
                        IASTExpression exp = (IASTExpression) ((CPPASTFunctionCallExpression) expression).getArguments()[0];
                        String rawValue = exp.getRawSignature();
                        String value = getStringValue(exp, variablesMap);
                        if (value != null && !value.isEmpty()
                                && !caughtRegexValues.contains(value)) {
                            regexNodes.add(new RegexNode("NoNameRegex", rawValue, value));
                            caughtRegexValues.add(value);
                        }
                    }
                }
                return PROCESS_CONTINUE;
            }
        };

        visitor.shouldVisitDeclarations = true;
        visitor.shouldVisitExpressions = true;
        ast.accept(visitor);

        return regexNodes;
    }

    private static boolean isRegexType(String type) {
        type = type.replace("std::", SpecialCharacter.EMPTY);
        return type.equals("regex") || type.equals("wregex") || type.equals("basic_regex");
    }

    private static boolean isStringType(String type) {
        type = type.replace("std::", SpecialCharacter.EMPTY);
        return type.equals("string") || type.equals("wstring");
    }

    private static RegexNode handleToCreateRegexNode(String name, IASTInitializer initializer, Map<String, String> predeclaredVarMap) {
        String rawValue = null;
        String value = null;
        if (initializer instanceof CPPASTConstructorInitializer) {
            IASTExpression expression = (IASTExpression) ((CPPASTConstructorInitializer) initializer).getArguments()[0];
            rawValue = expression.getRawSignature();
            value = getStringValue(expression, predeclaredVarMap);
            if (value == null || value.isEmpty()) {
                return null;// ignore
            }

        } else if (initializer instanceof CPPASTEqualsInitializer) {
            IASTExpression expression = (IASTExpression) initializer.getChildren()[0];
            IASTExpression regexContentExpression = null;
            if (expression instanceof CPPASTFunctionCallExpression) {
                regexContentExpression = (IASTExpression)
                        ((CPPASTFunctionCallExpression) expression).getArguments()[0];
            } else if (expression instanceof CPPASTSimpleTypeConstructorExpression) {
                regexContentExpression = (IASTExpression)
                        ((CPPASTSimpleTypeConstructorExpression) expression).getInitializer().getChildren()[0];
            }

            if (regexContentExpression != null) {
                rawValue = regexContentExpression.getRawSignature();
                value = getStringValue(regexContentExpression, predeclaredVarMap);
            }
        } else if (initializer instanceof CPPASTInitializerList) {
            /* todo: need to handle the case that have more than one initializer */
            for (IASTNode child : initializer.getChildren()) {
                if (child instanceof IASTExpression) {
                    rawValue = child.getRawSignature();
                    value = getStringValue((IASTExpression) child, predeclaredVarMap);
                }
            }
        }

        if (rawValue != null && value != null && !value.isEmpty()) {
            return new RegexNode(name, rawValue, value);
        }
        return null;
    }

    private static List<NamespaceNode> getNamespaceNode(INode root) {
        List<NamespaceNode> namespaceNodes = new ArrayList<>();
        for (INode node : root.getChildren()) {
            if (node instanceof NamespaceNode) {
                namespaceNodes.add((NamespaceNode) node);
                namespaceNodes.addAll(getNamespaceNode(node));
            }
        }
        return namespaceNodes;
    }

    /**
     * From raw signature of expression, convert to final real string value
     * e.g. "^" + url_regex + "/?$", and url_regex equals to "https?://[^\\s\"<>]+"
     * then the value is "^" + "https?://[^\\s\"<>]+" + "/?$"
     * and then the value is normalized
     *
     * @param expression      an expression describe the regex pattern (e.g. maybe "string" + var_name + "string")
     * @param idExpressionMap a map contains predeclared variable, is used for convert the regex pattern to real value
     * @return the real value after replace variables in the origin pattern with their values
     * and return null if can not resolve the expression
     */
    private static String getStringValue(IASTExpression expression, Map<String, String> idExpressionMap) {
        StringBuilder builder = new StringBuilder();
        String value = null;
        boolean isNormalized = false;
        if (expression instanceof ICPPASTBinaryExpression) {
            IASTExpression operand1 = ((ICPPASTBinaryExpression) expression).getOperand1();
            IASTExpression operand2 = ((ICPPASTBinaryExpression) expression).getOperand2();
            String op1 = getStringValue(operand1, idExpressionMap);
            String op2 = getStringValue(operand2, idExpressionMap);
            if (op1 == null || op2 == null) { // can not resolve
                return null;
            }

            builder.append(op1)
                    .append(op2);
            isNormalized = true;
        } else if (expression instanceof ICPPASTLiteralExpression) {
//            value = expression.getRawSignature();
            value = expression.toString(); // if Macro is used, then the expression.toString() is the real value
            // handle Raw string literal
            if (value.startsWith("R") || value.startsWith("LR")) { // handle raw string
                value = value.substring(value.indexOf("(") + 1, value.lastIndexOf(")"));
                isNormalized = true;
            }
        } else if (expression instanceof CPPASTIdExpression) {
            CPPASTIdExpression idExpression = (CPPASTIdExpression) expression;
            if (idExpressionMap.containsKey(idExpression.getName().toString())) {
                value = idExpressionMap.get(idExpression.getName().toString());

                if (value.startsWith("R")) { // handle raw string
                    value = value.substring(value.indexOf("(") + 1, value.lastIndexOf(")"));
                }
                isNormalized = true; // the value was normalized before put to the map
            } else {
                return null; // that means can not resolve
            }
        } else {
            return null; // can not resolve
        }

        if (!isNormalized && !value.isEmpty()) {
            if (value.contains("\"")) {
                value = value.substring(value.indexOf("\"") + 1, value.lastIndexOf("\""));
            }
            String[] strings = value.split("[^\\\\]\"\\s*\""); // for ("string"\n "string) and not ("string and escape \"    " "string")
            value = String.join("", strings);
            value = value.replaceAll("\\\\\\\\", "\\\\"); // all \\\\ to \\
        }

        if (value != null) {
            builder.append(value);
        }
        return builder.toString();
    }

    public static void setPrimaryStage(Stage primaryStage) {
        MasterController.primaryStage = primaryStage;
    }

    public static Stage getPrimaryStage() {
        return primaryStage;
    }

    public static Map<RegexNode, Prog> getRegexNodeToProgMap() {
        return regexNodeToProgMap;
    }

    public static Map<CppFileNode, String> getFileToMacroMap() {
        return fileToMacroMap;
    }

    /**
     * check if the list of bytes (int values) is valid to convert to character UTF-8.
     *
     * @param integers list of bytes (int values)
     * @return true if valid, vice versus
     */
    public static boolean isValidCharacterSequence(List<Integer> integers) {
        if (integers.isEmpty()) {
            return false;
        }
        if (integers.size() == 1) {
            return integers.get(0) >= 0 && integers.get(0) <= 127;
        }

        if (integers.get(0) >= 0 && integers.get(0) <= 127) { // ascii code <0-127>
//            if (integers.size() == 1) {
//                return true;
//            } else
//                return isValidCharacterSequence(integers.subList(1, integers.size())); // size > 1
            return isValidCharacterSequence(integers.subList(1, integers.size())); // size > 1

        } else if (integers.get(0) >= 194 && integers.get(0) <= 223) { // <194-223><128-191>
//            if (integers.size() < 2)
//                return false;

            if (integers.get(1) >= 128 && integers.get(1) <= 191) {
                if (integers.size() == 2) {
                    return true;
                }
                return isValidCharacterSequence(integers.subList(2, integers.size())); // size > 2
            }

        } else if (integers.get(0) == 224) { // <224><160-191><128-191>
            if (integers.size() < 3)
                return false;

            if (integers.get(1) >= 160 && integers.get(1) <= 191
                    && integers.get(2) >= 128 && integers.get(2) <= 191) {
                if (integers.size() == 3) {
                    return true;
                }
                return isValidCharacterSequence(integers.subList(3, integers.size())); // size > 3
            }

        } else if (integers.get(0) >= 225 && integers.get(0) <= 239) { // <225-239><128-191><128-191>
            if (integers.size() < 3)
                return false;

            if (integers.get(1) >= 128 && integers.get(1) <= 191
                    && integers.get(2) >= 128 && integers.get(2) <= 191) {
                if (integers.size() == 3) {
                    return true;
                }
                return isValidCharacterSequence(integers.subList(3, integers.size())); // size > 3
            }

        } else if (integers.get(0) == 240) { // <240><144-191><128-191><128-191> (48x64x64)
            if (integers.size() < 4)
                return false;

            if (integers.get(1) >= 144 && integers.get(1) <= 191
                    && integers.get(2) >= 128 && integers.get(2) <= 191
                    && integers.get(3) >= 128 && integers.get(3) <= 191) {
                if (integers.size() == 4) {
                    return true;
                }
                return isValidCharacterSequence(integers.subList(4, integers.size())); // size > 4
            }

        } else if (integers.get(0) >= 241 && integers.get(0) <= 243) {
            if (integers.size() < 4)
                return false;

            if (integers.get(1) >= 128 && integers.get(1) <= 191
                    && integers.get(2) >= 128 && integers.get(2) <= 191
                    && integers.get(3) >= 128 && integers.get(3) <= 191) {
                if (integers.size() == 4) {
                    return true;
                }
                return isValidCharacterSequence(integers.subList(4, integers.size())); // size > 4
            }

        } else if (integers.get(0) == 244) { // <244><128-143><128-191><128-191>
            if (integers.size() < 4)
                return false;

            if (integers.get(1) >= 128 && integers.get(1) <= 143
                    && integers.get(2) >= 128 && integers.get(2) <= 191
                    && integers.get(3) >= 128 && integers.get(3) <= 191) {
                if (integers.size() == 4) {
                    return true;
                }
                return isValidCharacterSequence(integers.subList(4, integers.size())); // size > 4
            }
        }

        return false;
    }
}
