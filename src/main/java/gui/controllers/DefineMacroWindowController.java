package gui.controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextArea;
import javafx.stage.Modality;
import javafx.stage.Stage;
import parser.object.CppFileNode;
import util.Utils;

import java.io.IOException;
import java.util.Map;

public class DefineMacroWindowController {

    private Stage stage = null;
    private CppFileNode fileNode = null;

    @FXML
    private TextArea taMacros;

    public static DefineMacroWindowController getInstance(CppFileNode fileNode) {
        FXMLLoader loader = new FXMLLoader(DefineMacroWindowController.class.getResource("/FXML/DefineMacroWindow.fxml"));
        try {
            Parent parent = loader.load();
            Scene scene = new Scene(parent);
            Stage stage = new Stage();
            stage.setScene(scene);

            DefineMacroWindowController controller = loader.getController();
            controller.setStage(stage);
            controller.setFileNode(fileNode);

            Map<CppFileNode, String> fileNodeStringMap = MasterController.getFileToMacroMap();
            if (fileNodeStringMap.containsKey(fileNode)) {
                controller.taMacros.setText(fileNodeStringMap.get(fileNode));
            }

            return controller;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public void showWindowAndWait() {
        if (stage != null) {
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner(MasterController.getPrimaryStage());
            stage.showAndWait();
        }
    }

    @FXML
    public void ok() {
        Map<CppFileNode, String> fileNodeStringMap = MasterController.getFileToMacroMap();
        if (fileNodeStringMap.containsKey(fileNode)) {
            fileNodeStringMap.remove(fileNode);
        }
        fileNodeStringMap.put(fileNode, taMacros.getText());

        try {
            if (!fileNode.isCopied())
                fileNode.makeACopy();
            String fileContent = Utils.readFileContent(fileNode.getUsedForProcessPath());

            String upperBound = "// AREXT MACRO ADDITION START\n";
            String lowerBound = "// AREXT MACRO ADDITION END\n";

            if (fileContent.contains(lowerBound)) {
                int index = fileContent.indexOf(lowerBound);
                fileContent = fileContent.substring(index);
                fileContent = upperBound + taMacros.getText() + fileContent;
            } else {
                fileContent = upperBound + taMacros.getText() + lowerBound + fileContent;
            }

            Utils.writeContentToFile(fileContent, fileNode.getUsedForProcessPath());
        } catch (IOException e) {
            e.printStackTrace();
        }

        stage.close();
    }

    @FXML
    public void cancel() {
        stage.close();
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public void setFileNode(CppFileNode fileNode) {
        this.fileNode = fileNode;
    }
}
