package gui.controllers;

import gui.Configuration;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import parser.object.RegexNode;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class GraphViewController {
    @FXML
    private Slider sliderZoomLvl;
    @FXML
    private ScrollPane scrollPane;
    @FXML
    private VBox vbox;
    @FXML
    private Label zoomLevel;
    @FXML
    private Label lGraphFilePath;

    private Stage stage;

    private RegexNode regexNode;
    private double source_width, source_height;
    private static double zoomlvl;

    public static GraphViewController getInstance() {
        FXMLLoader loader = new FXMLLoader(GraphViewController
                .class.getResource("/FXML/GraphView.fxml"));
        try {
            Parent parent = loader.load();
            GraphViewController controller = loader.getController();
            Stage stage = new Stage();
            stage.setScene(new Scene(parent));
            controller.setStage(stage);
            return controller;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public void showWindow() {
        stage.show();
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public void setRegexNode(RegexNode regexNode) {
        this.regexNode = regexNode;
    }

    public void initView() {
        stage.setTitle(regexNode.getName());

        String path = Configuration.GRAPH_DATA_DIR + File.separator + regexNode.getName()
                + ".png";
        lGraphFilePath.setText(path);

        Image source = null;
        try {
            source = new Image(new FileInputStream(path));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        assert source != null;

        ImageView image = new ImageView(source);

        double view_height = scrollPane.getPrefHeight() - 10;// -10 for hide the no need scroolbar
        double view_width = scrollPane.getPrefWidth() - 10;

        source_width = source.getWidth();
        source_height = source.getHeight();
        double ratio = source_width / source_height;

        double tmp_width;
        double tmp_height;
        if (view_height * ratio >= view_width) { // should fit the width
            tmp_width = view_width;
            tmp_height = view_width / ratio;
        } else { // should fit the height
            tmp_width = view_height * ratio;
            tmp_height = view_height;
        }

        image.setFitHeight((int) tmp_height);
        image.setFitWidth((int) tmp_width);
        vbox.getChildren().add(image);

        sliderZoomLvl.setValue((int) (tmp_width / source_width * 100 / 2));
        zoomLevel.setText(((int) sliderZoomLvl.getValue() * 2) + "%");
        sliderZoomLvl.valueProperty().addListener(e->{
            zoomlvl = sliderZoomLvl.getValue();
            double newValue = zoomlvl * 2 / 100;
            zoomLevel.setText(((int) sliderZoomLvl.getValue() * 2) + "%");

            image.setFitWidth(source_width * newValue);
            image.setFitHeight(source_height * newValue);
        });
    }
}
