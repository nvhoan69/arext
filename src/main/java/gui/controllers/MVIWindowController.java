package gui.controllers;

import gui.file_view.FXFileView;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.AnchorPane;
import parser.object.CppFileNode;
import parser.object.RegexNode;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

/**
 * Multiple View Interface Window, located in the right of the Arext application
 */
public class MVIWindowController implements Initializable {
    @FXML
    private Tab tabSourceCode;
    @FXML
    private TabPane tpMVITabPane;
    @FXML
    private Tab tabRegex;
    @FXML
    private TabPane tpSourceCodeTabPane;
    @FXML
    private TabPane tpRegexTabPane;

    private final Map<String, Tab> nameToRegexTab = new HashMap<>();
    private final Map<String, Tab> nameToSourceCodeTab = new HashMap<>();
    private final Map<RegexNode, RegexTabViewController> regexToViewControllerMap = new HashMap<>();

    private final static int MAX_TAB = 10;

    /**
     * singleton like pattern
     */
    private static MVIWindowController instance = null;
    private static AnchorPane mviWindowPane = null;

    public static void prepare() {
        FXMLLoader loader = new FXMLLoader(MVIWindowController.class.getResource("/FXML/MVIWindow.fxml"));
        try {
            mviWindowPane = loader.load();
            instance = loader.getController();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void initialize(URL var1, ResourceBundle var2) {
        // clear redundant data in design fxml
        tpMVITabPane.getTabs().clear();
        tpRegexTabPane.getTabs().clear();
        tpSourceCodeTabPane.getTabs().clear();

        tpMVITabPane.setTabClosingPolicy(TabPane.TabClosingPolicy.ALL_TABS);
        tpRegexTabPane.setTabClosingPolicy(TabPane.TabClosingPolicy.ALL_TABS);
        tpSourceCodeTabPane.setTabClosingPolicy(TabPane.TabClosingPolicy.ALL_TABS);

        tabSourceCode.setOnClosed(event -> {
            tpSourceCodeTabPane.getTabs().clear();
            nameToSourceCodeTab.clear();
        });

        tabRegex.setOnClosed(event -> {
            tpRegexTabPane.getTabs().clear();
            nameToRegexTab.clear();
            regexToViewControllerMap.clear();
        });
    }

    public void viewSourceCode(CppFileNode cppFileNode) {
        if (!tpMVITabPane.getTabs().contains(tabSourceCode)) {
            tpMVITabPane.getTabs().add(tabSourceCode);
        }
        tpMVITabPane.getSelectionModel().select(tabSourceCode);
        tpMVITabPane.getSelectionModel().select(tabSourceCode);

        FXFileView fxFileView = new FXFileView(cppFileNode.getAbsolutePath());
        String fileName = cppFileNode.getName();

        if (nameToSourceCodeTab.containsKey(fileName)) {
            tpSourceCodeTabPane.getSelectionModel().select(nameToSourceCodeTab.get(fileName));
            return;
        }

        Tab tab = new Tab(fileName);
        tab.setContent(fxFileView.getAnchorPane(false));
        tab.setOnCloseRequest(event -> {
            nameToSourceCodeTab.remove(tab.getText());
        });

        // control max tab
        if (tpSourceCodeTabPane.getTabs().size() == MAX_TAB) {
            Tab shouldBeClosedTab = tpSourceCodeTabPane.getTabs().get(0);
            tpSourceCodeTabPane.getTabs().remove(0);
            nameToSourceCodeTab.remove(shouldBeClosedTab.getText());
        }

        nameToSourceCodeTab.put(fileName, tab);
        tpSourceCodeTabPane.getTabs().add(tab);
        tpSourceCodeTabPane.getSelectionModel().select(tab);
    }

    public void viewRegex(RegexNode regexNode) {
        if (!tpMVITabPane.getTabs().contains(tabRegex)) {
            tpMVITabPane.getTabs().add(tabRegex);
        }
        tpMVITabPane.getSelectionModel().select(tabRegex);
        tpMVITabPane.getSelectionModel().select(tabRegex);

        String regexName = regexNode.getName();
        if (nameToRegexTab.containsKey(regexName)) {
            tpRegexTabPane.getSelectionModel().select(nameToRegexTab.get(regexName));
            return;
        }

        RegexTabViewController regexTabViewController = RegexTabViewController.getInstance();
        if (regexTabViewController != null) {
            Tab tab = new Tab(regexName);
            tab.setContent(regexTabViewController.getRegexTabViewPane());
            regexTabViewController.setRegexNode(regexNode);
            regexTabViewController.representRegex();
            tab.setOnCloseRequest(event -> {
                nameToRegexTab.remove(tab.getText());
                regexToViewControllerMap.remove(regexNode);
            });

            // control max tab
            if (tpRegexTabPane.getTabs().size() == MAX_TAB) {
                Tab shouldBeClosedTab = tpRegexTabPane.getTabs().get(0);
                tpRegexTabPane.getTabs().remove(0);
                nameToRegexTab.remove(shouldBeClosedTab.getText());
            }

            nameToRegexTab.put(regexName, tab);
            tpRegexTabPane.getTabs().add(tab);
            tpRegexTabPane.getSelectionModel().select(tab);

            regexToViewControllerMap.put(regexNode,regexTabViewController);
        } else {
            System.out.println("AREXT ERROR: failed to create regex tab view");
        }
    }

    public AnchorPane getMviWindowPane() {
        return mviWindowPane;
    }

    public static MVIWindowController getInstance() {
        if (instance == null) {
            prepare();
        }
        return instance;
    }

    public Map<RegexNode, RegexTabViewController> getRegexToViewControllerMap() {
        return regexToViewControllerMap;
    }
}
