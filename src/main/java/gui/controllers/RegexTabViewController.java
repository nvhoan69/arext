package gui.controllers;

import gui.Configuration;
import gui.graph.DynamicProg;
import gui.graph.Prog;
import gui.graph.State;
import gui.graph.Transition;
import gui.objects.StringInput;
import gui.objects.StringInputListCell;
import javafx.collections.ListChangeListener;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import parser.object.RegexNode;

import java.io.*;
import java.util.*;

public class RegexTabViewController implements Initializable {
    @FXML
    private TextArea taRegexContent;
    @FXML
    private ScrollPane spPreviewGraph;
    @FXML
    private VBox vbPreviewGraph;
    @FXML
    private AnchorPane apVisualizationPane;
    @FXML
    private TextArea taCoverage;
    @FXML
    private TextField tfAddString;
    @FXML
    private SplitPane spRegexRepresent;
    @FXML
    private ListView<StringInput> lvPositive;
    @FXML
    private ListView<StringInput> lvNegative;
    @FXML
    private Tab tabPositive;
    @FXML
    private Tab tabNegative;

    private RegexNode regexNode;

    private AnchorPane regexTabViewPane = null;
    private Algorithm usingAlgorithms = Algorithm.OPTIMIZE;

    public void initialize(java.net.URL url, java.util.ResourceBundle resourceBundle) {
        tfAddString.setOnKeyReleased(keyEvent -> {
            if (keyEvent.getCode().equals(KeyCode.ENTER)) {
                String input = tfAddString.getText();
                computeInput(input, true);
                syntheticAllDynamic();
            }
        });
        tabNegative.setText("Negative[0]");
        tabPositive.setText("Positive[0]");
        lvPositive.getItems().addListener((ListChangeListener<StringInput>) change -> {
            if (change != null) {
                updateTabPositive();
            }
        });
        lvNegative.getItems().addListener((ListChangeListener<StringInput>) change -> {
            if (change != null) {
                updateTabNegative();
            }
        });

        // Constrain min size of the apVisualization
        apVisualizationPane.minWidthProperty().bind(spRegexRepresent.widthProperty().multiply(0.6));
        lvPositive.setCellFactory(stringInputListView -> new StringInputListCell());
        lvPositive.setOnMouseClicked(mouseEvent -> {
            StringInput selected = lvPositive.getSelectionModel().getSelectedItem();
            if (selected != null) {
                if (selected.getState() == StringInput.State.STRING_VALUE) {
                    selected.setState(StringInput.State.UTF_8_BYTE_VALUES);
                } else {
                    selected.setState(StringInput.State.STRING_VALUE);
                }
                lvPositive.refresh();
            }
        });
        lvNegative.setCellFactory(stringInputListView -> new StringInputListCell());
        lvNegative.setOnMouseClicked(mouseEvent -> {
            StringInput selected = lvNegative.getSelectionModel().getSelectedItem();
            if (selected != null) {
                if (selected.getState() == StringInput.State.STRING_VALUE) {
                    selected.setState(StringInput.State.UTF_8_BYTE_VALUES);
                } else {
                    selected.setState(StringInput.State.STRING_VALUE);
                }
                lvNegative.refresh();
            }
        });


    }

    public AnchorPane getRegexTabViewPane() {
        return regexTabViewPane;
    }

    public static RegexTabViewController getInstance() {
        FXMLLoader loader = new FXMLLoader(RegexTabViewController
                .class.getResource("/FXML/RegexTabView.fxml"));
        try {
            AnchorPane regexTabViewPane = loader.load();
            RegexTabViewController instance = loader.getController();
            instance.setRegexTabViewPane(regexTabViewPane);

            return instance;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * @param input string input
     * @return realpath with the input
     */
    public String computeInput(String input, boolean updateGUI) {
        String regex = regexNode.getContent();
        DynamicProg dynamicProg = MasterController.getRegexNodeToProgMap()
                .get(regexNode).fullMatch(regex, input);
        List<State> coveredStates = new ArrayList<>(dynamicProg.getStateMap().values());

        StringInput stringInput = new StringInput(input);

        if (updateGUI) {
            if (dynamicProg.isMatch()) {
                if (!lvPositive.getItems().contains(stringInput)) {
                    lvPositive.getItems().add(stringInput);
                }
            } else {
                if (!lvNegative.getItems().contains(stringInput)) {
                    lvNegative.getItems().add(stringInput);
                }
            }
        }

        Prog staticProg = MasterController.getRegexNodeToProgMap().get(regexNode);
        if (!staticProg.getInputToDynamicProgMap().containsKey(input)) {
            staticProg.getInputToDynamicProgMap().put(input, dynamicProg);
        }

        return dynamicProg.getPath();
    }

    private void updateTabPositive() {
        String oldName = tabPositive.getText();
        String newName = oldName.substring(0, oldName.indexOf("["));
        newName += "[" + lvPositive.getItems().size() + "]";
        tabPositive.setText(newName);
    }

    private void updateTabNegative() {
        String oldName = tabNegative.getText();
        String newName = oldName.substring(0, oldName.indexOf("["));
        newName += "[" + lvNegative.getItems().size() + "]";
        tabNegative.setText(newName);
    }

    public void syntheticAllDynamic() {
        Prog staticProg = MasterController.getRegexNodeToProgMap().get(regexNode);
        String target = Configuration.GRAPH_DATA_DIR + File.separator
                + regexNode.getName();

        System.out.println("coverage computing starting.");

        List<Transition> coveredTransitions = new ArrayList<>();
        List<gui.graph.State> coveredStates = new ArrayList<>();
        List<String> coveredEdgePairs = new ArrayList<>();
        for (DynamicProg dynamic : staticProg.getInputToDynamicProgMap().values()) { // synthetic all dynamicProg
            for (gui.graph.State state : dynamic.getStateMap().values()) {
                if (!coveredStates.contains(state))
                    coveredStates.add(state);
            }
            for (Transition transition : dynamic.getCoveredTransitions()) {
                if (!coveredTransitions.contains(transition)) {
                    coveredTransitions.add(transition);
                }
            }
            for (String edgePair : dynamic.getCoveredEdgePairs()) {
                if (!coveredEdgePairs.contains(edgePair)) {
                    coveredEdgePairs.add(edgePair);
                }
            }
        }

        int coveredNodes = coveredStates.size();
        int coveredEdges = coveredTransitions.size();
        int numOfCoveredEP = coveredEdgePairs.size();

        StringBuilder builder = new StringBuilder();
        int numOfStates = staticProg.getStates().size();
        builder.append("Node Coverage (NC): ").append(coveredNodes * 100 / numOfStates).append("% (")
                .append(coveredNodes).append("/").append(numOfStates).append(")\n");
        int numOfEdges = staticProg.getTransitions().size();
        builder.append("Edge Coverage (EC): ").append(coveredEdges * 100 / numOfEdges).append("% (")
                .append(coveredEdges).append("/").append(numOfEdges).append(")\n");
        int numOfEdgePairs = staticProg.getEdgePairs().size();
        builder.append("Edge-paired Coverage (EPC): ").append(numOfCoveredEP * 100 / numOfEdgePairs).append("% (")
                .append(numOfCoveredEP).append("/").append(numOfEdgePairs).append(")\n");

        int deadEdges = staticProg.getDeadTransitionsMap().size();
        int deadEdgePairs = staticProg.getDeadEdgePairs().size();
        builder.append("Dead Edges: ").append(deadEdges).append(Arrays.toString(staticProg.getDeadTransitionsMap().keySet().toArray())).append("\n");
        builder.append("Dead Edge Pairs: ").append(deadEdgePairs).append(Arrays.toString(staticProg.getDeadEdgePairs().toArray())).append("\n");
        builder.append("Feasible Edge Coverage (FEC): ").append(coveredEdges * 100 / (numOfEdges - deadEdges))
                .append("% (").append(coveredEdges).append("/").append(numOfEdges - deadEdges).append(")\n");

        builder.append("Feasible Edge-paired Coverage (FEPC): ").append(numOfCoveredEP * 100 / (numOfEdgePairs - deadEdgePairs))
                .append("% (").append(numOfCoveredEP).append("/").append(numOfEdgePairs - deadEdgePairs).append(")\n");
//        taCoverage.setText(builder.toString());
//        taCoverage.appendText(builder.toString());
//        taCoverage.appendText("\n\n");
        taCoverage.setText(builder.toString());

        System.out.println("coverage computing done.");

        staticProg.generateDotFile(target, coveredStates, coveredTransitions);
        showPreviewGraph(target + ".png");
    }

    public void representRegex() {
        if (regexNode == null) return;
        taRegexContent.setText(regexNode.getContent());
        // STEP 1: getDFA,
        // data saved in json file (dataFilePath)
        String getDFAFilePath = Configuration.GET_DFA_EXEC;
        String dataFilePath = regexNode.getDataFilePath();
        String targetGraphFilePrefix = regexNode.getTargetGraphFilePrefix();

        try {
            // run command and export data into json data file
            Process process = Runtime.getRuntime().exec(new String[]{
                    getDFAFilePath, regexNode.getContent(), dataFilePath
            });

            BufferedReader stdInput = new BufferedReader(new
                    InputStreamReader(process.getInputStream()));
            // Read the output from the command
            System.out.println("Running command to get the DFA...");
            String s;
            while ((s = stdInput.readLine()) != null) {
//                System.out.println(s);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        boolean isTheDataFileExisting = new File(dataFilePath).exists();
        System.out.println("getting DFA done? " + isTheDataFileExisting);

        if (isTheDataFileExisting) {
            // STEP 2: generate graph
            Task<Object> task = new Task<Object>() {
                @Override
                protected Object call() {
                    Prog prog = new Prog(regexNode);
                    prog.loadFromJson(dataFilePath, targetGraphFilePrefix, true);

                    if (!MasterController.getRegexNodeToProgMap().containsKey(regexNode)) {
                        MasterController.getRegexNodeToProgMap().put(regexNode, prog);
                    }

                    boolean isGenGraphSuccessfully = new File(targetGraphFilePrefix).exists();
                    System.out.println("gen graph done? " + isGenGraphSuccessfully);

                    StringBuilder builder = new StringBuilder();
                    int numOfStates = prog.getStates().size();
                    builder.append("Node Coverage (NC): ").append(0).append("% (")
                            .append(0).append("/").append(numOfStates).append(")\n");
                    int numOfEdges = prog.getTransitions().size();
                    builder.append("Edge Coverage (EC): ").append(0).append("% (")
                            .append(0).append("/").append(numOfEdges).append(")\n");
                    builder.append("Edge-paired Coverage (EPC): ");

                    int deadEdges = prog.getDeadTransitionsMap().size();
                    if (deadEdges > 0) {
                        builder.append("Dead Edges: ").append(deadEdges).append("\n");
                        builder.append("Feasible Edge Coverage (FEC): ").append(0)
                                .append("% (").append(0).append("/").append(numOfEdges - deadEdges).append(")\n");
                        builder.append("Feasible Edge-paired Coverage (FEPC): \n");
                    }

                    taCoverage.setText(builder.toString());
                    return null;
                }
            };

            task.setOnSucceeded(event -> {
                // represent on app view
                String target = targetGraphFilePrefix + ".png";
                showPreviewGraph(target);
            });

            new Thread(task).start();
        } else {
            System.out.println("The DFA data file is not existing.");
        }
    }

    private void showPreviewGraph(String targetGraphFile) {
        // represent on main view
        Image source = null;
        try {
            source = new Image(new FileInputStream(targetGraphFile));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        assert source != null;

        ImageView image = new ImageView(source);

        double view_height = spPreviewGraph.getHeight() - 10; // -10 for hide the no need scroolbar
        double view_width = spPreviewGraph.getWidth() - 10;

        double source_width = source.getWidth();
        double source_height = source.getHeight();
        double ratio = source_width / source_height;

        double tmp_width;
        double tmp_height;
        if (view_height * ratio >= view_width) { // should fit the width
            tmp_width = view_width;
            tmp_height = view_width / ratio;
        } else { // should fit the height
            tmp_width = view_height * ratio;
            tmp_height = view_height;
        }

        image.setFitHeight((int) tmp_height);
        image.setFitWidth((int) tmp_width);
        vbPreviewGraph.getChildren().clear();
        vbPreviewGraph.getChildren().add(image);
    }

    public void setRegexNode(RegexNode regexNode) {
        this.regexNode = regexNode;
    }

    public void setRegexTabViewPane(AnchorPane regexTabViewPane) {
        this.regexTabViewPane = regexTabViewPane;
    }

    @FXML
    public void viewGraph() {
        GraphViewController controller = GraphViewController.getInstance();
        assert controller != null;
        controller.setRegexNode(regexNode);
        controller.initView();
        controller.showWindow();
    }

    @FXML
    public void generateStrings() {
        taCoverage.clear();
//        MasterController.getRegexNodeToProgMap().get(regexNode).generateStrings();
        Prog prog = MasterController.getRegexNodeToProgMap().get(regexNode);
        prog.generateStringsEPPool();
        for (String string : prog.getTmpPositiveStrings()) {
            StringInput stringInput = new StringInput(string);
            lvPositive.getItems().add(stringInput);
        }
        for (String string : prog.getTmpNegativeStrings()) {
            StringInput stringInput = new StringInput(string);
            lvNegative.getItems().add(stringInput);
        }
        syntheticAllDynamic();
        MasterController.getRegexNodeToProgMap().get(regexNode).getInputToDynamicProgMap().clear();
//        MasterController.getRegexNodeToProgMap().get(regexNode).generateStringsUsingEgret();
    }

    public void clearInputStrings() {
        lvNegative.getItems().clear();
        lvNegative.refresh();
        lvPositive.getItems().clear();
        lvPositive.refresh();
    }

    private State findNotSatisfiedState(List<State> coveredStates, List<Transition> coveredEdges) {
        for (State state : coveredStates) {
            for (Transition tran : state.getTransitions()) {
                if (!isDeadState(tran.getEndState()) && !coveredEdges.contains(tran)) {
                    return state;
                }
            }
        }

        return null;
    }

    public void generateStrings(Algorithm algorithm, Stack<String> stack, List<String> canBeRevisitOnceEdges) {
        // BASE algorithm
        List<String> positive = new ArrayList<>();
        List<String> negative = new ArrayList<>();

        List<String> positiveCoveredEdges = new ArrayList<>();
        List<String> negativeCoveredEdges = new ArrayList<>();

        Prog prog = MasterController.getRegexNodeToProgMap().get(regexNode);
        Map<String, State> nameToStateMap = new HashMap<>();
        for (State state : prog.getStates()) {
            nameToStateMap.put(state.getName(), state);
        }

        while (!stack.isEmpty()) {
            String currentPath = stack.pop();
            String currentStateName;
            if (currentPath.contains(".")) {
                currentStateName = currentPath.substring(currentPath.lastIndexOf(".") + 1);
            } else {
                currentStateName = currentPath; // when currentPath is "0"
            }
            int stateNum = -1;
            stateNum = Integer.parseInt(currentStateName);
            if (stateNum >= 0) {
                State curState = nameToStateMap.get(currentStateName);
                List<String> nextStates = new ArrayList<>();
                for (Transition trans : curState.getTransitions()) {
                    State nextState = trans.getEndState();
                    String nextName = nextState.getName();
                    String nextEdge = currentStateName + "." + nextName;

                    boolean isOk = false;
                    if (algorithm == Algorithm.BASIC && currentPath.indexOf(nextEdge) == currentPath.lastIndexOf(nextEdge)) {
                        if (canBeRevisitOnceEdges == null) {
                            isOk = true; // handle large loop accept for 0 or 1 time (some time we need that loop)
                        } else {
                            if (!currentPath.contains(nextEdge)) {
                                isOk = true;
                            } else if (canBeRevisitOnceEdges.contains(nextEdge)) {
                                isOk = true;
                            }
                        }
                    }
                    if (algorithm == Algorithm.OPTIMIZE && !currentPath.contains(nextEdge)) {
                        isOk = true; // no accept for revisit
                    }

                    if (isOk) { // handle large loop accept for 0 or 1 time (some time we need that loop)
//                    if (!currentPath.contains(nextEdge)) { // handle large loop accept for 0 or 1 time
                        if (currentPath.contains(nextEdge) && currentStateName.equals(nextName)) { // no accept for loop edge
                            continue;
                        }

                        if (prog.getFinalStates().contains(nextState)) {
                            List<String> additionalCoveredEdges = additionalCoveredEdges(positiveCoveredEdges,
                                    currentPath + "." + nextState.getName());
                            if (!additionalCoveredEdges.isEmpty()) {
                                positive.add(currentPath + "." + nextState.getName());
                                positiveCoveredEdges.addAll(additionalCoveredEdges);
                            }

                            nextStates.add(nextState.getName()); // to push to the map
                        } else if (isDeadState(nextState)) {
                            List<String> additionalCoveredEdges = additionalCoveredEdges(negativeCoveredEdges,
                                    currentPath + "." + nextState.getName());
                            if (!additionalCoveredEdges.isEmpty()) {
                                negative.add(currentPath + "." + nextState.getName());
                                negativeCoveredEdges.addAll(additionalCoveredEdges);
                            }

                            // no need to push to the map
                        } else {
                            nextStates.add(nextState.getName()); // to push to the map
                        }

                        Map<String, Character> map = prog.getEdgeToCharInputMap();
                        if (!map.containsKey(nextEdge)) {
                            char ch = prog.chooseCharInput(trans.getConditions());
                            map.put(nextEdge, ch);
                        }
                    }
                }

                // the path to "e" state (dead state) should be priority to handle latest. so push it first
                if (nextStates.contains("e")) {
                    stack.push(currentPath + ".e");
                    nextStates.remove("e");
                }

                for (String nextStateName : nextStates) {
                    stack.push(currentPath + "." + nextStateName);
//                    System.out.println(currentPath + "." + nextStateName);
                }

                // loop in the current state should be priority to handle first. so move it to the last stack
                for (String nextStateName : nextStates) {
                    if (nextStateName.equals(currentStateName)) {
                        stack.remove(currentPath + "." + nextStateName);
                        stack.push(currentPath + "." + nextStateName);
                        break;
                    }
                }
            }
        }

        List<String> paths = new ArrayList<>();
        paths.addAll(positive);
        paths.addAll(negative);
        List<String> inputs = new ArrayList<>();

        System.out.println();
        System.out.println("Found " + paths.size() + " paths.");
        System.out.println(positive.size() + " positive paths.");
        System.out.println(negative.size() + " negative paths.");
//        for (String path : paths) {
//            System.out.println(path);
//        }
        System.out.println();

        for (String path : paths) {
            String input = prog.convertPathToStringInput(path, true);
            if (input != null) {
                String realPath = computeInput(input, true);
                if (!realPath.equals(path)) {
                    System.out.println("WARNING: The expected and real path don't match.");
                    System.out.println("Real path: " + realPath);
                    System.out.println("Expected path: " + path);
                    System.out.println("input: " + input);
                }
                inputs.add(input);
            }
        }
        System.out.println("Generated inputs: " + inputs);

        // filter to get all truly dead edges
        prog.removeFakeDeadEdges();

        syntheticAllDynamic();
    }

    private void generateStringAlgorithms3() {

    }

    /**
     * find out additional covered edges.
     *
     * @param coveredEdges list of covered edges
     * @param path         path, e.g. 0.3.4.e
     * @return list of additional covered edges
     */
    private List<String> additionalCoveredEdges(List<String> coveredEdges, String path) {
        List<String> additionalEdges = new ArrayList<>();
        String[] stateNames = path.split("\\.");
        for (int i = 0; i < stateNames.length - 1; i++) {
            String nextEdge = stateNames[i] + "." + stateNames[i + 1];
            if (!coveredEdges.contains(nextEdge)) {
                additionalEdges.add(nextEdge);
            }
        }
        return additionalEdges;
    }

    public void showProcessedContent() {
        taRegexContent.setText(regexNode.getContent());
    }

    public void showRawContent() {
        taRegexContent.setText(regexNode.getRawContent());
    }

    private boolean isDeadState(State state) {
        return state.getName().equals("e");
    }

    public static enum Algorithm {
        BASIC, OPTIMIZE
    }
}