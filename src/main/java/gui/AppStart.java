package gui;

import gui.controllers.BaseSceneController;
import gui.controllers.MasterController;
import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class AppStart extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        BaseSceneController.prepare();
        Parent root = BaseSceneController.getBaseScenePane();

        primaryStage.setTitle("AREXT");
        primaryStage.setScene(new Scene(root));

        MasterController.setPrimaryStage(primaryStage);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
