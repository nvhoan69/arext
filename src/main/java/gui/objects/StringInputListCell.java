package gui.objects;

import javafx.scene.control.ListCell;

public class StringInputListCell extends ListCell<StringInput> {
    @Override
    protected void updateItem(StringInput s, boolean b) {
        super.updateItem(s, b);
        if (s == null) {
            setText(null);
        } else {
            setText(s.getRepresent());
        }
    }
}
