package gui.objects;

import gui.Configuration;
import gui.controllers.*;
import gui.graph.Prog;
import javafx.concurrent.Task;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.cell.TextFieldTreeCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import parser.object.*;

import java.util.Objects;

public class StructureTreeCellFactory extends TextFieldTreeCell<INode> {
    INode node = null;

    @Override
    public void updateItem(INode iNode, boolean b) {
        super.updateItem(iNode, b);

        if (getTreeItem() != null) {
            node = getItem();
            if (node instanceof CppFileNode) {
                setText(iNode.getName()
                        + " [" + iNode.getChildren().size() + "]");
                setGraphic(getIcon(node));
            } else {
                setText(node.getName());
            }
        }

        if (node != null) {
            // initialize popup
            setContextMenu(new ContextMenu());

            // add options to popup
            addViewSourceCode(node);
            addExtractRegexes(node);

            if (node instanceof RegexNode) {
                addViewRegex((RegexNode) node);
                addViewRegexGraph((RegexNode) node);
                addGenerateStrings((RegexNode) node);
                addGenerateStringsUsingEgret((RegexNode) node);
            } else if (node instanceof CppFileNode) {
                addDefineMacro((CppFileNode) node);
            }
        } else {
            setContextMenu(null);
        }
    }

    private void addGenerateStrings(RegexNode regexNode) {
        Menu menuGenStrings = new Menu("Generate Strings");

        for (Configuration.Gen_Algorithm alg : Configuration.Gen_Algorithm.values()) {
            MenuItem item = new MenuItem("Alg. " + alg.name());
            item.setOnAction(event -> {
                RegexTabViewController controller = MVIWindowController.getInstance()
                        .getRegexToViewControllerMap().get(regexNode);
                Prog prog = MasterController.getRegexNodeToProgMap().get(regexNode);

                prog.getInputToDynamicProgMap().clear();
                switch (alg) {
                    case NODES_POOL:
                        prog.generateStringsNodesPool();
                        break;
                    case EDGES_POOL:
                        prog.generateStringsEdgesPool();
                        break;
                    case EDGE_PAIRS_POOL:
                        prog.generateStringsEPPool();
                        break;
                    case EGRET:
                        prog.generateStringsUsingEgret();
                        break;
                    case MUTREX:
                        prog.generateStringsUsingMutRex();
                        break;
                    default:
                        break;
                }

                controller.syntheticAllDynamic();
            });

            menuGenStrings.getItems().add(item);
        }

        getContextMenu().getItems().add(menuGenStrings);
    }

    private void addGenerateStringsUsingEgret(RegexNode regexNode) {
        MenuItem menuItem = new MenuItem("Generate Strings Using Egret");
        menuItem.setOnAction(actionEvent -> {
            Prog prog = MasterController.getRegexNodeToProgMap().get(regexNode);
            prog.generateStringsUsingEgret();
        });

        getContextMenu().getItems().add(menuItem);
    }

    private void addDefineMacro(CppFileNode cppFileNode) {
        MenuItem menuItem = new MenuItem("Define Macro");
        menuItem.setOnAction(actionEvent -> {
            DefineMacroWindowController controller = DefineMacroWindowController.getInstance(cppFileNode);
            if (controller != null) {
                controller.showWindowAndWait();
            }
        });

        getContextMenu().getItems().add(menuItem);

    }

    private void addViewRegex(RegexNode regexNode) {
        MenuItem menuItem = new MenuItem("View Regex");
        menuItem.setOnAction(actionEvent -> MVIWindowController
                .getInstance().viewRegex(regexNode));

        getContextMenu().getItems().add(menuItem);
    }

    private void addViewRegexGraph(RegexNode regexNode) {
        MenuItem menuItem = new MenuItem("View Regex Graph On New Window");
        menuItem.setOnAction(actionEvent -> {
            MVIWindowController.getInstance().viewRegex(regexNode);

            GraphViewController controller = GraphViewController.getInstance();
            assert controller != null;
            controller.setRegexNode(regexNode);
            controller.initView();
            controller.showWindow();
        });

        getContextMenu().getItems().add(menuItem);
    }

    private ImageView getIcon(INode node) {
        Image image = new Image(Objects.requireNonNull(getClass().getResourceAsStream("/icons/file.png")));
        if (node instanceof FolderNode || node instanceof ProjectNode) {
            image = new Image(Objects.requireNonNull(getClass().getResourceAsStream("/icons/directory.png")));
        } else if (node instanceof CppFileNode) {
            image = new Image(Objects.requireNonNull(getClass().getResourceAsStream("/icons/cppfile.png")));
        } else if (node instanceof RegexNode) {
            image = new Image(Objects.requireNonNull(getClass().getResourceAsStream("/icons/regex.png")));
        } else if (node instanceof UnknowObjectNode) {
            image = new Image(Objects.requireNonNull(getClass().getResourceAsStream("/icons/file.png")));
        }

        return new ImageView(image);
    }

    private void addViewSourceCode(INode node) {
        if (node instanceof CppFileNode) {
            MenuItem menuItem = new MenuItem("View Source Code");
            menuItem.setOnAction(actionEvent -> {
                MVIWindowController.getInstance().viewSourceCode((CppFileNode) node);
            });

            getContextMenu().getItems().add(menuItem);
        }
    }

    private void addExtractRegexes(INode node) {
        if (node instanceof ProjectNode
                || node instanceof FolderNode
                || node instanceof CppFileNode) {
            MenuItem menuItem = new MenuItem("Extract Regular Expressions");
            menuItem.setOnAction(event -> {
                LoadingPopupController controller = LoadingPopupController.getInstance();
                if (controller != null) {
                    controller.showDialog(MasterController.getPrimaryStage());

                    Task task = new Task() {
                        @Override
                        protected Object call() throws Exception {
                            extractRegex(node);
                            return null;
                        }
                    };
                    task.setOnSucceeded(e -> controller.getLoadingStage().close());

                    new Thread(task).start();
                }
            });

            getContextMenu().getItems().add(menuItem);
        }
    }

    private void extractRegex(INode node) {
        if (node instanceof ProjectNode)
            extractRegexesInProject((ProjectNode) node);
        if (node instanceof FolderNode)
            extractRegexesInAFolder((FolderNode) node);
        if (node instanceof CppFileNode)
            extractRegexesInAFile((CppFileNode) node);
    }

    private void extractRegexesInAFile(CppFileNode cppFileNode) {
        MasterController.extractRegexesInAFile(cppFileNode);
    }

    private void extractRegexesInAFolder(FolderNode folderNode) {
        for (Node childNode : folderNode.getChildren()) {
            if (childNode instanceof CppFileNode) {
                extractRegexesInAFile((CppFileNode) childNode);
            }
        }
    }

    private void extractRegexesInProject(ProjectNode projectNode) {
        for (Node childNode : projectNode.getChildren()) {
            if (childNode instanceof FolderNode) {
                extractRegexesInAFolder((FolderNode) childNode);
            } else if (childNode instanceof CppFileNode) {
                extractRegexesInAFile((CppFileNode) childNode);
            }
        }
    }
}
