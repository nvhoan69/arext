package gui.objects;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Interval {
    private int min;
    private int max;

    public Interval(int min, int max) {
        this.min = min;
        this.max = max;
    }

    public Interval(Interval interval) {
        this.min = interval.min;
        this.max = interval.max;
    }

    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        int[] set = new int[] {1, 2, 5, 4, 6, 15, 9, 10, 0, 12};
        for (int i : set) {
            list.add(i);
        }
        List<Interval> intervals1 = convertListToIntervals(list);
        System.out.println(printIntervals(intervals1));

        list.clear();
        set = new int[] {3, 16, 17, 8};
        for (int i : set) {
            list.add(i);
        }
        List<Interval> intervals2 = convertListToIntervals(list);
        System.out.println(printIntervals(intervals2));

        List<Interval> allInterval = new ArrayList<>(intervals1);
        allInterval.addAll(intervals2);
        System.out.println(printIntervals(allInterval));

        System.out.println(printIntervals(optimizeIntervals(allInterval)));
    }

    public static List<Interval> convertListToIntervals(List<Integer> list) {
        List<Interval> intervals = new ArrayList<>();

        if (list == null || list.isEmpty()) {
            return intervals;
        }

        Collections.sort(list);
        Interval current = new Interval(list.get(0), list.get(0));
        for (int j = 1; j < list.size(); j++) {
            if (list.get(j) == current.getMax() + 1) {
                current.setMax(list.get(j));
            } else {
                intervals.add(current);
                current = new Interval(list.get(j), list.get(j));
            }
        }
        intervals.add(current);

        return intervals;
    }

    public static List<Interval> optimizeIntervals(List<Interval> intervals) {
        List<Interval> optIntervals = new ArrayList<>();

        if (intervals == null || intervals.isEmpty()) {
            return optIntervals;
        }

        intervals.sort((o1, o2) -> {
            if (o1.min != o2.min) {
                return o1.min - o2.min;
            } else {
                return o1.max - o2.max;
            }
        });

//        System.out.println(printIntervals(intervals));

        Interval current = new Interval(intervals.get(0));
        for (int i = 1; i < intervals.size(); i++) {
            Interval next = intervals.get(i);
            if (next.min == current.max + 1) {
                current.setMax(next.max);
            } else {
                optIntervals.add(current);
                current = new Interval(next);
            }
        }
        optIntervals.add(current);

        return optIntervals;
    }

    public static String printIntervals(List<Interval> intervals) {
        List<String> intervalStrings = new ArrayList<>();
        for (Interval interval : intervals) {
            intervalStrings.add(interval.toString());
        }
        return String.join(", ", intervalStrings);
    }

    public void setMax(int max) {
        this.max = max;
    }

    public int getMax() {
        return max;
    }

    public int getMin() {
        return min;
    }

    @Override
    public String toString() {
        if (min == max) return max + "";
        return min + "-" + max;
    }
}
