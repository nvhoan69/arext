package gui.objects;

import javafx.scene.control.TreeItem;
import parser.object.INode;

public class StructureTreeItem extends TreeItem<INode> {
    public StructureTreeItem(INode value) {
        super(value);
    }
}
