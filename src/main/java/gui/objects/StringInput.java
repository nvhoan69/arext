package gui.objects;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class StringInput {
    List<Integer> utf8ByteIntValue = new ArrayList<>();
    String stringContent = "";
    State state = State.STRING_VALUE;

    public StringInput(List<Integer> utf8ByteIntValue) {
        this.utf8ByteIntValue = new ArrayList<>(utf8ByteIntValue);
    }

    public StringInput(byte[] utf8ByteValue) {
        for (byte b : utf8ByteValue) {
            this.utf8ByteIntValue.add((int) b);
        }
    }

    public StringInput(String stringContent) {
        this.stringContent = stringContent;
        utf8ByteIntValue = convertToListOfBytes(stringContent);
        if (utf8ByteIntValue.contains(240)
                || utf8ByteIntValue.contains(241)
                || utf8ByteIntValue.contains(242)
                || utf8ByteIntValue.contains(243)
                || utf8ByteIntValue.contains(244)) { // extra utf32 unicode that not supported by javafx
            setState(StringInput.State.UTF_8_BYTE_VALUES);
        }
    }

    private List<Integer> convertToListOfBytes(String stringContent) {
        byte[] utf_8 = stringContent.getBytes(StandardCharsets.UTF_8);
        List<Integer> result = new ArrayList<>();
        for (byte b : utf_8) {
            if ((int) b > 0)
                result.add((int) b);
            else
                result.add(256 + (int) b);
        }
        return result;
    }


    public String getRepresent() {
        switch (state) {
            case STRING_VALUE:
                return "\"" + stringContent + "\"";
            case UTF_8_BYTE_VALUES:
                return utf8ByteIntValue.toString();
            default:
                return null;
        }
    }

    public List<Integer> getUtf8ByteIntValue() {
        return utf8ByteIntValue;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    //    private static String convertToStringContent(List<Integer> utf8ByteIntValue) {
//        String stringContent = "";
//        return stringContent;
//    }

    public enum State {
        STRING_VALUE, UTF_8_BYTE_VALUES
    }
}
