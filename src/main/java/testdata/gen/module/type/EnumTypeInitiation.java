package testdata.gen.module.type;

import parser.object.ExternalVariableNode;
import parser.object.VariableNode;
import testdata.object.DataNode;
import testdata.object.EnumDataNode;
import testdata.object.ValueDataNode;

/**
 * Khoi tao bien dau vao la kieu Enum
 */
public class EnumTypeInitiation extends AbstractTypeInitiation {
    public EnumTypeInitiation(VariableNode vParent, DataNode nParent) throws Exception {
        super(vParent, nParent);
    }

    @Override
    public ValueDataNode execute() throws Exception {
        EnumDataNode child = new EnumDataNode();

        child.setParent(nParent);
        child.setName(vParent.getNewType());
        child.setRawType(vParent.getFullType());
        child.setRealType(vParent.getRealType());
        child.setCorrespondingVar(vParent);
        if (vParent instanceof ExternalVariableNode)
            child.setExternel(true);
        nParent.addChild(child);
        return  child;
    }
}
