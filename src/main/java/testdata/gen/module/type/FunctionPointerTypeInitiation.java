package testdata.gen.module.type;

import parser.object.ExternalVariableNode;
import parser.object.VariableNode;
import testdata.object.DataNode;
import testdata.object.FunctionPointerDataNode;
import testdata.object.ValueDataNode;
import util.VFPLogger;

public class FunctionPointerTypeInitiation extends AbstractTypeInitiation {
    final static VFPLogger logger = VFPLogger.get(FunctionPointerTypeInitiation.class);

    public FunctionPointerTypeInitiation(VariableNode vParent, DataNode nParent) throws Exception {
        super(vParent, nParent);
    }

    @Override
    public ValueDataNode execute() throws Exception {
        FunctionPointerDataNode child = new FunctionPointerDataNode();

        child.setParent(nParent);
        child.setRawType(vParent.getRawType());
        child.setRealType(vParent.getRealType());
        child.setName(vParent.getNewType());
        child.setCorrespondingVar(vParent);

        if (vParent instanceof ExternalVariableNode)
            child.setExternel(true);

        nParent.addChild(child);

        return child;
    }


}
