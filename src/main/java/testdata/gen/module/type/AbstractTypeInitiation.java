package testdata.gen.module.type;

import parser.object.VariableNode;
import testdata.object.DataNode;
import testdata.object.ValueDataNode;

public abstract class AbstractTypeInitiation implements ITypeInitiation {
    protected VariableNode vParent;
    protected DataNode nParent;

//    public AbstractTypeInitiation() {
//
//    }

    public AbstractTypeInitiation(VariableNode vParent, DataNode nParent) throws Exception {
        this.vParent = vParent;
        this.nParent = nParent;
//        execute();
    }

    @Override
    public ValueDataNode execute() throws Exception {
        return null;

    }


}
