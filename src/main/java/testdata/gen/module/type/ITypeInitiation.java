package testdata.gen.module.type;

import testdata.object.ValueDataNode;

public interface ITypeInitiation {
    ValueDataNode execute() throws Exception;
}
