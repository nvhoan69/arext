package testdata.gen.module;

import parser.funcdetail.IFunctionDetailTree;
import parser.object.ICommonFunctionNode;
import parser.object.INode;
import testdata.IDataTree;
import testdata.gen.module.subtree.InitialUUTBranchGen;
import testdata.object.RootDataNode;
import util.Utils;
import util.VFPLogger;

import static util.NodeType.STUB;

/**
 * dựng cây Function Detail Tree của một test case (có UUT, STUB, GLOBAL)
 *
 *
 */
public class DataTreeGeneration extends AbstractDataTreeGeneration {
    final static VFPLogger logger = VFPLogger.get(DataTreeGeneration.class);

    private IFunctionDetailTree functionTree;
    private IDataTree dataTree;

    public DataTreeGeneration() {
    }

    public DataTreeGeneration(IDataTree dataTree, IFunctionDetailTree functionTree)  {
        this.dataTree = dataTree;
        setRoot(dataTree.getRoot());
        setFunctionNode(functionTree.getUUT());
        this.functionTree = functionTree;
    }

    @Override
    public void generateTree() throws Exception {
        root.setFunctionNode(functionNode);
        INode sourceCode = Utils.getSourcecodeFile(functionNode);

        // generate uut branch
        new InitialUUTBranchGen().generateCompleteTree(root, functionTree);

        // generate stub branch
        RootDataNode stubRoot = new RootDataNode(STUB);
        root.addChild(stubRoot);
        stubRoot.setParent(root);
    }

    @Override
    public void setFunctionNode(ICommonFunctionNode functionNode) {
        this.functionNode = functionNode;

        if (dataTree != null)
            dataTree.setFunctionNode(functionNode);
    }
}
