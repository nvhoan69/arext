package testdata.gen.module.subtree;

import parser.object.ICommonFunctionNode;
import parser.object.VariableNode;
import testdata.gen.module.InitialTreeGen;
import testdata.object.DataNode;
import testdata.object.IDataNode;
import testdata.object.ValueDataNode;
import util.VFPLogger;

public abstract class AbstractInitialTreeGen implements IInitialSubTreeGen {
    protected final static VFPLogger logger = VFPLogger.get(AbstractInitialTreeGen.class);

    protected ICommonFunctionNode functionNode;
    protected IDataNode root;

    @Override
    public ValueDataNode genInitialTree(VariableNode vCurrentChild, DataNode nCurrentParent) throws Exception {
        return new InitialTreeGen().genInitialTree(vCurrentChild, nCurrentParent);
    }

    public ICommonFunctionNode getFunctionNode() {
        return functionNode;
    }

    public void setFunctionNode(ICommonFunctionNode functionNode) {
        this.functionNode = functionNode;
    }

    public IDataNode getRoot() {
        return root;
    }

    public void setRoot(IDataNode root) {
        this.root = root;
    }
}
