package testdata.object;

import util.SpecialCharacter;
import util.TemplateUtils;
import util.VariableTypeUtils;

public class OneDimensionPointerDataNode extends OneDimensionDataNode {

    @Override
    public String getInputForGoogleTest() throws Exception {
        if (isUseUserCode()) {
            return getUserCodeContent();
        }

        String declaration = "";

        // get type
        String type = VariableTypeUtils
                .deleteStorageClassesExceptConst(this.getRawType().replace(IDataNode.REFERENCE_OPERATOR, ""));

        String coreType = type.replaceAll("\\[.*\\]", "");

        if (TemplateUtils.isTemplate(type))
            if (!getChildren().isEmpty()) {
                IDataNode first = getChildren().get(0);
                if (first instanceof ValueDataNode)
                    coreType = ((ValueDataNode) first).getRawType();
            }

        if (isExternel()) {
            coreType = "";
        }

        // get indexes
//        List<String> indexes = Utils.getIndexOfArray(TemplateUtils.deleteTemplateParameters(type));
//        if (indexes.size() > 0) {
        String dimension = "[" + getSize() + "]";

        // generate declaration
        if (this.isAttribute()) {
            declaration += "";
        } else if (this.isPassingVariable()) {
            declaration += String.format("%s %s%s" + SpecialCharacter.END_OF_STATEMENT, coreType,
                    this.getVituralName(), dimension);
        } else if (isSutExpectedArgument() || isGlobalExpectedValue()) {
            if (!dimension.equals("[-1]")) {
                declaration += String.format("%s %s%s" + SpecialCharacter.END_OF_STATEMENT, coreType,
                        this.getVituralName(), dimension);
            }
        } else if (isVoidPointerValue()) {
            declaration += String.format("%s %s%s" + SpecialCharacter.END_OF_STATEMENT, coreType,
                    this.getVituralName(), dimension);
        }
//        }

        return declaration + SpecialCharacter.LINE_BREAK + super.getInputForGoogleTest();
    }
}
