package testdata.object;

import parser.object.FunctionPointerTypeNode;
import parser.object.ICommonFunctionNode;
import parser.object.INode;
import search.Search;
import util.IRegex;
import util.SpecialCharacter;
import util.VariableTypeUtils;

import java.util.List;

public class FunctionPointerDataNode extends OtherUnresolvedDataNode {

    public static final String DEFAULT = "<<DEFAULT>>";

    private boolean isUseDefault = false;

    private List<ICommonFunctionNode> possibleFunctions;

    private ICommonFunctionNode selectedFunction;

    public List<ICommonFunctionNode> getPossibleFunctions() {
        FunctionPointerTypeNode typeNode = getCorrespondingType();
        if (possibleFunctions == null && typeNode != null) {
            possibleFunctions = Search.searchAllMatchFunctions(typeNode);
        }

        return possibleFunctions;
    }

    public void useDefault() {
        this.isUseDefault = true;
    }

    public void unUseDefault() {
        this.isUseDefault = false;
    }

    public boolean isUseDefault() {
        return isUseDefault;
    }

    @Override
    public boolean haveValue() {
        return selectedFunction != null;
    }

    public void setSelectedFunction(ICommonFunctionNode selectedFunction) {
        this.selectedFunction = selectedFunction;
        unUseDefault();
    }

    public INode getSelectedFunction() {
        return selectedFunction;
    }

    @Override
    public String getDisplayNameInParameterTree() {
        if (name.isEmpty()) {
            FunctionPointerTypeNode typeNode = getCorrespondingType();

            if (typeNode != null) {
                return typeNode.getFunctionName();
            }
        }

        return super.getDisplayNameInParameterTree();
    }

    @Override
    public String getInputForGoogleTest() {
        if (isUseUserCode()) {
            return getUserCodeContent();
        }

        String input = SpecialCharacter.EMPTY;

        String typeVar = getRawType();

        if (isExternel())
            typeVar = "";

        String valueVar = null;

        if (isUseDefault) {
            valueVar = getCorrespondingType().generateDefault();
        } else if (selectedFunction != null) {
            valueVar = selectedFunction.getSimpleName();
        }

        String name = getVituralName();
        if (VariableTypeUtils.isFunctionPointer(typeVar)) {
            typeVar = typeVar.replaceFirst("\\(\\s*\\*\\s*" + IRegex.NAME_REGEX + "\\s*\\)", "(*" + name + ")");
            name = SpecialCharacter.EMPTY;
        }

        if (valueVar != null) {
            valueVar = String.format("&%s;", valueVar);

            if (this.isPassingVariable()) {
                input += typeVar + " " + name + "=" + valueVar + SpecialCharacter.END_OF_STATEMENT;

            } else if (this.isAttribute()) {
                input += this.getVituralName() + "=" + valueVar + SpecialCharacter.END_OF_STATEMENT;

            } else if (this.isArrayElement()) {
                input += this.getVituralName() + "=" + valueVar + SpecialCharacter.END_OF_STATEMENT;

            } else if (isSTLListBaseElement()) {
                input += typeVar + " " + name + "=" + valueVar + SpecialCharacter.END_OF_STATEMENT;

            } else if (this.isInConstructor()){
                input += typeVar + " " + name + "=" + valueVar + SpecialCharacter.END_OF_STATEMENT;

            } else {
                input += typeVar + " " + name + "=" + valueVar + SpecialCharacter.END_OF_STATEMENT;
            }
        } else if (isPassingVariable()) {
            input += typeVar + " " + name + SpecialCharacter.END_OF_STATEMENT;
        }

        return input;
    }

    @Override
    public FunctionPointerTypeNode getCorrespondingType() {
        INode typeNode = super.getCorrespondingType();
        if (typeNode instanceof FunctionPointerTypeNode)
            return (FunctionPointerTypeNode) typeNode;
        return null;
    }
}
