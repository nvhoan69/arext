package testdata;

import javafx.scene.control.TreeTableCell;
import testdata.object.DataNode;
import testdata.object.ValueDataNode;

public interface IInputCellHandler {
    void update(TreeTableCell<DataNode, String> cell, DataNode dataNode);

    void commitEdit(ValueDataNode dataNode, String newValue) throws Exception;
}
