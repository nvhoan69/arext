package project_init;

import auto_testcase_generation.instrument.AbstractFunctionInstrumentation;
import auto_testcase_generation.instrument.FunctionInstrumentationForAllCoverages;
import environment.Environment;
import org.eclipse.cdt.core.dom.ast.*;
import parser.dependency.IncludeHeaderDependency;
import parser.dependency.IncludeHeaderDependencyGeneration;
import parser.object.*;
import search.Search;
import search.SearchCondition;
import search.condition.AbstractFunctionNodeCondition;
import search.condition.GlobalVariableNodeCondition;
import search.condition.IncludeHeaderNodeCondition;
import search.condition.SourcecodeFileNodeCondition;
import testcase_execution.DriverConstant;
import util.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

public class ProjectClone {

    public static final String CLONED_FILE_EXTENSION = ".VFPignore";
    public static final String MAIN_REFACTOR_NAME = "VFP_MAIN(";
    public static final String MAIN_REGEX = "\\bmain\\b\\(";
    private static final VFPLogger logger = VFPLogger.get(ProjectClone.class);
    protected final Map<String, String> refactors = new HashMap<>();
    List<String> globalDeclarations = new ArrayList<>();

    /**
     * Clone all source code file with extension in project directories.
     */
    public static void cloneEnvironment() {
        ProjectNode projectRoot = Environment.getInstance().getProjectNode();
        List<ISourcecodeFileNode> sources = Search.searchNodes(projectRoot, new SourcecodeFileNodeCondition());

        for (ISourcecodeFileNode sourceCode : sources) {
            try {
                String cloneFilePath = getClonedFilePath(sourceCode.getAbsolutePath());
                File cloneFile = new File(cloneFilePath);

                if (!cloneFile.exists()) {
                    Utils.copy(sourceCode.getFile(), cloneFile);
                    ProjectClone clone = new ProjectClone();
                    String newContent = clone.generateFileContent(sourceCode);
                    logger.debug("Generate instrument file of " + sourceCode.getName() + " successfully");
                    Utils.writeContentToFile(newContent, cloneFilePath);
                }
            } catch (InterruptedException | IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Ex: Utils.cpp -> Utils.VFPignore.cpp
     *
     * @param origin file path
     * @return cloned file path
     */
    public static String getClonedFilePath(String origin) {
        String originName = new File(origin).getName();

        int lastDotPos = originName.lastIndexOf(SpecialCharacter.DOT);

        String clonedName = originName.substring(0, lastDotPos) + CLONED_FILE_EXTENSION + originName.substring(lastDotPos);

        return origin.replace(originName, clonedName);
    }

    public static String getOriginFilePath(String clone) {
        return clone.replace(CLONED_FILE_EXTENSION, SpecialCharacter.EMPTY);
    }

    /**
     * Ex: int x;
     * To #ifdef VFP_GLOBAL_X
     * #define VFP_GLOBAL_X
     * int x;
     * #endif
     *
     * @param name    define name
     * @param content needed to be guard
     * @return new source code
     */
    public static String wrapInIncludeGuard(String name, String content) {
        return String.format("/** Guard statement to avoid multiple declaration */\n" +
                "#ifndef %s\n#define %s\n%s\n#endif\n", name, name, content);
    }

    public static String generateCallingMark(String content) {
        content = PathUtils.toRelative(content);
        content = Utils.doubleNormalizePath(content);
        return String.format(DriverConstant.MARK + "(\"Calling: %s\");" + SourceConstant.INCREASE_FCALLS, content);
    }

    /**
     * Generate clone file content
     *
     * @param sourceCode origin source code file content
     * @return cloned file content
     */
    protected String generateFileContent(INode sourceCode) throws InterruptedException {
        String oldContent = Utils.readFileContent(sourceCode.getAbsolutePath());

        List<SearchCondition> conditions = new ArrayList<>();
        conditions.add(new IncludeHeaderNodeCondition());
        conditions.add(new GlobalVariableNodeCondition());
        conditions.add(new AbstractFunctionNodeCondition());

        List<INode> redefines = Search.searchNodes(sourceCode, conditions);
        redefines.removeIf(this::isIgnore);

        int size = redefines.size();

        ExecutorService es = Executors.newFixedThreadPool(5);
        AtomicInteger counter = new AtomicInteger();
        List<Callable<Void>> tasks = new ArrayList<>();
        for (INode child : redefines) {
            Callable<Void> c = () -> {
                logger.debug(String.format("[%d/%d] Clone & instrument %s", counter.incrementAndGet(), size, child.getName()));

                if (child instanceof IncludeHeaderNode) {
                    refactorInclude((IncludeHeaderNode) child, sourceCode);

                } else if (child instanceof ExternalVariableNode)
                    guardGlobalDeclaration(child);

                else if (child instanceof ICommonFunctionNode) {
                    ICommonFunctionNode function = (ICommonFunctionNode) child;
                    refactorFunction(function);
                }

                return null;
            };
            tasks.add(c);
        }

        es.invokeAll(tasks);

        for (Map.Entry<String, String> entry : refactors.entrySet()) {
            String prev = entry.getKey();
            String newC = entry.getValue();
            logger.debug("New content: " + newC);
            oldContent = oldContent.replace(prev, newC);
        }

        for (String globalDeclaration : globalDeclarations)
            oldContent = oldContent.replace("#endif\n\n" + globalDeclaration, "#endif");

        oldContent = oldContent
                .replaceAll("\\bconstexpr\\b", SpecialCharacter.EMPTY);

        String defineSourceCodeName = SourceConstant.SRC_PREFIX + sourceCode.getAbsolutePath().toUpperCase()
                .replaceAll("[^\\w]", SpecialCharacter.UNDERSCORE);

        return wrapInIncludeGuard(defineSourceCodeName, oldContent);
    }

    private boolean isIgnore(INode node) {
        if (node instanceof ICommonFunctionNode && node.getParent() instanceof ICommonFunctionNode)
            return true;

        if (node instanceof AbstractFunctionNode) {
            IASTStatement body = ((AbstractFunctionNode) node).getAST().getBody();
            return body == null;
        }

        return false;
    }

    protected String getCorrespondingClonePath(String path) {
        return getClonedFilePath(path);
    }

    /**
     * Refactor include to clone VFPignore file and stub libraries source files.
     *
     * @param includeHeader node
     * @param sourceCode    node
     */
    private void refactorInclude(IncludeHeaderNode includeHeader, INode sourceCode) {

        // Guard all include statement in source code
        String guardIncludeStm = guardIncludeHeader(includeHeader);

        // find header node from include header statement
        List<INode> headerNodes = new IncludeHeaderDependencyGeneration().findIncludeNodes(includeHeader, sourceCode);

        // get prev include statement
        String oldIncludeStatement = includeHeader.getAST().getRawSignature();

        // Modify include dependency to clone VFPignore file
        if (!headerNodes.isEmpty()) {
            INode headerNode = headerNodes.get(0);
            IncludeHeaderDependency d = new IncludeHeaderDependency(sourceCode, headerNode);
            if (sourceCode.getDependencies().contains(d)) {
                String clonedFilePath = getCorrespondingClonePath(headerNode.getAbsolutePath());
                Path parentDirPath = Paths.get(sourceCode.getAbsolutePath()).getParent();
                Path clonedPath = Paths.get(clonedFilePath);
                Path relativePath = parentDirPath.relativize(clonedPath);
                String newIncludeStatement = oldIncludeStatement.replace(includeHeader.getNewType(), relativePath.toString());
                guardIncludeStm = guardIncludeStm.replace(oldIncludeStatement, newIncludeStatement);
            }
        }

        refactors.put(oldIncludeStatement, guardIncludeStm);
    }

    /**
     * Refactor function content
     *
     * @param function node
     */
    protected void refactorFunction(ICommonFunctionNode function) {
        if (function instanceof AbstractFunctionNode) {
            String oldFunctionCode;

            IASTFunctionDefinition functionAST = ((AbstractFunctionNode) function).getAST();
            oldFunctionCode = functionAST.getRawSignature();

            // generate instrumented function content
            String newFunctionCode;
            newFunctionCode = generateInstrumentedFunction((IFunctionNode) function);

            // change constexpr function to normal function
            newFunctionCode = newFunctionCode.replaceFirst("constexpr\\s+", " ");

            refactors.put(oldFunctionCode, newFunctionCode);
        }
    }

    /**
     * Ex: #include "class.h"
     * To #ifdef VFP_INCLUDE_CLASS_H
     * #define VFP_INCLUDE_CLASS_H
     * #include "class.h"
     * #endif
     *
     * @param child corresponding external variable node
     * @return new guarded include statement
     */
    private String guardIncludeHeader(INode child) {
        if (child instanceof IncludeHeaderNode) {
            String oldIncludeHeader = ((IncludeHeaderNode) child).getAST().getRawSignature();

            String header = child.getName().replaceAll("[^\\w]", SpecialCharacter.UNDERSCORE).toUpperCase();

            return wrapInIncludeGuard(SourceConstant.INCLUDE_PREFIX + header, oldIncludeHeader);
        }

        return null;
    }

    /**
     * Ex: int x;
     * To #ifdef VFP_GLOBAL_X
     * #define VFP_GLOBAL_X
     * int x;
     * #endif
     *
     * @param child corresponding external variable node
     */
    private void guardGlobalDeclaration(INode child) {
        IASTNodeLocation[] tempAstLocations = ((ExternalVariableNode) child).getASTType().getNodeLocations();
        if (tempAstLocations.length > 0) {
            IASTNodeLocation astNodeLocation = tempAstLocations[0];
            if (astNodeLocation instanceof IASTCopyLocation) {
                IASTNode declaration = ((IASTCopyLocation) astNodeLocation).getOriginalNode().getParent();
                if (declaration instanceof IASTDeclaration) {
                    String originDeclaration = declaration.getRawSignature();

                    if (!globalDeclarations.contains(originDeclaration))
                        globalDeclarations.add(originDeclaration);

                    String oldDeclaration = ((ExternalVariableNode) child).getASTType().getRawSignature() + " "
                            + ((ExternalVariableNode) child).getASTDecName().getRawSignature() + ";";

                    String header = child.getAbsolutePath();

                    if (header.startsWith(File.separator))
                        header = header.substring(1);

                    header = header.replaceAll("[^\\w]", SpecialCharacter.UNDERSCORE).toUpperCase();

                    String newDeclaration = wrapInIncludeGuard(SourceConstant.GLOBAL_PREFIX + header, oldDeclaration);

                    refactors.put(originDeclaration, newDeclaration + "\n" + originDeclaration);
                }
            }
        }
    }

    /**
     * Perform on instrumentation on the original function
     */
    private String generateInstrumentedFunction(IFunctionNode functionNode) {
        final String success = String.format("/** Instrumented function %s */\n", functionNode.getName());
        final String fail = String.format("/** Can not instrument function %s */\n", functionNode.getName());

        String instrumentedSourceCode;

        IASTFunctionDefinition astInstrumentedFunction = functionNode.getAST();

        AbstractFunctionInstrumentation fnInstrumentation = new FunctionInstrumentationForAllCoverages(
                astInstrumentedFunction, functionNode);

        fnInstrumentation.setFunctionPath(functionNode.getAbsolutePath());

        String instrument = fnInstrumentation.generateInstrumentedFunction();
        if (instrument == null || instrument.length() == 0) {
            // can not instrument
            instrumentedSourceCode = fail + functionNode.getAST().getRawSignature();
        } else {
            instrumentedSourceCode = success + instrument;
            int bodyIdx = instrumentedSourceCode.indexOf(SpecialCharacter.OPEN_BRACE) + 1;

            instrumentedSourceCode = instrumentedSourceCode.substring(0, bodyIdx)
                    + generateCallingMark(functionNode.getAbsolutePath()) // insert mark start function
                    + instrumentedSourceCode.substring(bodyIdx);
        }

        return instrumentedSourceCode;
    }
}
