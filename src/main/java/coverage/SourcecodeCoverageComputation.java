package coverage;

import environment.EnviroCoverageTypeNode;
import environment.Environment;
import parser.object.INode;
import parser.object.ISourcecodeFileNode;
import util.PathUtils;

import java.io.File;
import java.util.Map;

/**
 * This class is used to compute coverage of source code file.
 *
 * The type of coverage is only STATEMENT, BRANCH, and MCDC.
 *
 * For STATEMENT+BRANCH, STATEMENT+MCDC, these kinds of coverage include two coverage types.
 */
public class SourcecodeCoverageComputation extends AbstractCoverageComputation{
    
    public void compute() {
        if (consideredSourcecodeNode == null || !(new File(consideredSourcecodeNode.getAbsolutePath())).exists()
                || !(consideredSourcecodeNode instanceof ISourcecodeFileNode)
                || testpathContent == null || testpathContent.length() == 0) {
            this.numberOfInstructions = getNumberOfInstructions(consideredSourcecodeNode, coverage);
            return;
        }
        if (coverage.equals(EnviroCoverageTypeNode.STATEMENT_AND_BRANCH) ||
                coverage.equals(EnviroCoverageTypeNode.STATEMENT_AND_MCDC))
            return;

        Map<String, TestpathsOfAFunction> affectedFunctions = categoryTestpathByFunctionPath(testpathContent.split("\n"), coverage);
        affectedFunctions = removeRedundantTestpath(affectedFunctions);

        int nInstructions = getNumberOfInstructions(consideredSourcecodeNode, coverage);

        int nVisitedInstructions;
//        if (visitedCache.containsKey(testpathContent)) {
//            logger.debug("Coverage Computer Cache Hit");
//            nVisitedInstructions = visitedCache.get(testpathContent);
//            logger.debug("Visited instructions: " + nVisitedInstructions);
//        } else {
//            logger.debug("Coverage Computer Cache Miss");
//            logger.debug("Calculate visited instructions");
        nVisitedInstructions = getNumberOfVisitedInstructions(affectedFunctions, coverage, consideredSourcecodeNode, allCFG);
//            visitedCache.put(testpathContent, nVisitedInstructions);
//        }

        this.numberOfInstructions = nInstructions;
        this.numberOfVisitedInstructions = nVisitedInstructions;
    }

    protected Map<String, TestpathsOfAFunction>  removeRedundantTestpath(Map<String, TestpathsOfAFunction> affectedFunctions){
        return affectedFunctions;
    }

    @Override
    protected int getNumberofBranches(INode consideredSourcecodeNode) {
        String path = PathUtils.toRelative(consideredSourcecodeNode.getAbsolutePath());

        if (!Environment.getInstance().getBranchesMapping().containsKey(path)) {
            int number = InstructionComputator.getNumberOfStatements(consideredSourcecodeNode, EnviroCoverageTypeNode.BRANCH);
            Environment.getInstance().getBranchesMapping().put(path, number);
        }

        return Environment.getInstance().getBranchesMapping().get(path);
    }

    @Override
    protected int getNumberofStatements(INode consideredSourcecodeNode) {
        String path = PathUtils.toRelative(consideredSourcecodeNode.getAbsolutePath());

        if (!Environment.getInstance().getStatementsMapping().containsKey(path)) {
            int number = InstructionComputator.getNumberOfStatements(consideredSourcecodeNode, EnviroCoverageTypeNode.STATEMENT);
            Environment.getInstance().getStatementsMapping().put(path, number);
        }

        return Environment.getInstance().getStatementsMapping().get(path);
    }

    @Override
    protected int getNumberofMcdcs(INode consideredSourcecodeNode) {
        String path = PathUtils.toRelative(consideredSourcecodeNode.getAbsolutePath());

        if (!Environment.getInstance().getMcdcMapping().containsKey(path)) {
            int number = InstructionComputator.getNumberOfStatements(consideredSourcecodeNode, EnviroCoverageTypeNode.MCDC);
            Environment.getInstance().getMcdcMapping().put(path, number);
        }

        return Environment.getInstance().getMcdcMapping().get(path);
    }
}
