package coverage;

import auto_testcase_generation.cfg.ICFG;
import environment.EnviroCoverageTypeNode;
import parser.object.IFunctionNode;
import parser.object.INode;
import search.Search;
import search.condition.AbstractFunctionNodeCondition;
import util.CFGUtils;
import util.VFPLogger;

import java.util.List;

/**
 * Compute the number of statements/branches
 */
public class InstructionComputator {

    final static VFPLogger logger = VFPLogger.get(InstructionComputator.class);

    private static final String ERROR_FORMAT = "Error with %s: %s";

    public static int getNumberOfStatements(INode consideredSourcecodeNode, String coverageType) {

        // constructor, destructor, normal function
        List<INode> functionNodes = Search.searchNodes(consideredSourcecodeNode, new AbstractFunctionNodeCondition());

        int result = 0;

        for (INode node : functionNodes) {
            if (node instanceof IFunctionNode) {
                IFunctionNode functionNode = (IFunctionNode) node;
                result += computeNodeByType(functionNode, coverageType);
            }
        }

        return result;
    }


    private static int computeNodeByType(IFunctionNode functionNode, String coverageType) {
        try {
            ICFG cfg = CFGUtils.createCFG(functionNode, coverageType);

            switch (coverageType) {
                case EnviroCoverageTypeNode.BRANCH:
                case EnviroCoverageTypeNode.MCDC:
                    return cfg.getVisitedBranches().size() + cfg.getUnvisitedBranches().size();

                case EnviroCoverageTypeNode.STATEMENT:
                    return cfg.getVisitedStatements().size() + cfg.getUnvisitedStatements().size();

                default:
                    return 0;
            }

        } catch (Exception ex) {
            logger.debug(String.format(ERROR_FORMAT, functionNode.getName(), ex.getMessage()));
            return 0;
        }
    }
}
