package coverage;

import auto_testcase_generation.cfg.ICFG;
import coverage.highlight.SourcecodeHighlighterForCoverage;
import parser.object.ISourcecodeFileNode;
import testcase_manager.TestCase;
import util.Utils;
import util.VFPLogger;

import java.io.File;
import java.util.List;

public class CoverageManager {
    private final static VFPLogger logger = VFPLogger.get(CoverageManager.class);
    public static final String EMPTY = "";
    public static final float ZERO_COVERAGE = 0;

    private static String highlightSourcecode(ISourcecodeFileNode sourcecodeFileNode, TestCase testCase, String typeOfCoverage, List<ICFG> allCFG){
        SourcecodeHighlighterForCoverage sourcecodeHighlighter = new SourcecodeHighlighterForCoverage();
        sourcecodeHighlighter.setSourcecode(sourcecodeFileNode.getAST().getRawSignature());
        sourcecodeHighlighter.setTestpathContent(Utils.readFileContent(testCase.getTestPathFile()));
        sourcecodeHighlighter.setSourcecodePath(sourcecodeFileNode.getAbsolutePath());
        sourcecodeHighlighter.setTypeOfCoverage(typeOfCoverage);
        sourcecodeHighlighter.setAllCFG(allCFG);
        sourcecodeHighlighter.highlight();
        String mcdcCoverageContent = sourcecodeHighlighter.getSimpliedHighlightedSourcecode();
        return mcdcCoverageContent;
    }

    /**
     *
     * @param testCases test cases of a function
     * @param typeOfCoverage
     * @return
     */
    public static CoverageDataObject getCoverageOfMultiTestCaseAtFunctionLevel(List<TestCase> testCases, String typeOfCoverage) {
        if (testCases.size() == 0)
            return null;

        // get all test paths
        String allTestpaths = "";
        for (TestCase testCase : testCases)
            if (testCase != null && testCase.getTestPathFile() != null && new File(testCase.getTestPathFile()).exists())
                allTestpaths += Utils.readFileContent(testCase.getTestPathFile()) + "\n";

        // coverage
        if (allTestpaths.length() > 0 && testCases != null && testCases.get(0) != null) {
            CoverageDataObject coverageDataObject = new CoverageDataObject();

            ISourcecodeFileNode sourcecodeNode = Utils.getSourcecodeFile(testCases.get(0).getFunctionNode());
            FunctionCoverageComputation covComputation = new FunctionCoverageComputation();
            covComputation.setFunctionNode(testCases.get(0).getFunctionNode());
            covComputation.setCoverage(typeOfCoverage);
            covComputation.setConsideredSourcecodeNode(sourcecodeNode);
            covComputation.setTestpathContent(allTestpaths);
            covComputation.compute();
            coverageDataObject.setProgress(covComputation.getNumberOfVisitedInstructions() * 1.0f / covComputation.getNumberOfInstructions());
            coverageDataObject.setTotal(covComputation.getNumberOfInstructions());
            coverageDataObject.setVisited(covComputation.getNumberOfVisitedInstructions());


            // highlight after coverage computation
            SourcecodeHighlighterForCoverage sourcecodeHighlighter = new SourcecodeHighlighterForCoverage();
            sourcecodeHighlighter.setTypeOfCoverage(typeOfCoverage);
            sourcecodeHighlighter.setAllCFG(covComputation.getAllCFG());
            sourcecodeHighlighter.setSourcecode(Utils.readFileContent(sourcecodeNode.getAbsolutePath()));
            sourcecodeHighlighter.setSourcecodePath(sourcecodeNode.getAbsolutePath());
            sourcecodeHighlighter.setTestpathContent(allTestpaths);
            sourcecodeHighlighter.highlight();
            String fullHighlight = sourcecodeHighlighter.getFullHighlightedSourcecode();
            coverageDataObject.setContent(fullHighlight);

            return coverageDataObject;
        } else if (testCases.get(0) != null && testCases.get(0).getFunctionNode() != null){
            // we have compilable test cases, but we can not execute them successfully
            CoverageDataObject coverageDataObject = new CoverageDataObject();

            ISourcecodeFileNode sourcecodeNode = Utils.getSourcecodeFile(testCases.get(0).getFunctionNode());
            FunctionCoverageComputation covComputation = new FunctionCoverageComputation();
            covComputation.setCoverage(typeOfCoverage);
            covComputation.setConsideredSourcecodeNode(sourcecodeNode);
            covComputation.setTestpathContent(allTestpaths);
            covComputation.setFunctionNode(testCases.get(0).getFunctionNode());
            covComputation.compute();

            coverageDataObject.setProgress(Float.NaN);
            coverageDataObject.setTotal(covComputation.getNumberOfInstructions());
            coverageDataObject.setVisited(0);
            return coverageDataObject;
        } else
            return null;
    }

        /**
         * Compute coverage of multiple test cases
         * @param testCases test cases of a function
         * @param typeOfCoverage
         * @return
         */
    public static CoverageDataObject getCoverageOfMultiTestCaseAtSourcecodeFileLevel(List<TestCase> testCases, String typeOfCoverage) {
        if (testCases.size() == 0)
            return null;

        // get all test paths
        String allTestpaths = "";
        for (TestCase testCase : testCases)
            if (testCase != null && testCase.getTestPathFile() != null && new File(testCase.getTestPathFile()).exists())
                allTestpaths += Utils.readFileContent(testCase.getTestPathFile()) + "\n";

        // coverage
        if (allTestpaths.length() > 0 && testCases != null && testCases.get(0) != null) {
            CoverageDataObject coverageDataObject = new CoverageDataObject();

            ISourcecodeFileNode sourcecodeNode = Utils.getSourcecodeFile(testCases.get(0).getFunctionNode());
            SourcecodeCoverageComputation sourcecodeCoverageComputation = new SourcecodeCoverageComputation();
            sourcecodeCoverageComputation.setCoverage(typeOfCoverage);
            sourcecodeCoverageComputation.setConsideredSourcecodeNode(sourcecodeNode);
            sourcecodeCoverageComputation.setTestpathContent(allTestpaths);
            sourcecodeCoverageComputation.compute();
            coverageDataObject.setProgress(sourcecodeCoverageComputation.getNumberOfVisitedInstructions() * 1.0f / sourcecodeCoverageComputation.getNumberOfInstructions());
            coverageDataObject.setTotal(sourcecodeCoverageComputation.getNumberOfInstructions());
            coverageDataObject.setVisited(sourcecodeCoverageComputation.getNumberOfVisitedInstructions());

            // highlight after coverage computation
            SourcecodeHighlighterForCoverage sourcecodeHighlighter = new SourcecodeHighlighterForCoverage();
            sourcecodeHighlighter.setTypeOfCoverage(typeOfCoverage);
            sourcecodeHighlighter.setAllCFG(sourcecodeCoverageComputation.getAllCFG());
            sourcecodeHighlighter.setSourcecode(Utils.readFileContent(sourcecodeNode.getAbsolutePath()));
            sourcecodeHighlighter.setSourcecodePath(sourcecodeNode.getAbsolutePath());
            sourcecodeHighlighter.setTestpathContent(allTestpaths);
            sourcecodeHighlighter.highlight();
            String fullHighlight = sourcecodeHighlighter.getFullHighlightedSourcecode();
            coverageDataObject.setContent(fullHighlight);

            return coverageDataObject;
        } else if (testCases != null && testCases.get(0) != null && testCases.get(0).getFunctionNode() != null){
            // we have compilable test cases, but we can not execute them successfully
            CoverageDataObject coverageDataObject = new CoverageDataObject();

            ISourcecodeFileNode sourcecodeNode = Utils.getSourcecodeFile(testCases.get(0).getFunctionNode());
            SourcecodeCoverageComputation sourcecodeCoverageComputation = new SourcecodeCoverageComputation();
            sourcecodeCoverageComputation.setCoverage(typeOfCoverage);
            sourcecodeCoverageComputation.setConsideredSourcecodeNode(sourcecodeNode);
            sourcecodeCoverageComputation.setTestpathContent(allTestpaths);
            sourcecodeCoverageComputation.compute();

            coverageDataObject.setProgress(Float.NaN);
            coverageDataObject.setTotal(sourcecodeCoverageComputation.getNumberOfInstructions());
            coverageDataObject.setVisited(0);
            return coverageDataObject;
        } else
            return null;
    }

    public static String removeRedundantLineBreak(String content) {
        content = content.replace("\r", "\n");
        content = content.replace("\n\n", "\n");
        return content;
    }

}
