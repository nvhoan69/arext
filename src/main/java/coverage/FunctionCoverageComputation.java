package coverage;

import auto_testcase_generation.cfg.ICFG;
import environment.EnviroCoverageTypeNode;
import parser.object.AbstractFunctionNode;
import parser.object.ICommonFunctionNode;
import parser.object.IFunctionNode;
import parser.object.INode;
import util.CFGUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * This class is used to compute coverage of function level
 * <p>
 * The type of coverage is only STATEMENT, BRANCH, and MCDC.
 * <p>
 * For STATEMENT+BRANCH, STATEMENT+MCDC, these kinds of coverage include two coverage types.
 */
public class FunctionCoverageComputation extends AbstractCoverageComputation {

    protected ICommonFunctionNode functionNode;

    public void compute() {
        if (functionNode == null
                || !(functionNode instanceof IFunctionNode)
                || testpathContent == null || testpathContent.length() == 0) {
            this.numberOfInstructions = getNumberOfInstructions(functionNode, coverage);
            return;
        }
        if (coverage.equals(EnviroCoverageTypeNode.STATEMENT_AND_BRANCH) ||
                coverage.equals(EnviroCoverageTypeNode.STATEMENT_AND_MCDC))
            return;

        Map<String, TestpathsOfAFunction> affectedFunctions = categoryTestpathByFunctionPath(testpathContent.split("\n"), coverage);
        affectedFunctions = removeRedundantTestpath(affectedFunctions);

        int nInstructions = getNumberOfInstructions(functionNode, coverage);

        int nVisitedInstructions;
        nVisitedInstructions = getNumberOfVisitedInstructions(affectedFunctions, coverage, consideredSourcecodeNode, allCFG);

        this.numberOfInstructions = nInstructions;
        this.numberOfVisitedInstructions = nVisitedInstructions;
    }

    protected Map<String, TestpathsOfAFunction> removeRedundantTestpath(Map<String, TestpathsOfAFunction> affectedFunctions){
        Map<String, TestpathsOfAFunction> output = new HashMap<>();
        String path = functionNode.getAbsolutePath();
        output.put(path, affectedFunctions.get(path));
        return output;
    }

    protected int getNumberofMcdcs(INode functionNode) {
        int nMcdcs = 0;
        if (functionNode instanceof AbstractFunctionNode) {
            try {
                ICFG cfg = CFGUtils.createCFG((IFunctionNode) functionNode, EnviroCoverageTypeNode.MCDC);
                if (cfg != null) {
                    nMcdcs += cfg.getVisitedBranches().size() + cfg.getUnvisitedBranches().size();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return nMcdcs;
    }

    protected int getNumberofBranches(INode functionNode) {
        int nBranches = 0;
        if (functionNode instanceof AbstractFunctionNode) {
            try {
                ICFG cfg = CFGUtils.createCFG((IFunctionNode) functionNode, EnviroCoverageTypeNode.BRANCH);
                if (cfg != null) {
                    nBranches += cfg.getVisitedBranches().size() + cfg.getUnvisitedBranches().size();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return nBranches;
    }

    protected int getNumberofStatements(INode functionNode) {
        int nStatements = 0;
        if (functionNode instanceof AbstractFunctionNode) {
            try {
                ICFG cfg = CFGUtils.createCFG((IFunctionNode) functionNode, EnviroCoverageTypeNode.STATEMENT);
                if (cfg != null) {
                    nStatements += cfg.getVisitedStatements().size() + cfg.getUnvisitedStatements().size();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return nStatements;
    }


    public void setFunctionNode(ICommonFunctionNode functionNode) {
        this.functionNode = functionNode;
    }

    public ICommonFunctionNode getFunctionNode() {
        return functionNode;
    }
}
