package coverage.highlight;

/**
 * Highlight source code
 */
public interface IHighlighter {
    void highlight();
}
