package coverage.highlight;

import auto_testcase_generation.cfg.ICFG;
import auto_testcase_generation.cfg.object.ICfgNode;
import util.VFPLogger;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class FunctionHighlighter extends AbstractHighlighter {

    private final static VFPLogger logger = VFPLogger.get(FunctionHighlighter.class);

    @Override
    public void highlight() {
        //
    }

    public String highlightMCDC(ICFG cfg) {
        // The way we highlight function in MCDC coverage
        String highlightedFunction = highlightVisitedBlock(cfg);

        // add some information here
        return highlightedFunction;
    }

    public String highlightBranches(ICFG cfg) {
        // The way we highlight function in branch coverage and in statement coverage are the same
        String highlightedFunction = highlightVisitedBlock(cfg);

        // add some information here
        return highlightedFunction;
    }

    public String highlightVisitedBlock(ICFG cfg) {
        String originFunction = cfg.getFunctionNode().getAST().getRawSignature();

        // create a map
        Map<Object, ICfgNode> visitedNodesMap = new TreeMap<>(Collections.reverseOrder());
        int startingOffsetOfFunctionInSourcecode = cfg.getFunctionNode().getAST().getFileLocation().getNodeOffset();
        List<ICfgNode> nodes = cfg.getVisitedStatements();
        for (ICfgNode node : nodes) {
            Integer key = new Integer(node.getAstLocation().getNodeOffset() - startingOffsetOfFunctionInSourcecode);
            visitedNodesMap.put(key, node);
        }

        // sort the map
        for (Object key : visitedNodesMap.keySet()) {
            int start = ((Integer) key).intValue();
            int end = start + visitedNodesMap.get(key).getAstLocation().getNodeLength();

            String pre = originFunction.substring(0, start);
            String after = originFunction.substring(end);

            String middle = originFunction.substring(start, end);
            middle = "<b style=\"background-color:#7bd243;\">" + middle + "</b>";
            originFunction = pre + middle + after;
        }

        // display line numbers
        originFunction = addLineNumber(originFunction);
        originFunction = addPre(originFunction);
        return originFunction;
    }

    public String highlightStatements(ICFG cfg) {
        // The way we highlight function in MCDC coverage
        String highlightedFunction = highlightVisitedBlock(cfg);

        // add some information here
        return highlightedFunction;
    }

}
