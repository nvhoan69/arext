package util;

import auto_testcase_generation.track.MemoryTracker;
import auto_testcase_generation.track.QuantityTracker;
import auto_testcase_generation.track.TimeTracker;
import coverage.CoverageDataObject;
import coverage.CoverageManager;
import environment.Environment;
import parser.object.ICommonFunctionNode;
import parser.object.INode;
import testcase_manager.TestCase;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class CsvUtils {

    private static final VFPLogger logger = VFPLogger.get(CsvUtils.class);

    public static final String CSV_FILE_PATH = "vfpv/report/autogen.csv";

    public static String appendResult(ICommonFunctionNode fn, List<TestCase> testCases) {
        String typeOfCoverage = Environment.getInstance().getTypeofCoverage();

        CoverageDataObject srcCoverageData = CoverageManager
                .getCoverageOfMultiTestCaseAtSourcecodeFileLevel(testCases, typeOfCoverage);

        double srcCoverage = 0f;
        if (srcCoverageData != null) {
            int visited = srcCoverageData.getVisited();
            int total = srcCoverageData.getTotal();
            srcCoverage = (double) visited / (double) total;
        }

        CoverageDataObject funcCoverageData = CoverageManager
                .getCoverageOfMultiTestCaseAtFunctionLevel(testCases, typeOfCoverage);

        double funcCoverage = 0f;
        if (funcCoverageData != null) {
            int visited = funcCoverageData.getVisited();
            int total = funcCoverageData.getTotal();
            funcCoverage = (double) visited / (double) total;
        }

        long timeInNano = TimeTracker.getInstance().get(fn);
        int num = QuantityTracker.getInstance().get(fn);
        long mem = MemoryTracker.getInstance().get(fn);

        INode unit = Utils.getSourcecodeFile(fn);
        String line = String.format("%s,%s,%f,%f,%d,%d,%d\n",
                fn.getSimpleName(), unit.getName(), srcCoverage, funcCoverage, timeInNano, num, mem);

        try {
            FileWriter fw = new FileWriter(CSV_FILE_PATH, true);
            fw.write(line);
            fw.close();
            logger.debug("Export: " + line);
        } catch (IOException e) {
            logger.debug("Cant export: " + line);
        }

        return line;
    }

    public static final String FIRST_LINE = "Function,File,FileCov,FuncCov,Time,TestCases,Memory";
}
