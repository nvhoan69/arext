package util;

import auto_testcase_generation.cfg.ICFG;
import auto_testcase_generation.cfg.object.ICfgNode;
import parser.object.ICommonFunctionNode;
import testcase_manager.ITestCase;
import testcase_manager.TestCase;
import testcase_manager.minimize.Scope;
import testcase_manager.minimize.TestCaseMinimizer;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TestPathUtils {

    public static List<ICfgNode> getVisited(TestCase testCase, Scope scope) {
        List<ICfgNode> visited = new ArrayList<>();

        if (testCase.getTestPathFile() != null) {
            File testPath = new File(testCase.getTestPathFile());

            if (testPath.exists()) {
                List<ICFG> cfgList = null;
                if (scope == Scope.FUNCTION) {
                    ICFG cfg = CFGUtils.getAndMarkCFG(testCase);
                    if (cfg != null)
                        cfgList = Collections.singletonList(cfg);
                } else
                    cfgList = CFGUtils.getAndMarkAllCFG(testCase);

                if (cfgList != null) {
                    for (ICFG cfg : cfgList) {
                        List<ICfgNode> list = cfg.getVisitedStatements();
                        visited.addAll(list);
                    }
                }
            }
        }

        return visited;
    }

    private static List<ICfgNode> getVisitedOfCurrentType(TestCase testCase, Scope scope) {
        return getVisited(testCase, scope);
    }

    public static int compare(TestCase testCase, List<TestCase> other, Scope scope) {
        ICommonFunctionNode sut = testCase.getFunctionNode();

        if (!isExecutable(testCase))
            return HAVENT_EXEC;

        List<ICfgNode> visited1 = getVisitedOfCurrentType(testCase, scope);
        List<ICfgNode> visited2 = new ArrayList<>();

        for (TestCase tc : other) {
            TestCaseMinimizer.logger.debug("Comparing test case " + testCase.getName() + " with " + tc.getName());
            if (!tc.getFunctionNode().equals(sut))
                return ERR_COMPARE;

            if (isExecutable(tc)) {
                List<ICfgNode> visited = getVisitedOfCurrentType(tc, scope);
                for (ICfgNode node : visited) {
                    if (!visited2.contains(node))
                        visited2.add(node);
                }
            }
        }

        if (visited2.isEmpty())
            return HAVENT_EXEC * -1;

        return compare(visited1, visited2);
    }

    public static int compare(List<ICfgNode> visited1, List<ICfgNode> visited2) {
        int result;

        if (visited1 == null || visited2 == null)
            return ERR_COMPARE;
        else if (visited1.size() >= visited2.size())
            result = 1;
        else
            result = -1;

        List<Object> commonPart = new ArrayList<>();

        for (Object node1 : visited1) {
            for (Object node2 : visited2) {
                if (node1.equals(node2)) {
                    commonPart.add(node1);
                }
            }
        }

        if (commonPart.isEmpty()) {
            // do nothing
        } else if (commonPart.size() == visited1.size() && commonPart.size() == visited2.size())
            result *= EQUAL;
        else if (commonPart.size() == visited1.size() || commonPart.size() == visited2.size())
            result *= CONTAIN_GT;
        else
            result *= COMMON_GT;

        return result;

    }

    private static boolean isExecutable(ITestCase testCase) {
        return testCase.getStatus().equals(ITestCase.STATUS_RUNTIME_ERR)
                || testCase.getStatus().equals(ITestCase.STATUS_SUCCESS);
    }

    public static final int EQUAL = 0;
    public static final int SEPARATED_GT = 1;
    public static final int COMMON_GT = 2;
    public static final int CONTAIN_GT = 3;
    public static final int ERR_COMPARE = 4;
    public static final int HAVENT_EXEC = 5;
}
