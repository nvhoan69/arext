package util;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DateTimeUtils {
    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("dd MMM uuuu");
    private static final DateTimeFormatter TIME_FORMATTER = DateTimeFormatter.ofPattern("hh:mm:ss a");

    public static String getDate(LocalDateTime dateTime) {
        if (dateTime == null)
            dateTime = LocalDateTime.now();
        return DATE_FORMATTER.format(dateTime).toUpperCase();
    }

    public static String getTime(LocalDateTime dateTime) {
        if (dateTime == null)
            dateTime = LocalDateTime.now();
        return TIME_FORMATTER.format(dateTime).toUpperCase();
    }

    public static LocalDateTime parse(String dateTime) {
        if (dateTime == null)
            return null;
        return LocalDateTime.parse(dateTime);
    }
}
