package util;

import testcase_execution.DriverConstant;

public interface SourceConstant {

    String EXPECTED_OUTPUT = "VFP_EXPECTED_OUTPUT";

    String ACTUAL_OUTPUT = "VFP_ACTUAL_OUTPUT";

    String INSTANCE_VARIABLE = "VFP_INSTANCE";

    String INSTANCE_VARIABLE_POINTER = "VFP_INSTANCE_PTR";

    String MARK_STM = DriverConstant.MARK;

    String INCREASE_FCALLS = DriverConstant.CALL_COUNTER + "++;";

    String STUB_PREFIX = "VFP_STUB_";

    String SRC_PREFIX = "VFP_SRC_";

    String EXPECTED_PREFIX = "EXPECTED_";

    String INCLUDE_PREFIX = "VFP_INCLUDE_";

    String GLOBAL_PREFIX = "VFP_GLOBAL_";

    String TEST = "TEST";

    String EXPECT_EQ = "EXPECT_EQ";

    // Assert integer value
    String ASSERT_EQ = "EXPECT_EQ";

    // Assert decimal value
    String ASSERT_NEAR = "ASSERT_NEAR";

    String PASSED_FLAG = "[  PASSED  ]";

    String FAILED_FLAG = "[  FAILED  ]";
}
