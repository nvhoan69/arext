package util;

import environment.Environment;
import parser.dependency.Dependency;
import parser.dependency.TypeDependency;
import parser.object.INode;
import parser.object.VariableNode;
import search.Search;
import search.condition.StructurevsTypedefCondition;

import java.util.List;

public class ResolveCoreTypeHelper {

    private final static VFPLogger logger = VFPLogger.get(ResolveCoreTypeHelper.class);

    public static INode resolve(String vPath) {
        List<Dependency> dependencies = Environment.getInstance().getDependencies();
        for (Dependency dependency : dependencies) {
            if (dependency instanceof TypeDependency) {
                if (dependency.getStartArrow().getAbsolutePath().equals(vPath)) {
                    return dependency.getEndArrow();
                }
            }
        }

        logger.error("Can not resolve " + vPath);
        return null;
    }

    public static INode resolve(VariableNode variableNode) {
        INode coreType = resolve(variableNode.getAbsolutePath());
        if (coreType == null) {
            logger.error("Can not resolve core type for the variableNode: " + variableNode.getAbsolutePath());
        }
        return coreType;
    }

    public static INode getType(String absolutePath) {
        // Search Level 1
        List<Dependency> dependencies = Environment.getInstance().getDependencies();
        for (Dependency dependency : dependencies) {
            if (dependency instanceof TypeDependency) {
                if (dependency.getEndArrow().getAbsolutePath().equals(absolutePath)) {
                    return dependency.getEndArrow();
                }
            }
        }

        // Search Level 2
        List<INode> structureNodes = Search
                .searchNodes(Environment.getInstance().getProjectNode(), new StructurevsTypedefCondition());
        for (INode structureNode : structureNodes) {
            if (structureNode.getAbsolutePath().equals(absolutePath)) {
                return structureNode;
            }
        }

        return null;
    }
}
