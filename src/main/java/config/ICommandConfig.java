package config;

import java.util.Map;

public interface ICommandConfig {
    Map<String, String> getCompilationCommands();
    String getLinkingCommand();
    String getExecutablePath();
}
