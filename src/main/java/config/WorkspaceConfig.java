package config;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import compiler.AvailableCompiler;
import compiler.Compiler;
import environment.EnviroCoverageTypeNode;
import util.Utils;

import java.io.File;

import static util.CsvUtils.CSV_FILE_PATH;
import static util.CsvUtils.FIRST_LINE;

public class WorkspaceConfig {

    private static WorkspaceConfig instance;

    public static WorkspaceConfig getInstance() {
        if (instance == null)
            instance = new WorkspaceConfig();
        return instance;
    }

    public void initialize() {
        if (!WorkspaceConfig.CONFIG_FILE.exists()) {
            Compiler compiler;
            try {
                compiler = new Compiler(AvailableCompiler.C_GNU_NATIVE.class);
            } catch (IllegalAccessException | NoSuchFieldException e) {
                compiler = new Compiler();
            }
            JsonObject jsonCompiler = new JsonParser().parse(new Gson().toJson(compiler)).getAsJsonObject();
            JsonObject config = new JsonObject();
            config.add("compiler", jsonCompiler);
            config.addProperty("coverage", EnviroCoverageTypeNode.STATEMENT);
            config.addProperty("z3Path", "");

            Utils.writeContentToFile(config.toString(), CONFIG_FILE.getAbsolutePath());

            if (!TEST_DRIVER_DIR.exists())
                TEST_DRIVER_DIR.mkdirs();

            if (!EXE_DIR.exists())
                EXE_DIR.mkdirs();

            if (!CONSTRAINT_DIR.exists())
                CONSTRAINT_DIR.mkdirs();

            if (!TEST_PATH_DIR.exists())
                TEST_PATH_DIR.mkdirs();

            if (!new File(CSV_FILE_PATH).exists())
                Utils.writeContentToFile(FIRST_LINE, CSV_FILE_PATH);
        }
    }

    public static final File CONFIG_FILE = new File("vfpv/config.json");

    public static final File TEST_DRIVER_DIR = new File("vfpv/test-driver");
    public static final File EXE_DIR = new File("vfpv/exe");
    public static final File CONSTRAINT_DIR = new File("vfpv/constraint");
    public static final File TEST_PATH_DIR = new File("vfpv/test-path");
}
