package testcase_execution.testdriver;

import parser.object.ConstructorNode;
import testcase_execution.DriverConstant;
import testcase_manager.TestCase;
import util.SourceConstant;
import util.SpecialCharacter;

public abstract class AssertableTestDriverGeneration extends TestDriverGeneration {

    protected String generateBodyScript(TestCase testCase) throws Exception {
        // STEP 1: assign VFP test case name
        String testCaseNameAssign = String.format("%s=\"%s\";", DriverConstant.VFP_TEST_CASE_NAME, testCase.getName());

        // STEP 2: Generate initialization of variables
        String initialization = generateInitialization(testCase);

        // STEP 3: Generate full function call
        String functionCall = generateFunctionCall(testCase);

        // STEP 4: FCALLS++ - Returned from UUT
        String increaseFcall;
        if (testCase.getFunctionNode() instanceof ConstructorNode)
            increaseFcall = SpecialCharacter.EMPTY;
        else
            increaseFcall = SourceConstant.INCREASE_FCALLS + generateReturnMark(testCase);

        // STEP 5: Repeat iterator
        String singleScript = String.format(
                    "{\n" +
                        "%s\n" +
                        "%s\n" +
                        "%s\n" +
                        "%s\n" +
                    "}",
                testCaseNameAssign,
                initialization,
                functionCall,
                increaseFcall);

        singleScript = wrapScriptInTryCatch(singleScript);

        return singleScript;
    }
}
