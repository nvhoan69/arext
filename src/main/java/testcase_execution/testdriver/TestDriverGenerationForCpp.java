package testcase_execution.testdriver;

import testcase_execution.DriverConstant;
import util.Utils;

/**
 * Old name: TestdriverGenerationforCpp
 *
 * Generate test driver for function put in an .cpp file in executing test data entering by users
 * <p>
 * comparing EO and RO
 *
 * 
 */
public class TestDriverGenerationForCpp extends AssertableTestDriverGeneration {

    @Override
    public String getTestDriverTemplate() {
        return Utils.readResourceContent(CPP_TEST_DRIVER_PATH);
    }

    protected String wrapScriptInTryCatch(String script) {
        return String.format(
                "try {\n" +
                        "%s\n" +
                        "} catch (std::exception& error) {\n" +
                        DriverConstant.MARK + "(\"Phat hien loi runtime\");\n" +
                        "}", script);
    }
}
