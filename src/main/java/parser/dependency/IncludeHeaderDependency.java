package parser.dependency;

import parser.object.INode;

public class IncludeHeaderDependency extends Dependency {

    public IncludeHeaderDependency(INode startArrow, INode endArrow) {
        super(startArrow, endArrow);
    }

}
