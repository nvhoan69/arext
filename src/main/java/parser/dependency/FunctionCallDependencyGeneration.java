package parser.dependency;

import org.eclipse.cdt.core.dom.ast.IASTFunctionCallExpression;
import org.eclipse.cdt.core.dom.ast.IASTFunctionDefinition;
import org.eclipse.cdt.core.dom.ast.IASTSimpleDeclaration;
import parser.FunctionCallParser;
import parser.dependency.finder.MethodFinder;
import parser.object.ICommonFunctionNode;
import parser.object.IFunctionNode;
import parser.object.INode;
import util.VFPLogger;

import java.util.List;

public class FunctionCallDependencyGeneration extends AbstractDependencyGeneration {
    final static VFPLogger logger = VFPLogger.get(FunctionCallDependencyGeneration.class);

    public void dependencyGeneration(INode root) {
        if (root instanceof IFunctionNode) {
            if (!((IFunctionNode) root).isFunctionCallDependencyState()) {
                IASTFunctionDefinition fnAst = ((IFunctionNode) root).getAST();

                FunctionCallParser visitor = new FunctionCallParser();
                fnAst.accept(visitor);

                checkExpectedList(visitor, (IFunctionNode) root);
                checkUnExpectedList(visitor, (IFunctionNode) root);
                ((IFunctionNode) root).setFunctionCallDependencyState(true);
            } else {
                logger.debug(root.getAbsolutePath() + " is analyzed function call dependency before");
            }

        }
    }

    private void checkExpectedList(FunctionCallParser visitor, IFunctionNode owner) {
        List<IASTFunctionCallExpression> expressions = visitor.getExpressions();

        for (IASTFunctionCallExpression expression : expressions) {
//            IASTExpression ex = expression.getFunctionNameExpression();
//            String funcName = getFunctionName(ex, owner);
//            String funcName = null;
//            if (ex instanceof IASTFieldReference) {
//                String type = new TypeResolver(owner).exec(((IASTFieldReference) ex).getFieldOwner());
//                funcName = type + "::" + ((IASTFieldReference) ex).getFieldName().toString();
//
//                VariableSearchingSpace space = new VariableSearchingSpace(owner);
//                INode typeNode = space.search(type, new StructurevsTypedefCondition());
//                if (typeNode instanceof StructureNode) {
//                    INode parent = typeNode.getParent();
//                    // TODO: structure define in another file
//                    while (parent != null) {
//                        // structure define in a namespace
//                        if (parent instanceof NamespaceNode) {
//                            String namespace = parent.getName();
//                            funcName = namespace + "::" + funcName;
//                        }
//                        parent = parent.getParent();
//                    }
//                }
//                IType type = ((IASTFieldReference) ex).getFieldOwner().getExpressionType();
//                funcName = type.toString() + "::" + ((IASTFieldReference) ex).getFieldName().toString();
//
//                if (type instanceof CPPClassType) {
//                    IASTNode parent = ((CPPClassType) type).getDefinition().getParent();
//                    // TODO: structure define in another file
//                    while (parent != null) {
//                        // structure define in a namespace
//                        if (parent instanceof ICPPASTNamespaceDefinition) {
//                            String namespace = ((ICPPASTNamespaceDefinition) parent).getName().getRawSignature();
//                            funcName = namespace + "::" + funcName;
//                        }
//                        parent = parent.getParent();
//                    }
//                }
//            } else if (ex instanceof IASTIdExpression) {
//                funcName = ((IASTIdExpression) ex).getName().toString();
//            }
//            IASTInitializerClause[] args = expression.getArguments();

            MethodFinder finder = new MethodFinder(owner);
            try {
                ICommonFunctionNode refferedNode = finder.find(expression);

                if (refferedNode != null) {
                    FunctionCallDependency d = new FunctionCallDependency(owner, refferedNode);
                    if (!owner.getDependencies().contains(d)
                            && !refferedNode.getDependencies().contains(d)) {
                        d.appendExpr(expression);
                        owner.getDependencies().add(d);
                        refferedNode.getDependencies().add(d);
                        logger.debug("Found a function call dependency: " + d.toString());
                    } else {
                        int index = owner.getDependencies().indexOf(d);
                        if (index > 0) {
                            d = (FunctionCallDependency) owner.getDependencies().get(index);
                            d.appendExpr(expression);
                        }
                        index = refferedNode.getDependencies().indexOf(d);
                        if (index > 0) {
                            d = (FunctionCallDependency) refferedNode.getDependencies().get(index);
                            d.appendExpr(expression);
                        }
                    }
                } else {
                    //logger.debug("expression " + expression.getFunctionNameExpression().getRawSignature() + " in " + owner.getAbsolutePath() + " is not a function!");
                }
            } catch (Exception e) {
//                e.printStackTrace();
            }
        }
    }

//    private String getFunctionName(IASTExpression ex, IFunctionNode owner) {
//        String funcName = null;
//        if (ex instanceof IASTFieldReference) {
//            String type = new TypeResolver(owner).exec(((IASTFieldReference) ex).getFieldOwner());
//            funcName = type.replaceAll("[ *]", "") + "::" + ((IASTFieldReference) ex).getFieldName().toString();
//
//            VariableSearchingSpace space = new VariableSearchingSpace(owner);
//            INode typeNode = space.search(type, new StructurevsTypedefCondition());
//            if (typeNode instanceof StructureNode) {
//                INode parent = typeNode.getParent();
//                // TODO: structure define in another file
//                while (parent != null) {
//                    // structure define in a namespace
//                    if (parent instanceof NamespaceNode) {
//                        String namespace = parent.getName();
//                        funcName = namespace + "::" + funcName;
//                    }
//                    parent = parent.getParent();
//                }
//            }
////                IType type = ((IASTFieldReference) ex).getFieldOwner().getExpressionType();
////                funcName = type.toString() + "::" + ((IASTFieldReference) ex).getFieldName().toString();
////
////                if (type instanceof CPPClassType) {
////                    IASTNode parent = ((CPPClassType) type).getDefinition().getParent();
////                    // TODO: structure define in another file
////                    while (parent != null) {
////                        // structure define in a namespace
////                        if (parent instanceof ICPPASTNamespaceDefinition) {
////                            String namespace = ((ICPPASTNamespaceDefinition) parent).getName().getRawSignature();
////                            funcName = namespace + "::" + funcName;
////                        }
////                        parent = parent.getParent();
////                    }
////                }
//        } else if (ex instanceof IASTIdExpression) {
//            funcName = ((IASTIdExpression) ex).getName().toString();
//        }
//
//        return funcName;
//    }

    private void checkUnExpectedList(FunctionCallParser visitor, IFunctionNode owner) {
        List<IASTSimpleDeclaration> names = visitor.getUnexpectedCalledFunctions();

        for (IASTSimpleDeclaration name : names) {

            String funcName = name.getChildren()[0].getRawSignature();

            int nParameters = name.getChildren().length - 1; // ignore the first children because it is corresponding to the name of the alled method

            MethodFinder finder = new MethodFinder(owner);
            try {
                List<ICommonFunctionNode> refferedNodes = finder.find(funcName, nParameters);
                for (ICommonFunctionNode refferedNode : refferedNodes) {
                    FunctionCallDependency d = new FunctionCallDependency(owner, refferedNode);
                    if (!owner.getDependencies().contains(d)
                            && !refferedNode.getDependencies().contains(d)) {
                        owner.getDependencies().add(d);
                        refferedNode.getDependencies().add(d);
                        logger.debug("Found an extended dependency: " + d.toString());
                    }
                }
            } catch (Exception e) {
                logger.debug("function " + funcName + " in " + owner.getAbsolutePath() + " can't found!");
            }
        }
    }
}

