package parser.dependency;

import parser.object.INode;

public abstract class AbstractDependencyGeneration {

    protected abstract void dependencyGeneration(INode root);
}
