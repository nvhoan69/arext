package parser.dependency;

import org.eclipse.cdt.core.dom.ast.IASTFunctionCallExpression;
import org.eclipse.cdt.core.dom.ast.IASTInitializerClause;
import parser.object.ICommonFunctionNode;
import parser.object.INode;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class FunctionCallDependency extends Dependency {

    private final List<IASTFunctionCallExpression> exprs = new ArrayList<>();

    public FunctionCallDependency(INode startArrow, INode endArrow) {
        super(startArrow, endArrow);
    }

    @Override
    public ICommonFunctionNode getEndArrow() {
        return (ICommonFunctionNode) super.getEndArrow();
    }

    @Override
    public ICommonFunctionNode getStartArrow() {
        return (ICommonFunctionNode) super.getStartArrow();
    }

    public boolean fromNode(INode func) {
        return this.startArrow == func;
    }

    public boolean toNode(INode func) {
        return this.endArrow == func;
    }

    public List<IASTInitializerClause[]> getExprArguments() {
        return exprs.stream()
                .map(IASTFunctionCallExpression::getArguments)
                .collect(Collectors.toList());
    }

    public List<IASTFunctionCallExpression> getExprs() {
        return exprs;
    }

    public List<IASTFunctionCallExpression> appendExpr(IASTFunctionCallExpression expr) {
        if (!exprs.contains(expr))
            exprs.add(expr);
        return exprs;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof FunctionCallDependency) {
            FunctionCallDependency dependency = (FunctionCallDependency) obj;
            return fromNode(dependency.getStartArrow()) && toNode(dependency.getEndArrow());
        }

        return false;
    }
}
