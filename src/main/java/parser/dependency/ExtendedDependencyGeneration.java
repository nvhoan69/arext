package parser.dependency;

import parser.object.AvailableTypeNode;
import parser.object.INode;
import parser.object.StructOrClassNode;
import parser.object.VariableNode;
import util.VFPLogger;

import java.util.List;

public class ExtendedDependencyGeneration extends AbstractDependencyGeneration {
    final static VFPLogger logger = VFPLogger.get(ExtendedDependencyGeneration.class);

    public ExtendedDependencyGeneration() {
    }


    public void dependencyGeneration(INode n) {
        if (n instanceof StructOrClassNode) {
            // if the current node is has ability to inherit and it is never analyzed extended dependency before
            List<String> extendClassNames = ((StructOrClassNode) n).getExtendedNames();
            if (extendClassNames != null)
                for (String extendClassName : extendClassNames) {
                    /*
                     * Create temporary variable
                     */
                    VariableNode v = new VariableNode();
                    v.setCoreType(extendClassName);
                    v.setRawType(extendClassName);
                    v.setName(extendClassName);
                    v.setParent(n);
                    /*
                     * Find type of temporary variable
                     */
                    CTypeDependencyGeneration typeGen;
                    try {
                        typeGen = new CTypeDependencyGeneration();
                        typeGen.setAddToTreeAutomatically(false);// because the variable node is fake, we can not update the tree
                        // if we found any type dependencies
                        typeGen.dependencyGeneration(v);

                        INode correspondingNode = typeGen.getCorrespondingNode();
                        if (correspondingNode != null && !(correspondingNode instanceof AvailableTypeNode)) {
                            INode refferedNode = correspondingNode;

                            ExtendDependency d = new ExtendDependency(n, refferedNode);

                            if (!n.getDependencies().contains(d)) {
                                n.getDependencies().add(d);
                                refferedNode.getDependencies().add(d);
                                logger.debug("Found an extended dependency: " + d.toString());
                            }
                        }
                    } catch (Exception e) {
//                        e.printStackTrace();
                    }
                }
        }
    }
}
