package parser.dependency.finder;


import parser.object.IFunctionNode;
import parser.object.INode;
import search.Search;
import search.condition.GlobalVariableNodeCondition;

import java.util.List;

/**
 * Find variable by name
 *
 *
 */
public class VariableFinder {

    /**
     * Node in the structure that contains the searched variable
     */
    private IFunctionNode context;

    public VariableFinder(IFunctionNode context) {
        this.context = context;
    }

    public INode find(String variableName) {
        List<Level> spaces = new VariableSearchingSpace(context).generateExtendSpaces();
        for (Level l : spaces)
            for (INode n : l) {
                List<INode> completeVariables = Search.searchNodes(n, new GlobalVariableNodeCondition());
                for (INode variable : completeVariables)
                    if (variable.getName().equals(variableName)) {
                        return variable;
                    }
            }

        return null;
    }


}
