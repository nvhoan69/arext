package parser;

import environment.Environment;
import parser.dependency.*;
import parser.object.*;
import search.Search;
import search.SearchCondition;
import search.condition.*;
import util.VFPLogger;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ProjectParser {
    final static VFPLogger logger = VFPLogger.get(ProjectParser.class);

    protected File projectPath;
    protected List<File> ignoreFolders = new ArrayList<>();
    private ProjectNode root;

    // always true
    private static final boolean expandTreeuptoMethodLevel_enabled = false;
    private static final boolean cpptoHeaderDependencyGeneration_enabled = false;
    private static final boolean parentReconstructor_enabled = false;
    private static final boolean extendedDependencyGeneration_enabled = false;
    private static final boolean generateSetterandGetter_enabled = false;
    private static final boolean funcCallDependencyGeneration_enabled = false;
    private static final boolean globalVarDependencyGeneration_enabled = false;
    private static final boolean sizeOfDependencyGeneration_enabled = false;
    private static final boolean typeDependency_enable = false;
	private static final boolean typedefDependency_enable = false;

	public ProjectParser(ProjectNode root) {
		this.setRoot(root);
	}

	public ProjectParser(File projectPath) {
		this.projectPath = projectPath;
		ProjectLoader loader = new ProjectLoader();
		loader.setIgnoreFolders(getIgnoreFolders());
		logger.debug("Ignore folders: " + getIgnoreFolders());

		logger.debug("Loading the project " + projectPath.getAbsolutePath());
		setRoot(loader.load(projectPath));
		logger.debug("Loaded the project " + projectPath.getAbsolutePath() + " successfully");
	}

	public ProjectParser(IProjectNode root) {
		this.projectPath = new File(root.getAbsolutePath());
	}

	public static void main(String[] args) {
		ProjectParser projectParser = new ProjectParser(new File("T:\\IdeaProjects\\arext\\dataset\\regex_examples"));

		ProjectNode projectRoot = projectParser.getRootTree();
		Environment.getInstance().setProjectNode(projectRoot);
	}

	public ProjectNode getRootTree() {
		if (root != null) {
			List<INode> sourcecodeFileNodes = Search.searchNodes(root, new SourcecodeFileNodeCondition());

			// STEP
			if (expandTreeuptoMethodLevel_enabled)
				try {
					logger.debug("calling expandTreeuptoMethodLevel");
					for (INode sourceCodeNode : sourcecodeFileNodes)
						if (sourceCodeNode instanceof ISourcecodeFileNode) {
							try {
								ISourcecodeFileNode fNode = (ISourcecodeFileNode) sourceCodeNode;
								File dir = fNode.getFile();

								// if the source code file is not expanded to method level
								if (dir != null && !((ISourcecodeFileNode) sourceCodeNode).isExpandedToMethodLevelState()) {
									SourcecodeFileParser cppParser = new SourcecodeFileParser();
									cppParser.setSourcecodeNode((ISourcecodeFileNode) sourceCodeNode);
									INode sourcecodeTreeRoot = cppParser.generateTree();
									fNode.setAST(cppParser.getTranslationUnit());

									if (sourcecodeTreeRoot != null && sourcecodeTreeRoot.getChildren() != null)
										for (Node sourcecodeItem : sourcecodeTreeRoot.getChildren()) {
											List<INode> nodes = Search.searchNodes(sourceCodeNode, new NodeCondition(), sourcecodeItem.getAbsolutePath());
											if (nodes.isEmpty()) {
												sourceCodeNode.getChildren().add(sourcecodeItem);
												sourcecodeItem.setParent(sourceCodeNode);
											} else {
												nodes.removeIf(node -> !node.getClass().isInstance(sourcecodeItem));
												if (nodes.isEmpty()) {
													sourceCodeNode.getChildren().add(sourcecodeItem);
													sourcecodeItem.setParent(sourceCodeNode);
												}
											}
										}
								}
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
				} catch (Exception ex) {
					ex.printStackTrace();
				}

			// STEP
			if (cpptoHeaderDependencyGeneration_enabled) {
				logger.debug("calling CpptoHeaderDependencyGeneration");

				for (INode cppFileNode : sourcecodeFileNodes)
					try {
						new IncludeHeaderDependencyGeneration().dependencyGeneration(cppFileNode);
					} catch (Exception e) {
						e.printStackTrace();
					}
			}

			// STEP
			if (extendedDependencyGeneration_enabled) {
				logger.debug("calling ExtendedDependencyGeneration");
				List<SearchCondition> conditions = new ArrayList<>();
				conditions.add(new ClassNodeCondition());
				conditions.add(new StructNodeCondition());
//				conditions.add(new NamespaceNodeCondition());
				List<INode> nodes = Search.searchNodes(root, conditions);
				nodes = nodes.stream().filter(node -> !(node.getParent() instanceof ClassNode))
						.collect(Collectors.toList()); // why?
				for (INode node : nodes)
					try {
						new ExtendedDependencyGeneration().dependencyGeneration(node);
					} catch (Exception e) {
						e.printStackTrace();
					}
			}

			// STEP
			if (parentReconstructor_enabled) {
				logger.debug("calling ParentReconstructor");
				List<INode> functionNodes = Search.searchNodes(root, new AbstractFunctionNodeCondition());
				for (INode function : functionNodes)
					if (function instanceof AbstractFunctionNode)
						try {
							new RealParentDependencyGeneration().dependencyGeneration(function);
						} catch (Exception e) {
							e.printStackTrace();
						}
			}

			// STEP
			if (generateSetterandGetter_enabled) {
				logger.debug("calling generateSetterandGetter");
				List<INode> variableNodes = Search.searchNodes(root, new VariableNodeCondition());

				for (INode var : variableNodes)
					if (var instanceof AttributeOfStructureVariableNode) {
						try {
							new SetterGetterDependencyGeneration().dependencyGeneration(var);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
			}

			// STEP
			List<INode> functionNodes = Search.searchNodes(root, new FunctionNodeCondition());
			if (funcCallDependencyGeneration_enabled) {
				logger.debug("calling funcCallDependencyGeneration");
				for (INode node : functionNodes) {
					if (node instanceof IFunctionNode) {
						try {
							logger.debug("Analyzing function " + node.getAbsolutePath());
							new FunctionCallDependencyGeneration().dependencyGeneration(node);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
			}

			// STEP
			if (globalVarDependencyGeneration_enabled){
				logger.debug("calling globalVarDependencyGeneration");
				for (INode node : functionNodes) {
					if (node instanceof IFunctionNode)
						try {
							new GlobalVariableDependencyGeneration().dependencyGeneration(node);
						} catch (Exception e) {
							e.printStackTrace();
						}
				}
			}

			// STEP
			// Find dependency between arguments in a function: a pointer/array and its size
			if (sizeOfDependencyGeneration_enabled) {
				logger.debug("calling sizeDependencyGeneration");
				for (INode node : functionNodes) {
					if (node instanceof IFunctionNode && !(node instanceof ConstructorNode))
						try {
							new SizeOfArrayDepencencyGeneration().dependencyGeneration(node);
						} catch (Exception e) {
							e.printStackTrace();
						}
				}
			}

			// STEP
			if (typeDependency_enable){
				logger.debug("calling typeDpendency");
				for (INode node : functionNodes) {
					if (node instanceof IFunctionNode)
						for (IVariableNode var: ((IFunctionNode) node).getArguments())
						try {
							CTypeDependencyGeneration gen = new CTypeDependencyGeneration();
							gen.setAddToTreeAutomatically(true);
							gen.dependencyGeneration(var);
						} catch (Exception e) {
							e.printStackTrace();
						}
				}
			}

			if (typedefDependency_enable){
				logger.debug("calling typedefDependency");

				List<SearchCondition> conditions = new ArrayList<>();
				conditions.add(new EnumNodeCondition());
				conditions.add(new StructNodeCondition());
				conditions.add(new UnionNodeCondition());
				List<INode> structures = Search.searchNodes(root, conditions);

				List<INode> allTypedefs = Search.searchNodes(root, new TypedefNodeCondition());
				TypedefDependencyGeneration gen = new TypedefDependencyGeneration(allTypedefs);

				for (INode node : structures) {
					gen.dependencyGeneration(node);
				}
			}
		}

        return root;
    }

	public ProjectNode getRoot() {
		return root;
	}

	public void setRoot(ProjectNode root) {
		this.root = root;
	}

	public File getProjectPath() {
		return projectPath;
	}

	public void setProjectPath(File projectPath) {
		this.projectPath = projectPath;
	}

	public List<File> getIgnoreFolders() {
		return ignoreFolders;
	}

	public void setIgnoreFolders(List<File> ignoreFolders) {
		this.ignoreFolders = ignoreFolders;
	}
}