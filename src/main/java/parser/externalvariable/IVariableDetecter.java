package parser.externalvariable;

import parser.object.IFunctionNode;
import parser.object.IVariableNode;
import search.ISearch;

import java.util.List;

/**
 * Find all external variables of a function
 *
 *
 */
public interface IVariableDetecter extends ISearch {
    /**
     * Find external variables of a function
     *
     * @return
     */
    List<IVariableNode> findVariables();

    IFunctionNode getFunction();

    void setFunction(IFunctionNode function);
}