package parser.normalizer;

import parser.object.IFunctionNode;

/**
 * Ex:"while (a[c] != '\0'){"--------------->"while (a[c] != 0){"
 *
 *
 */
public class EndStringNormalizer extends AbstractFunctionNormalizer implements IFunctionNormalizer {
    public EndStringNormalizer() {

    }

    public EndStringNormalizer(IFunctionNode functionNode) {
        this.functionNode = functionNode;
    }

    @Override
    public void normalize() {
        String content = functionNode.getAST().getRawSignature();
        normalizeSourcecode = content.replace("'\\0'", "0");
    }
}
