package parser.normalizer;

import parser.object.IFunctionNode;

/**
 * Convert nullptr to NULL <br/>
 * Ex:
 * <p>
 * <p>
 * <pre>
 * int NullPtrTest(int *x){
 * if (x == nullptr)
 * return -1;
 * else
 * return *x;
 * }
 * </pre>
 *
 *
 */
public class NullPtrNormalizer extends AbstractFunctionNormalizer implements IFunctionNormalizer {
    public NullPtrNormalizer() {

    }

    public NullPtrNormalizer(IFunctionNode functionNode) {
        this.functionNode = functionNode;
    }

    @Override
    public void normalize() {
        String content = functionNode.getAST().getRawSignature();
        normalizeSourcecode = content.replaceAll("\\bnullptr\\b", "NULL");
    }
}
