package parser.normalizer;

import auto_testcase_generation.testdatagen.structuregen.ChangedToken;
import auto_testcase_generation.testdatagen.structuregen.ChangedTokens;
import parser.PreprocessorParser;
import parser.object.FunctionStyleMacroDefinitionNode;
import parser.object.IFunctionNode;
import parser.object.MacroDefinitionNode;
import parser.object.PreprocessorMacroDefinitionNode;
import util.Utils;

import java.util.List;

/**
 * Convert macro-code in the function into its corresponding values. <br/>
 * Ex:
 * <p>
 * <p>
 * <pre>
 * #define MY_DEFAULT_VALUE 10
 *
 * int SimpleMarcoTest(int x){
 * if (x == MY_DEFAULT_VALUE)
 * return 1;
 * else
 * return 0;
 * }
 * </pre>
 * <p>
 * is converted into
 * <p>
 * <p>
 * <pre>
 * #define MY_DEFAULT_VALUE 10
 *
 * int SimpleMarcoTest(int x){
 * if (x == <b>10</b>)
 * return 1;
 * else
 * return 0;
 * }
 * </pre>
 *
 * 
 */
@Deprecated
public class MacroNormalizer extends AbstractFunctionNormalizer implements IFunctionNormalizer {

    public MacroNormalizer() {
    }

    public MacroNormalizer(IFunctionNode functionNode) {
        this.functionNode = functionNode;
    }

    @Override
    public void normalize() {
        PreprocessorParser preprocessor = new PreprocessorParser(functionNode);

        try {
            List<PreprocessorMacroDefinitionNode> macros = preprocessor.getAllPreprocessors();
            String content = functionNode.getAST().getRawSignature();
            for (PreprocessorMacroDefinitionNode macro : macros)
                if (macro instanceof MacroDefinitionNode)
                    content = parseMacroDefinition(tokens, macro, content);
                else if (macro instanceof FunctionStyleMacroDefinitionNode)
                    content = parseFunctionStyleMacroDefinition(tokens, macro, content);
            normalizeSourcecode = content;
        } catch (Exception e) {
            e.printStackTrace();
            normalizeSourcecode = "";
        }
    }

    /**
     * Ex: #define ADD(a, b) ((a) + (b))
     *
     * @param changedTokens
     * @param macro
     * @param content
     * @return
     */
    private String parseFunctionStyleMacroDefinition(ChangedTokens changedTokens, PreprocessorMacroDefinitionNode macro,
                                                     String content) {
        String normalizedContent = content;
        if (macro instanceof FunctionStyleMacroDefinitionNode) {
            FunctionStyleMacroDefinitionNode functionMacro = (FunctionStyleMacroDefinitionNode) macro;
            functionMacro.getAST();

        }
        return normalizedContent;
    }

    /**
     * Ex: #define MY_DEFAULT_VALUE 10
     *
     * @param changedTokens
     * @param macro
     * @param content
     * @return
     */
    private String parseMacroDefinition(ChangedTokens changedTokens, PreprocessorMacroDefinitionNode macro,
                                        String content) {
        String normalizedContent = content;
        if (macro instanceof MacroDefinitionNode) {
            MacroDefinitionNode castMacro = (MacroDefinitionNode) macro;

            String oldValueRegex = "\\b" + castMacro.getNewType() + "\\b";
            if (Utils.containRegex(normalizedContent, oldValueRegex)) {
                String newValue = castMacro.getValue();
                String oldValue = castMacro.getNewType();
                changedTokens.add(new ChangedToken(newValue, oldValue));

                normalizedContent = normalizedContent.replaceAll(oldValueRegex, castMacro.getValue());
            }
        }
        return normalizedContent;
    }
}
