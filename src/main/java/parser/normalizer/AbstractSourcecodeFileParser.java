package parser.normalizer;

import org.eclipse.cdt.core.dom.ast.IASTTranslationUnit;
import parser.object.ISourcecodeFileNode;

import java.io.File;

/**
 * Abstract class for parsing source code file, e.g., *.c, *.cpp, *.h
 *
 *
 */
public abstract class AbstractSourcecodeFileParser extends AbstractParser {

    protected ISourcecodeFileNode sourcecodeNode;

    protected IASTTranslationUnit translationUnit;

    public IASTTranslationUnit getTranslationUnit() {
        return translationUnit;
    }

    public void setTranslationUnit(IASTTranslationUnit translationUnit) {
        this.translationUnit = translationUnit;
    }

    public File getSourcecodeFile() {
        return new File(sourcecodeNode.getAbsolutePath());
    }

    public void setSourcecodeFile(File sourcecodeFile) {
        // nothing to do
    }

    public ISourcecodeFileNode getSourcecodeNode() {
        return sourcecodeNode;
    }

    public void setSourcecodeNode(ISourcecodeFileNode sourcecodeNode) {
        this.sourcecodeNode = sourcecodeNode;
    }
}
