package parser.normalizer;

import auto_testcase_generation.testdatagen.structuregen.ChangedToken;
import org.eclipse.cdt.core.dom.ast.IASTInitializer;
import parser.dependency.finder.IVariableSearchingSpace;
import parser.dependency.finder.Level;
import parser.dependency.finder.VariableSearchingSpace;
import parser.object.IFunctionNode;
import parser.object.INode;
import parser.object.VariableNode;
import search.Search;
import search.condition.ExternVariableNodeCondition;
import search.condition.VariableNodeCondition;
import util.Utils;

import java.util.List;

/**
 * Replace extern variables with its value
 *
 * 
 */
public class ExternNormalizer extends AbstractFunctionNormalizer implements IFunctionNormalizer {

    public ExternNormalizer() {
    }

    public ExternNormalizer(IFunctionNode functionNode) {
        this.functionNode = functionNode;
    }

    @Override
    public void normalize() {
        if (functionNode != null) {
            normalizeSourcecode = functionNode.getAST().getRawSignature();

            IVariableSearchingSpace space = new VariableSearchingSpace(functionNode);

            for (Level level : space.getSpaces())
                for (INode n : level) {

                    List<INode> externsVariables = Search.searchNodes(n, new ExternVariableNodeCondition());

                    for (INode externsVariable : externsVariables)
                        if (externsVariable instanceof VariableNode) {
                            /*
                              Convert value of extern variable into
                              corresponding value
                             */
                            String oldName = externsVariable.getNewType();
                            String newName = getValueOfExternalVariable(oldName, Utils.getRoot(n));

                            normalizeSourcecode = normalizeSourcecode.replaceAll("\\b" + oldName + "\\b", newName);

                            tokens.add(new ChangedToken(newName, oldName));
                        }
                }
        } else
            normalizeSourcecode = INormalizer.ERROR;
    }

    /**
     * Get initializer of an extern variable in the current project
     *
     * @param nameExternalVariable
     * @param root
     * @return
     */
    private String getValueOfExternalVariable(String nameExternalVariable, INode root) {
        String value = "";

        List<INode> nodes = Search.searchNodes(root, new VariableNodeCondition());

        for (INode node : nodes)
            if (node instanceof VariableNode) {

                VariableNode n = (VariableNode) node;
                if (!n.isExtern() && n.getNewType().equals(nameExternalVariable)) {
                    IASTInitializer initializer = n.getInitializer();

                    if (initializer != null)
                        return initializer.getChildren()[0].getRawSignature();
                }
            }
        return value;
    }
}
