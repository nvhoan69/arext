package parser.normalizer;

/**
 * Normalize source code file
 *
 *
 */
public interface ISourcecodeFileNormalizer extends ISourceCodeNormalizer {
    @Override
    default boolean shouldWriteToFile() {
        return true;
    }
}
