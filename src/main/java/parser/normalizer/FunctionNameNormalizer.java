package parser.normalizer;

import util.SpecialCharacter;

/**
 * Ex1: "int test(Student a)"----------------> "int ClassA::test(Student a)
 * <br/>
 * <p>
 * Ex2: "int test2(Apple a)"----------------> "int
 * Namespace1::Namespace2:ClassB::test2(Apple a) <br/>
 *
 *
 */
public class FunctionNameNormalizer extends AbstractFunctionNormalizer implements IFunctionNormalizer {

    @Override
    public void normalize() {
        normalizeSourcecode = getFunctionNode().getAST().getRawSignature();
        String prefixPathofFunction = getFunctionNode().getLogicPathFromTopLevel();
        if (prefixPathofFunction.length() > 0)
            if (prefixPathofFunction.startsWith(SpecialCharacter.STRUCTURE_OR_NAMESPACE_ACCESS))
                normalizeSourcecode = normalizeSourcecode.replace(getFunctionNode().getSimpleName(),
                        getFunctionNode().getLogicPathFromTopLevel() + getFunctionNode().getSimpleName());
            else {
                String newName = getFunctionNode().getLogicPathFromTopLevel()
                        + SpecialCharacter.STRUCTURE_OR_NAMESPACE_ACCESS + getFunctionNode().getSingleSimpleName();
                normalizeSourcecode = normalizeSourcecode.replace(getFunctionNode().getSimpleName(), newName);
            }

    }
}
