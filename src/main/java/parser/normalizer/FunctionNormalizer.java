package parser.normalizer;

import auto_testcase_generation.testdatagen.AbstractAutomatedTestdataGeneration;
import auto_testcase_generation.testdatagen.structuregen.ChangedTokens;
import org.eclipse.cdt.core.dom.ast.IASTFunctionDefinition;
import org.eclipse.cdt.core.dom.ast.IASTNode;
import org.eclipse.cdt.internal.core.dom.parser.cpp.CPPASTProblemDeclaration;
import parser.object.IFunctionNode;
import util.Utils;
import util.VFPLogger;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

//import com.ibm.icu.util.Calendar;

/**
 * Use some normalizers to make the function to be standard
 *
 * 
 */
public class FunctionNormalizer extends AbstractFunctionNormalizer implements IFunctionNormalizer {
	final static VFPLogger logger = VFPLogger.get(FunctionNormalizer.class);
	private List<AbstractFunctionNormalizer> normalizers = new ArrayList<>();

	public FunctionNormalizer() {
	}

	public FunctionNormalizer(IFunctionNode functionNode, List<AbstractFunctionNormalizer> normalizers) {
		this.functionNode = functionNode;
		this.normalizers = normalizers;
	}

	@Override
	public void normalize() {
		Date startTime = Calendar.getInstance().getTime();

		IFunctionNode clone = (IFunctionNode) functionNode.clone();

		for (AbstractFunctionNormalizer normalizer : normalizers) {
			// logger.debug(normalizer.getClass().getSimpleName() + "...");
			normalizer.setFunctionNode(clone);
			normalizer.normalize();
			String newSourceCode = normalizer.getNormalizedSourcecode();

			try {
				IASTNode node = Utils.getIASTTranslationUnitforCpp(newSourceCode.toCharArray()).getChildren()[0];

				if (node instanceof CPPASTProblemDeclaration)
					node = Utils.getIASTTranslationUnitforC(newSourceCode.toCharArray()).getChildren()[0];

				if (node instanceof IASTFunctionDefinition) {
					IASTFunctionDefinition newAST = (IASTFunctionDefinition) node;
					clone.setAST(newAST);
				} else
					logger.error("Can not convert \n" + newSourceCode + "\n to AST");

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		normalizeSourcecode = clone.getAST().getRawSignature();

		Date end = Calendar.getInstance().getTime();
		AbstractAutomatedTestdataGeneration.normalizationTime += end.getTime() - startTime.getTime();
	}

	@Override
	public ChangedTokens getTokens() {
		ChangedTokens mappingVars = new ChangedTokens();
		for (AbstractNormalizer normalizer : normalizers)
			mappingVars.addAll(normalizer.getTokens());
		return mappingVars;
	}

	public List<AbstractFunctionNormalizer> getNormalizers() {
		return normalizers;
	}

	public void setNormalizers(List<AbstractFunctionNormalizer> normalizers) {
		this.normalizers = normalizers;
	}

	public void addNormalizer(AbstractFunctionNormalizer normalizer) {
		normalizers.add(normalizer);
	}

}
