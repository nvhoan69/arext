package parser.normalizer;

import parser.object.IFunctionNode;

/**
 * Ex:"if (status != EXIT_SUCCESS) emit_try_help ();" --->"if (status != 0) emit_try_help ();"
 *
 *
 */
public class ConstantNormalizer extends AbstractFunctionNormalizer implements IFunctionNormalizer {
    public ConstantNormalizer() {

    }

    public ConstantNormalizer(IFunctionNode functionNode) {
        this.functionNode = functionNode;
    }

    @Override
    public void normalize() {
        String content = functionNode.getAST().getRawSignature();
        normalizeSourcecode = content.replace("EXIT_SUCCESS", "0");
    }
}
