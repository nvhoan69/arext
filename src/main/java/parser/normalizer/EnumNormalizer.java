package parser.normalizer;

import auto_testcase_generation.testdatagen.structuregen.ChangedToken;
import auto_testcase_generation.testdatagen.testdatainit.VariableTypes;
import auto_testcase_generation.testdatagen.testdatainit.VariableTypes.BASIC.NUMBER.INTEGER;
import parser.dependency.finder.IVariableSearchingSpace;
import parser.dependency.finder.Level;
import parser.dependency.finder.VariableSearchingSpace;
import parser.object.EnumNode;
import parser.object.IFunctionNode;
import parser.object.INode;
import parser.object.IVariableNode;
import search.Search;
import search.condition.EnumNodeCondition;
import util.Utils;
import util.VFPLogger;

import java.util.List;

/**
 * Convert enum value in the function into another representation
 *
 * 
 */
public class EnumNormalizer extends AbstractFunctionNormalizer implements IFunctionNormalizer {
	final static VFPLogger logger = VFPLogger.get(EnumNormalizer.class);
	public static int DEFAULT_ID_TOKEN = 672;
	public static int ID_TOKEN = EnumNormalizer.DEFAULT_ID_TOKEN;

	public EnumNormalizer() {
	}

	public EnumNormalizer(IFunctionNode functionNode) {
		this.functionNode = functionNode;
	}

	@Override
	public void normalize() {
		if (functionNode != null) {
			EnumNormalizer.ID_TOKEN = EnumNormalizer.DEFAULT_ID_TOKEN;

			normalizeSourcecode = functionNode.getAST().getRawSignature();

			IVariableSearchingSpace space = new VariableSearchingSpace(functionNode);

			for (Level level : space.getSpaces())
				for (INode n : level) {
					// logger.debug("Parse " + n.getAbsolutePath());

					List<INode> enums = Search.searchNodes(n, new EnumNodeCondition());

					for (INode enumItem : enums) {
						if (enumItem instanceof EnumNode) {
							/*
							  Increase id of token to prevent the restore test data to make mistakes
							 */
							EnumNormalizer.ID_TOKEN += 100;
							/*
							  Convert value of item in enum into interger value
							 */
							for (String value : ((EnumNode) enumItem).getAllNameEnumItems()) {
								String newName = EnumNormalizer.ID_TOKEN++ + "";

								String oldName = value;
								normalizeSourcecode = normalizeSourcecode.replaceAll("\\b" + oldName + "\\b", newName);

								String regex = "return\\s*\\b" + newName + "\\b";
								normalizeSourcecode = normalizeSourcecode.replaceAll(regex, "return " + oldName);
								tokens.add(new ChangedToken(newName, oldName));
							}
							/*
							 * Convert type enum into a specified number type
							 */
							List<String> basicNumberTypes = VariableTypes.getAllBasicFieldNames(INTEGER.class);
							basicNumberTypes.remove("bool");

							for (String basicNumberType : basicNumberTypes)
								/*
								 * Our aim is to replace the type of enum in the function with a name of integer
								 * type (e.g., int, long).
								 *
								 * Therefore, we need check the replaced integer type to be in the function or
								 * not. And also, this type must be unique because of the function may access
								 * multiple enums.
								 */
								if (!normalizeSourcecode.contains(basicNumberType)
										&& !tokens.containNewName(basicNumberType)) {
									String ALTERNATIVE_TYPE = basicNumberType;
									for (IVariableNode var : getFunctionNode().getArguments())
										if (enumItem.getNewType().length() > 0
												&& var.getReducedRawType().endsWith(enumItem.getNewType())) {
											normalizeSourcecode = normalizeSourcecode
													.replaceAll(Utils.toRegex(var.getFullType()), ALTERNATIVE_TYPE);
											tokens.add(new ChangedToken(ALTERNATIVE_TYPE, var.getFullType()));
										}
									break;
								}

						}
					}
				}
		} else
			normalizeSourcecode = INormalizer.ERROR;
	}
}
