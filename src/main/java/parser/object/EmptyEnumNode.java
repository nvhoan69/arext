package parser.object;

/**
 * Represent enum declaration
 * <p>
 * <p>
 * 
 * <pre>
 * |    enum Color;
 * </pre>
 *
 *
 */
public class EmptyEnumNode extends EnumNode implements IEmptyStructureNode {
}
