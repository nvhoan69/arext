package parser.object;

import java.util.ArrayList;
import java.util.List;

/**
 * Represent a constructor
 *
 * 
 */
public class ConstructorNode extends AbstractFunctionNode {


    @Override
    public List<IVariableNode> getArguments() {
        if (this.arguments == null || this.arguments.size() == 0) {
            this.arguments = new ArrayList<>();
            for (INode child : getChildren())
                if (child instanceof VariableNode) {
                    this.arguments.add((IVariableNode) child);
                }
        }
        return this.arguments;
    }
}
