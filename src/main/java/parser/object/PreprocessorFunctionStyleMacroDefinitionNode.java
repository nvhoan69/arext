package parser.object;

import org.eclipse.cdt.core.dom.ast.IASTPreprocessorFunctionStyleMacroDefinition;

/**
 * Ex: <b>#define min(X, Y) ((X) < (Y) ? (X) : (Y))</b><br/>
 *
 *
 */
public class PreprocessorFunctionStyleMacroDefinitionNode
        extends PreprocessorMacroDefinitionNode<IASTPreprocessorFunctionStyleMacroDefinition> {

}
