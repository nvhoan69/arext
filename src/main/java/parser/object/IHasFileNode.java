package parser.object;

import java.io.File;

/**
 *
 */
public interface IHasFileNode {

    File getFile();
}
