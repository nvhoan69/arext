package parser.object;

import org.eclipse.cdt.core.dom.ast.IASTSimpleDeclaration;
import util.SpecialCharacter;
import util.Utils;

public class FunctionPointerTypeNode extends CustomASTNode<IASTSimpleDeclaration> {

    private String returnType;

    private String[] argumentTypes;

    private String functionName;

    public String getReturnType() {
        return returnType;
    }

    public void setReturnType(String returnType) {
        this.returnType = returnType;
    }

    public String[] getArgumentTypes() {
        return argumentTypes;
    }

    public void setArgumentTypes(String[] argumentTypes) {
        this.argumentTypes = argumentTypes;
    }

    public String getFunctionName() {
        return functionName;
    }

    public void setFunctionName(String functionName) {
        this.functionName = functionName;
    }

    public String generateDefault() {
        StringBuilder builder = new StringBuilder();

        for (String paramType : argumentTypes) {
            builder.append(paramType).append(SpecialCharacter.UNDERSCORE);
        }

        builder.append(returnType);

        return "VFPDefault" + Utils.computeMd5(builder.toString());
    }
}
