package parser.object;

import gui.Configuration;
import gui.controllers.MasterController;

import java.io.File;
import java.util.Random;

public class RegexNode extends Node {
    private String content;
    private String rawContent;
    private String name;
    private String dataFilePath;
    private String targetGraphFilePrefix;

    public RegexNode(String name, String rawContent, String content) {
        this.name = "re." + new Random().nextInt(10000) + "." + name;
        this.rawContent = rawContent;
        this.content = content;
        selfNormalize();

        this.dataFilePath = Configuration.DATA_DIR + File.separator + this.name + ".json";
        this.targetGraphFilePrefix = Configuration.GRAPH_DATA_DIR + File.separator + this.name;
    }

    private void selfNormalize() {
        if (this.content.startsWith("^")) {
            this.content = this.content.substring(1);
        }

        if (this.content.endsWith("$")) {
            this.content = this.content.substring(0, this.content.length() - 1);
        }
    }

    public String getContent() {
        return content;
    }

    public String getRawContent() {
        return rawContent;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDataFilePath() {
        return dataFilePath;
    }

    public String getTargetGraphFilePrefix() {
        return targetGraphFilePrefix;
    }
}
