package parser.object;

import org.eclipse.cdt.internal.core.dom.parser.cpp.CPPASTTranslationUnit;
import parser.dependency.Dependency;
import parser.dependency.IncludeHeaderDependency;
import util.Utils;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CppFileNode extends SourcecodeFileNode<CPPASTTranslationUnit> {
    private String copiedAbsolutePath = null;
    private boolean isCopied = false;

    public CppFileNode() {
        try {
            Icon ICON_CPP = new ImageIcon(Node.class.getResource("/image/node/Soucecode-Cpp.png"));
            setIcon(ICON_CPP);
        } catch (Exception e) {
        }
    }

    public List<Dependency> getIncludeHeaderNodes() {
        List<Dependency> includedDependencies = new ArrayList<>();
        for (Dependency dependency : getDependencies())
            if (dependency instanceof IncludeHeaderDependency)
                includedDependencies.add(dependency);
        return includedDependencies;
    }

    public String getUsedForProcessPath() {
        if (!isCopied) {
            return getAbsolutePath();
        }
        if (copiedAbsolutePath == null) {
            String copyFilePath = getAbsolutePath().substring(0, getAbsolutePath().lastIndexOf(File.separator) + 1);
            copiedAbsolutePath = copyFilePath + "AREXT_copy_" + getName();
        }
        return copiedAbsolutePath;
    }

    /**
     * if makeACopy then the isCopied is true forever, so the getUsedForProcessPath is the copy path
     */
    public void makeACopy() throws IOException {
        if (copiedAbsolutePath == null) {
            String copyFilePath = getAbsolutePath().substring(0, getAbsolutePath().lastIndexOf(File.separator) + 1);
            copiedAbsolutePath = copyFilePath + "AREXT_copy_" + getName();
        }
        Utils.copy(new File(getAbsolutePath()), new File(copiedAbsolutePath));
        isCopied = true;
    }

    public boolean isCopied() {
        return isCopied;
    }
}
