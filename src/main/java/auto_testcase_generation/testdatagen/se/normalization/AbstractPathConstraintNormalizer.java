package auto_testcase_generation.testdatagen.se.normalization;

import auto_testcase_generation.testdatagen.se.memory.IVariableNodeTable;
import parser.normalizer.AbstractNormalizer;

/**
 * Normalize path constraint
 *
 * 
 */
public abstract class AbstractPathConstraintNormalizer extends AbstractNormalizer implements IPathConstraintNormalizer {

    /**
     * Table of variables
     */
    protected IVariableNodeTable tableMapping;

    public IVariableNodeTable getTableMapping() {
        return tableMapping;
    }

    public void setTableMapping(IVariableNodeTable tableMapping) {
        this.tableMapping = tableMapping;
    }

}
