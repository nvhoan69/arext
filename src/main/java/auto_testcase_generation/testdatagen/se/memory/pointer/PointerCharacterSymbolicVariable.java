package auto_testcase_generation.testdatagen.se.memory.pointer;

/**
 * Represent one level pointer
 *
 * 
 */
public class PointerCharacterSymbolicVariable extends PointerSymbolicVariable {
    public PointerCharacterSymbolicVariable(String name, String type, int scopeLevel) {
        super(name, type, scopeLevel);
    }
}
