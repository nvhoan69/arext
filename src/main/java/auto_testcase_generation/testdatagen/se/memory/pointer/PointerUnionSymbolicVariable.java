package auto_testcase_generation.testdatagen.se.memory.pointer;

/**
 * Represent one level pointer
 *
 * 
 */
public class PointerUnionSymbolicVariable extends PointerStructureSymbolicVariable {
    public PointerUnionSymbolicVariable(String name, String type, int scopeLevel) {
        super(name, type, scopeLevel);
    }
}
