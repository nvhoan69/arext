package auto_testcase_generation.testdatagen.se.memory.basic;

/**
 * Represent character variable
 *
 *
 */
public class NumberSymbolicVariable extends BasicSymbolicVariable {

    public NumberSymbolicVariable(String name, String type, int scopeLevel, String value) {
        super(name, type, scopeLevel, value);
    }
}
