package auto_testcase_generation.testdatagen.advanced_pointer;

import java.util.*;

public class TypeMap extends HashMap<String, List<String>> {

    private Map<String, Boolean> functionUsage = new HashMap<>();

    public List<String> append(String key, String value) {
        List<String> values = get(key);

        if (values == null)
            values = new ArrayList<>();

        if (!values.contains(value))
            values.add(value);

        return put(key, values);
    }

    public String removeButKeepOne(String key) {
        List<String> values = get(key);
        if (values == null || values.isEmpty())
            return null;
        else {
            String first = values.get(0);
            remove(key);
            append(key, first);
            return first;
        }
    }

    public List<String> append(String key, List<String> values) {
        List<String> origin = get(key);

        if (values == null)
            values = new ArrayList<>();

        for (String value : values) {
            if (!origin.contains(value))
                origin.add(value);
        }

        return put(key, origin);
    }

    public void putFunctionUsage(String name) {
        functionUsage.put(name, true);
    }

    public boolean isFunctionUsage(String name) {
        return functionUsage.get(name) != null && functionUsage.get(name);
    }

    public void setFunctionUsage(TypeMap other) {
        this.functionUsage = other.functionUsage;
    }

    public Set<String> getAllFunctionUsages() {
        return functionUsage.keySet();
    }
}
