package auto_testcase_generation.cfg.testpath;

/**
 * Represent a normalized test path. The content of all condition nodes reveals
 * its true/false value
 *
 * 
 */
public interface INormalizedTestpath extends ITestpathInCFG {

}
