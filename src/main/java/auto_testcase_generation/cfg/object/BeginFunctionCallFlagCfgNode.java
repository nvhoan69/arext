package auto_testcase_generation.cfg.object;

/**
 * Represent the function call flag node of CFG
 *
 *
 */
public class BeginFunctionCallFlagCfgNode extends FunctionCallFlagCfgNode {

	public BeginFunctionCallFlagCfgNode() {
		setContent(BeginFlagCfgNode.BEGIN_FLAG);
	}
}
