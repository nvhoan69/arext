package auto_testcase_generation.cfg.object;

/**
 * This class prevents two nodes that can not be merged
 *
 *
 */
public class SeparatePointCfgNode extends CfgNode {

    @Override
    public boolean isNormalNode() {
        return false;
    }

    @Override
    public boolean shouldDisplayInCFG() {
        return false;
    }

    @Override
    public boolean shouldInBlock() {
        return false;
    }
}
