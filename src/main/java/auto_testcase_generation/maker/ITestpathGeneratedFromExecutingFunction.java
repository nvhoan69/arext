package auto_testcase_generation.maker;

/**
 * Represent a test path in function from the beginning statement to the end
 * statement
 * 
 *
 *
 */
public interface ITestpathGeneratedFromExecutingFunction {
	String getEncodedTestpath();

	void setEncodedTestpath(String testpath);
}
