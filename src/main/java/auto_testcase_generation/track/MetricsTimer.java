package auto_testcase_generation.track;

import thread.AbstractVFPTask;
import thread.VFPThread;

public class MetricsTimer extends AbstractVFPTask<Long> {

    private long memory = 0;
    private int count = 0;

    private long start, end;

    private static final int TIMEOUT = 200;

    @Override
    protected Long call() throws Exception {
        try {
            while (!isCancelled()) {
                increase();
                Thread.sleep(TIMEOUT);
            }
        } catch (InterruptedException e) {

        }

        return memory / count;
    }

    private void increase() {
        memory += getCurrentUsedMemory();
        count++;
    }

    private long getCurrentUsedMemory() {
//        return ManagementFactory.getMemoryMXBean().getHeapMemoryUsage().getUsed() +
//                ManagementFactory.getMemoryMXBean().getNonHeapMemoryUsage().getUsed();
        return Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
    }

    public long getMemory() {
        return memory / count;
    }

    public long getTime() {
        return end - start;
    }

    public void start() {
        start = System.nanoTime();
        increase();
        new VFPThread(this).start();
    }

    public void stop() {
        end = System.nanoTime();
        cancel();
    }
}
