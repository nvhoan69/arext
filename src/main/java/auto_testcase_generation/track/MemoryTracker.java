package auto_testcase_generation.track;

import parser.object.ICommonFunctionNode;

import java.util.HashMap;
import java.util.Map;

public class MemoryTracker {

    private final Map<ICommonFunctionNode, Long> memory = new HashMap<>();

    private static MemoryTracker instance;

    public static MemoryTracker getInstance() {
        if (instance == null)
            instance = new MemoryTracker();

        return instance;
    }

    public int size() {
        return memory.size();
    }

    public boolean containsKey(ICommonFunctionNode key) {
        return memory.containsKey(key);
    }

    public long get(ICommonFunctionNode key) {
        Long value = memory.get(key);
        if (value == null)
            return 0;
        else
            return value;
    }

    public Long put(ICommonFunctionNode key, Long value) {
        return memory.put(key, value);
    }

    public Long remove(ICommonFunctionNode key) {
        return memory.remove(key);
    }

    public synchronized void append(ICommonFunctionNode key, long time) {
        long sum = time;
        if (containsKey(key)) {
            sum += get(key);
        }
        put(key, sum);
    }

    public void clear() {
        memory.clear();
    }
}
