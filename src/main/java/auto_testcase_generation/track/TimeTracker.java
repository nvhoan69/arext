package auto_testcase_generation.track;

import parser.object.ICommonFunctionNode;

import java.util.HashMap;
import java.util.Map;

public class TimeTracker {

    private final Map<ICommonFunctionNode, Long> executedTime = new HashMap<>();

    private static TimeTracker instance;

    public static TimeTracker getInstance() {
        if (instance == null)
            instance = new TimeTracker();

        return instance;
    }

    public int size() {
        return executedTime.size();
    }

    public boolean containsKey(ICommonFunctionNode key) {
        return executedTime.containsKey(key);
    }

    public long get(ICommonFunctionNode key) {
        Long value = executedTime.get(key);
        if (value == null)
            return 0;
        else
            return value;
    }

    public Long put(ICommonFunctionNode key, Long value) {
        return executedTime.put(key, value);
    }

    public Long remove(ICommonFunctionNode key) {
        return executedTime.remove(key);
    }

    public synchronized void append(ICommonFunctionNode key, long time) {
        long sum = time;
        if (containsKey(key)) {
            sum += get(key);
        }
        put(key, sum);
    }

    public void clear() {
        executedTime.clear();
    }
}
