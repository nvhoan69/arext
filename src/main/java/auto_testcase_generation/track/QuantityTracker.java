package auto_testcase_generation.track;

import parser.object.ICommonFunctionNode;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class QuantityTracker {

    private final Map<ICommonFunctionNode, AtomicInteger> quantity = new HashMap<>();

    private static QuantityTracker instance;

    public static QuantityTracker getInstance() {
        if (instance == null)
            instance = new QuantityTracker();

        return instance;
    }

    public int get(ICommonFunctionNode key) {
        AtomicInteger value = quantity.get(key);

        if (value == null) {
            value = new AtomicInteger();
            quantity.put(key, value);
        }
        return value.get();
    }

    public int increase(ICommonFunctionNode key) {
        AtomicInteger value = quantity.get(key);

        if (value == null) {
            value = new AtomicInteger();
            quantity.put(key, value);
        }

        return value.incrementAndGet();
    }


}
