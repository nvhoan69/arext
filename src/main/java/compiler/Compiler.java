package compiler;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import compiler.message.CompileMessage;
import compiler.message.ICompileMessage;
import environment.Environment;
import parser.object.INode;
import util.*;
import util.VFPLogger;

import java.util.ArrayList;
import java.util.List;

public class Compiler implements ICompiler {
    private final static VFPLogger logger = VFPLogger.get(Compiler.class);

    @Expose
    private String compileCommand = "compile";
    @Expose
    private String linkCommand = "link";
    @Expose
    private String includeFlag = "-I";
    @Expose
    private String defineFlag = "-D";
    @Expose
    private String outputFlag = "-o";
    @Expose
    private String outputExtension = ".out";
    @Expose
    private List<String> includePaths = new ArrayList<>();
    @Expose
    private List<String> defines = new ArrayList<>();

    public Compiler() {

    }

    public String toJson() {
        return new Gson().toJson(this);
    }

    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException {
        Environment.getInstance().setupConfiguration();
        System.out.println();
    }

    public Compiler(Class<?> c) throws IllegalAccessException, NoSuchFieldException {
        compileCommand = c.getField("COMPILE_CMD").get(null).toString();
        linkCommand = c.getField("LINK_CMD").get(null).toString();

        includeFlag = c.getField("INCLUDE_FLAG").get(null).toString();
        defineFlag = c.getField("DEFINE_FLAG").get(null).toString();
        outputFlag = c.getField("OUTPUT_FLAG").get(null).toString();

        outputExtension = c.getField("OUTPUT_EXTENSION").get(null).toString();
    }

    @Override
    public ICompileMessage compile(INode file) {
        String filepath = file.getAbsolutePath();
        filepath = PathUtils.toRelative(filepath);

        return compile(filepath);
    }

    @Override
    public ICompileMessage compile(String filePath) {
        String script = generateCompileCommand(filePath);

        ICompileMessage compileMessage = null;

        try {
            String[] command = CompilerUtils.prepareForTerminal(this, script);
            String message = new Terminal(command).get();
            compileMessage = new CompileMessage(message, filePath);
            compileMessage.setCompilationCommand(script);

            logger.debug("Compilation of " + filePath + ": " + script);
        } catch (Exception ex) {
            logger.error("Error " + ex.getMessage() + " when compiling " + filePath);
        }

        return compileMessage;
    }

    @Override
    public ICompileMessage link(String executableFilePath, String... outputPaths) {
        String script = CompilerUtils.generateLinkCommand(linkCommand, outputFlag, executableFilePath, outputPaths);

        ICompileMessage compileMessage;

        try {
            String[] command = CompilerUtils.prepareForTerminal(this, script);
            String message = new Terminal(command).get();
            compileMessage = new CompileMessage(message, executableFilePath);
            compileMessage.setLinkingCommand(script);
        } catch (Exception ex) {
            logger.error("Can not linkage in " + executableFilePath + " with command " + script);
            compileMessage = null;
        }

        return compileMessage;
    }

    @Override
    public String generateCompileCommand(String filePath) {
        String outfilePath = CompilerUtils.getOutfilePath(filePath, outputExtension);

        StringBuilder builder = new StringBuilder();
        builder.append(compileCommand)
                .append(SpecialCharacter.SPACE)
                .append("\"" + filePath + "\"")
                .append(SpecialCharacter.SPACE);

        if (includePaths != null && includePaths.size() != 0) {
            for (String path : includePaths) {
                builder.append(includeFlag)
                        .append("\"" + path + "\"")
                        .append(SpecialCharacter.SPACE);
            }
        }

        if (defines != null && defines.size() != 0) {
            for (String variable : defines) {
                builder.append(defineFlag)
                        .append(variable)
                        .append(SpecialCharacter.SPACE);
            }
        }

        builder.append(outputFlag)
                .append("\"" + outfilePath + "\"");

        return builder.toString();
    }

    @Override
    public String generateLinkCommand(String executableFilePath, String... outputPaths) {
        if (outputPaths == null || outputPaths.length == 0)
            return null;

        StringBuilder builder = new StringBuilder();

        builder.append(linkCommand)
                .append(SpecialCharacter.SPACE);

        for (String output : outputPaths)
            builder.append("\"" + output + "\"")
                    .append(SpecialCharacter.SPACE);

        builder.append(outputFlag)
                .append("\"" + executableFilePath + "\"");

        return builder.toString();
    }

    @Override
    public String getCompileCommand() {
        return compileCommand;
    }

    @Override
    public String getLinkCommand() {
        return linkCommand;
    }

    @Override
    public List<String> getIncludePaths() {
        return includePaths;
    }

    @Override
    public List<String> getDefines() {
        return defines;
    }

    @Override
    public String getDefineFlag() {
        return defineFlag;
    }

    @Override
    public String getIncludeFlag() {
        return includeFlag;
    }

    @Override
    public String getOutputExtension() {
        return outputExtension;
    }

    @Override
    public String getOutputFlag() {
        return outputFlag;
    }

    public void setCompileCommand(String compileCommand) {
        this.compileCommand = compileCommand;
    }

    public void setLinkCommand(String linkCommand) {
        this.linkCommand = linkCommand;
    }

    public void setIncludePaths(List<String> includePaths) {
        this.includePaths = includePaths;
    }

    public void setDefines(List<String> defines) {
        this.defines = defines;
    }

    public void setDefineFlag(String defineFlag) {
        this.defineFlag = defineFlag;
    }

    public void setIncludeFlag(String includeFlag) {
        this.includeFlag = includeFlag;
    }

    public void setOutputExtension(String outputExtension) {
        this.outputExtension = outputExtension;
    }

    public void setOutputFlag(String outputFlag) {
        this.outputFlag = outputFlag;
    }

    public boolean isGccCommand(){
        return compileCommand.contains("gcc");
    }

    public boolean isGPlusPlusCommand(){
        return compileCommand.contains("g++");
    }
}
