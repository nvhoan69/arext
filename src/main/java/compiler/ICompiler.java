package compiler;

import compiler.message.ICompileMessage;
import parser.object.INode;

import java.util.List;

public interface ICompiler {
    ICompileMessage compile(INode root);

    ICompileMessage compile(String filePath);

    ICompileMessage link(String executableFilePath, String... outputPaths);

    String generateCompileCommand(String filePath);

    // the project path will contain the executable file
    String generateLinkCommand(String executablePath, String... outputPaths);

    String getCompileCommand();

    String getLinkCommand();

    List<String> getIncludePaths();

    List<String> getDefines();

    String getDefineFlag();

    String getIncludeFlag();

    String getOutputExtension();

    String getOutputFlag();

    String INCLUDE_FILE_FLAG = "-include";
}
