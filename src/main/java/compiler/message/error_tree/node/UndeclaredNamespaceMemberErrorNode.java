package compiler.message.error_tree.node;

import parser.object.NamespaceNode;

public abstract class UndeclaredNamespaceMemberErrorNode extends UndeclaredErrorNode<NamespaceNode> {

}
