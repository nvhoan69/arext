package thread.task;

import compiler.Compiler;
import compiler.message.CompileMessage;
import compiler.message.ICompileMessage;
import config.CommandConfig;
import environment.Environment;
import parser.ProjectParser;
import parser.dependency.Dependency;
import parser.dependency.IncludeHeaderDependency;
import parser.object.HeaderNode;
import parser.object.INode;
import parser.object.ProjectNode;
import search.Search;
import search.condition.SourcecodeFileNodeCondition;
import thread.AbstractVFPTask;
import util.CompilerUtils;
import util.VFPLogger;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class BuildEnvironmentTask extends AbstractVFPTask<ICompileMessage> {

    private static final VFPLogger logger = VFPLogger.get(BuildEnvironmentTask.class);

    private final String path;

    public BuildEnvironmentTask(String path) {
        super();
        this.path = path;
    }

    @Override
    protected ICompileMessage call() throws Exception {
        Environment.getInstance().setupConfiguration();

        ProjectParser parser = new ProjectParser(new File(path));
        ProjectNode root = parser.getRootTree();
        Environment.getInstance().setProjectNode(root);

        List<INode> compilableFiles = findAllSourceCodeFiles(root);
        ICompileMessage compileMessage = findErrorSourcecodeFile(compilableFiles);

        logger.debug("Linking source code file to create an executable file");
        return linkSourceFiles(compilableFiles);
    }

    private List<INode> findAllSourceCodeFiles(INode root) {
        // search for source code file nodes in source code file lists and compile them
        List<INode> sourceFiles = Search.searchNodes(root, new SourcecodeFileNodeCondition());
        sourceFiles.removeIf(this::isIncludedFile);
        return sourceFiles;
    }

    private ICompileMessage findErrorSourcecodeFile(List<INode> sourceCodes) throws Exception {
        ICompileMessage message = null;
        Compiler compiler = Environment.getInstance().getCompiler();

        for (INode fileNode : sourceCodes) {
            String filePath = fileNode.getAbsolutePath();
            logger.debug("Compiling " + filePath);
            message = compiler.compile(fileNode);

            if (message.getType() == ICompileMessage.MessageType.ERROR) {
                throw new Exception(message.getMessage());
            }
        }

        return message;
    }

    private ICompileMessage linkSourceFiles(List<INode> sourceCodes) {
        String executablePath = getExecutablePath();

        List<INode> linkableSourceCodes = sourceCodes.stream()
                .filter(n -> !(n instanceof HeaderNode))
                .collect(Collectors.toList());

        String[] filePaths = getAllOutputFiles(linkableSourceCodes);

        Compiler compiler = Environment.getInstance().getCompiler();
        ICompileMessage linkeMessage = compiler.link(executablePath, filePaths);

        if (linkeMessage == null)
            linkeMessage = new CompileMessage("Can not create executable file", "");

        if (linkeMessage.getMessage().isEmpty()) {
            linkeMessage = new CompileMessage("Main function already exists", "");
        }

        logger.debug("Linking command: " + linkeMessage.getLinkingCommand());

        CommandConfig.LinkEntry linkEntry = new CommandConfig.LinkEntry();
        linkEntry.setBinFiles(Arrays.asList(filePaths));
        linkEntry.setCommand(compiler.getLinkCommand());
        linkEntry.setOutFlag(compiler.getOutputFlag());
        linkEntry.setExeFile(executablePath);

        Environment.getInstance().setLinkEntry(linkEntry);

        return linkeMessage;
    }

    private String[] getAllOutputFiles(List<INode> sourceCodes) {
        Compiler compiler = Environment.getInstance().getCompiler();
        return sourceCodes.stream()
                .filter(file -> !isIncludedFile(file))
                .map(f -> CompilerUtils.getOutfilePath(f.getAbsolutePath(), compiler.getOutputExtension()))
                .toArray(String[]::new);
    }

    private String getExecutablePath() {
        String executablePath = Environment.getInstance().getProjectNode().getAbsolutePath();
        if (!executablePath.endsWith(File.separator))
            executablePath += File.separator;
        executablePath += "vfp_root.exe";
        return executablePath;
    }

    private boolean isIncludedFile(INode node) {
        for (Dependency d : node.getDependencies()) {
            if (d instanceof IncludeHeaderDependency && d.getEndArrow() == node)
                return true;
        }

        return false;
    }
}
