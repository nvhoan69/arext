package thread.task;

import environment.Environment;
import parser.object.INode;
import project_init.ProjectClone;
import testcase_manager.TestCaseManager;
import thread.AbstractVFPTask;

public class PostProcessTask extends AbstractVFPTask<Void> {
    @Override
    protected Void call() throws Exception {
        INode root = Environment.getInstance().getProjectNode();
//        InstructionComputator.compute(root);
        ProjectClone.cloneEnvironment();
        TestCaseManager.initializeMaps();
        return null;
    }
}
